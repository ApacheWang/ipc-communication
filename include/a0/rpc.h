#ifndef A0_RPC_H
#define A0_RPC_H

#include <a0/alloc.h>
#include <a0/arena.h>
#include <a0/buf.h>
#include <a0/callback.h>
#include <a0/file.h>
#include <a0/map.h>
#include <a0/packet.h>
#include <a0/reader.h>
#include <a0/writer.h>
#include <a0/user.h> ///@lxf add

#ifdef __cplusplus
extern "C" {
#endif

typedef struct a0_rpc_topic_s {
  const char* name;
  const a0_file_options_t* file_opts;
} a0_rpc_topic_t;

////////////
// Server //
////////////

typedef struct a0_rpc_server_s a0_rpc_server_t;
//lxf add, for log
typedef struct a0_rpc_log_s a0_rpc_log_t;

typedef struct a0_rpc_request_s {
  a0_rpc_server_t* server;
  a0_packet_t pkt;
} a0_rpc_request_t;

typedef struct a0_rpc_request_callback_s {
  void* user_data;
  void (*fn)(void* user_data, a0_rpc_request_t);
} a0_rpc_request_callback_t;


//@lxf add
typedef struct a0_rpc_request_flat_callback_s {
  void* user_data;
  void* flat_data;
  void (*fn)(void* user_data, void* flat_data);///@lxf
} a0_rpc_request_flat_callback_t;

struct a0_rpc_server_s {
  a0_file_t _file;
  a0_user_identify_t _user; //@lxf add
  a0_reader_t _request_reader;
  //a0_writer_t _response_writer; //lxf remove, not use

  a0_rpc_request_flat_callback_t _onrequest_flat; ///@lxf add
  a0_rpc_request_callback_t _onrequest;
  //a0_packet_id_callback_t _oncancel; //lxf remove, not use
};

//lxf add, for log
struct a0_rpc_log_s {
  a0_file_t _file;
  a0_user_identify_t _user;
  a0_reader_sync_t _reader_sync;
};

//@lxf not use
a0_err_t a0_rpc_server_init(a0_rpc_server_t*,
                            a0_rpc_topic_t,
                            a0_alloc_t,
                            a0_rpc_request_callback_t onrequest,
                            a0_packet_id_callback_t oncancel);

//@lxf add
a0_err_t a0_rpc_init_user(a0_user_identify_t* user,
                            a0_user_type_t user_type,
                            const char* reader_name,
                            const char* writter_name);

//@lxf add
a0_err_t a0_flat_rpc_server_init(a0_rpc_server_t*,
                            a0_rpc_topic_t topic,
                            a0_alloc_t,
                            a0_rpc_request_flat_callback_t onrequest_flat/*@lxf add*/,
                            a0_rpc_request_callback_t onrequest,
                            a0_packet_id_callback_t oncancel,
                            a0_zero_copy_mode_t cpymode/*@lxf add*/,
                            a0_cache_t cache/*@lxf add*/);

//@lxf add
a0_err_t a0_flat_rpc_server_init_oldest(a0_rpc_server_t*,
                                 a0_rpc_topic_t topic,
                                 a0_alloc_t,
                                 a0_rpc_request_flat_callback_t onrequest_flat/*@lxf add*/,
                                 a0_rpc_request_callback_t onrequest,
                                 a0_packet_id_callback_t oncancel,
                                 a0_zero_copy_mode_t cpymode/*@lxf add*/,
                                 a0_cache_t cache/*@lxf add*/);

a0_err_t a0_rpc_server_close(a0_rpc_server_t*);

// Note: do NOT respond with the request packet. The ids MUST be unique!
a0_err_t a0_rpc_server_reply(a0_rpc_request_t, a0_packet_t response);

////////////
// Client //
////////////

typedef struct a0_rpc_client_s {
  a0_file_t _file;
  a0_user_identify_t _user; //@lxf add
  a0_writer_t _request_writer;
  //a0_reader_t _response_reader; //lxf remove

  //a0_map_t _outstanding_requests; //lxf remove, not use
  //pthread_mutex_t _outstanding_requests_mu; //lxf remove, not use
} a0_rpc_client_t;

a0_err_t a0_rpc_client_init(a0_rpc_client_t*, a0_rpc_topic_t, a0_alloc_t);
a0_err_t a0_rpc_client_close(a0_rpc_client_t*);

a0_err_t a0_rpc_client_send(a0_rpc_client_t*, a0_packet_t*/*lxf modify*/, size_t/*lxf add*/, a0_packet_callback_t);
///@lxf add
a0_err_t a0_rpc_client_send_no_ack(a0_rpc_client_t*, a0_packet_t*, size_t);

// Note: use the id from the packet used in a0_rpc_client_send.
a0_err_t a0_rpc_client_cancel(a0_rpc_client_t*, const a0_uuid_t);

////////////////
// Log server //
///////////////

//@lxf Add, for log
a0_err_t a0_rpc_log_server_init(a0_rpc_log_t* log_server,
                            a0_rpc_topic_t topic);

//@lxf Add, for log
a0_err_t a0_rpc_log_server_close(a0_rpc_log_t* log_server);

//@lxf Add, for log
a0_err_t a0_rpc_log_server_read_once(a0_rpc_log_t* log_server,
                            a0_alloc_t alloc,
                            a0_reader_init_t init,
                            int flags,                                  
                           a0_arena_log_t *out);

#ifdef __cplusplus
}
#endif

#endif  // A0_RPC_H
