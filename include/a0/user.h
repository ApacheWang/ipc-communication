/**
 * \file user.h
 * \rst
 * \lxf create
 * User
 * -----
 *
 * User identify.
 *
 *
 * \endrst
 */

#ifndef A0_USER_H
#define A0_USER_H

#ifdef __cplusplus
extern "C" {
#endif

enum{
    USER_NAME_LENGTH = 31, /*exclude tail zero*/
};

typedef enum a0_user_type_e {
  A0_USER_TYPE_READ = 1,
  A0_USER_TYPE_WRITE = 1 <<1,
}a0_user_type_t;

//@lxf add
typedef struct a0_user_identify_s {
    a0_user_type_t user_type;
    char reader_name[USER_NAME_LENGTH];
    char writer_name[USER_NAME_LENGTH];
} a0_user_identify_t;

#ifdef __cplusplus
}
#endif

#endif  // A0_USER_H
