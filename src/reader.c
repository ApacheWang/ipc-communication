#include <a0/alloc.h>
#include <a0/arena.h>
#include <a0/buf.h>
#include <a0/err.h>
#include <a0/event.h>
#include <a0/inline.h>
#include <a0/packet.h>
#include <a0/reader.h>
#include <a0/tid.h>
#include <a0/transport.h>
#include <a0/unused.h>


#include <errno.h>
#include <stdalign.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdbool.h>
#include <stddef.h>
#include <malloc.h>

#include "err_macro.h"

#ifdef DEBUG
#include "assert.h"
#include "ref_cnt.h"
#endif

// Synchronous zero-copy version.

///@lxf add,用于调试，一次读取多个包后就Sleep一下
//#define DEBUG_FORWARD_MULTIPLE_PACKETS
#define USE_CACHE_WHEN_ASSEMBLE

/*
 * the same as function <a0_max_align> in transport.c
 */
A0_STATIC_INLINE
size_t a0_max_align(size_t off) {
    return ((off + alignof(max_align_t) - 1) & ~(alignof(max_align_t) - 1));
}

a0_err_t a0_reader_sync_zc_init(a0_reader_sync_zc_t* reader_sync_zc,
                                a0_user_identify_t* user,
                                a0_arena_t arena,
                                a0_reader_init_t init,
                                a0_reader_iter_t iter) {
  reader_sync_zc->_init = init;
  reader_sync_zc->_iter = iter;
  reader_sync_zc->_first_read_done = false;
  A0_RETURN_ERR_ON_ERR(a0_transport_init(&reader_sync_zc->_transport,A0_USER_TYPE_READ,user->reader_name,arena));

  a0_transport_locked_t tlk;
  A0_RETURN_ERR_ON_ERR(a0_transport_lock(&reader_sync_zc->_transport, &tlk));

  if (init == A0_INIT_OLDEST) {
    a0_transport_jump_head(tlk);
  } else if (init == A0_INIT_MOST_RECENT || init == A0_INIT_AWAIT_NEW) {
    a0_transport_jump_tail(tlk);
  }

  A0_RETURN_ERR_ON_ERR(a0_transport_unlock(tlk));

#ifdef DEBUG
  A0_ASSERT_OK(a0_ref_cnt_inc(arena.buf.ptr, NULL), "");
#endif

  return A0_OK;
}

a0_err_t a0_reader_sync_zc_close(a0_reader_sync_zc_t* reader_sync_zc) {
  A0_MAYBE_UNUSED(reader_sync_zc);
#ifdef DEBUG
  A0_ASSERT(reader_sync_zc, "Cannot close null reader (sync+zc).");

  A0_ASSERT_OK(
      a0_ref_cnt_dec(reader_sync_zc->_transport._arena.buf.ptr, NULL),
      "Reader (sync+zc) closing. Arena was previously closed.");
#endif

  //lxf add
  a0_transport_locked_t tlk;
  A0_RETURN_ERR_ON_ERR(a0_transport_lock(&reader_sync_zc->_transport, &tlk));
  a0_transport_close_reader_key(tlk);
  A0_RETURN_ERR_ON_ERR(a0_transport_unlock(tlk));

  return A0_OK;
}

a0_err_t a0_reader_sync_zc_has_next(a0_reader_sync_zc_t* reader_sync_zc, bool* has_next) {
#ifdef DEBUG
  A0_ASSERT(reader_sync_zc, "Cannot read from null reader (sync+zc).");
#endif

  a0_err_t err;
  a0_transport_locked_t tlk;
  A0_RETURN_ERR_ON_ERR(a0_transport_lock(&reader_sync_zc->_transport, &tlk));

  if (reader_sync_zc->_first_read_done || reader_sync_zc->_init == A0_INIT_AWAIT_NEW) {
    err = a0_transport_has_next(tlk, has_next);
  } else {
    err = a0_transport_nonempty(tlk, has_next);
  }

  a0_transport_unlock(tlk);
  return err;
}

a0_err_t a0_reader_sync_zc_next(a0_reader_sync_zc_t* reader_sync_zc,
                                a0_zero_copy_callback_t cb) {
#ifdef DEBUG
  A0_ASSERT(reader_sync_zc, "Cannot read from null reader (sync+zc).");
#endif

  a0_transport_locked_t tlk;
  A0_RETURN_ERR_ON_ERR(a0_transport_lock(&reader_sync_zc->_transport, &tlk));

  bool empty;
  a0_transport_empty(tlk, &empty);
  if (empty) {
    a0_transport_unlock(tlk);
    return A0_MAKE_SYSERR(EAGAIN);
  }

#ifdef DEBUG
  bool has_next = true;
  if (reader_sync_zc->_first_read_done || reader_sync_zc->_init == A0_INIT_AWAIT_NEW) {
    a0_transport_has_next(tlk, &has_next);
  }
  if (!has_next) {
    a0_transport_unlock(tlk);
    return A0_MAKE_SYSERR(EAGAIN);
  }
#endif

  bool should_step = reader_sync_zc->_first_read_done || reader_sync_zc->_init == A0_INIT_AWAIT_NEW;
  if (!should_step) {
    bool is_valid;
    a0_transport_ptr_valid(tlk, &is_valid);
    should_step = !is_valid;
  }

  if (should_step) {
    if (reader_sync_zc->_iter == A0_ITER_NEXT) {
      a0_transport_step_next(tlk);
    } else if (reader_sync_zc->_iter == A0_ITER_NEWEST) {
      a0_transport_jump_tail(tlk);
    }
  }
  reader_sync_zc->_first_read_done = true;

  a0_transport_frame_t frame;
  a0_transport_read_frame(tlk, &frame);

  a0_flat_packet_t flat_packet = {
      .buf = {
          .ptr = frame.data,
          .size = frame.hdr.data_size,
      },
  };

  cb.fn(cb.user_data, tlk, flat_packet);

  return a0_transport_unlock(tlk);
}

A0_STATIC_INLINE
a0_err_t a0_reader_sync_zc_arena_log(a0_reader_sync_zc_t* reader_sync_zc,
                                a0_alloc_t alloc,
                                a0_arena_log_t* out) {
#ifdef DEBUG
  A0_ASSERT(reader_sync_zc, "Cannot read from null reader (sync+zc).");
#endif

  a0_transport_locked_t tlk;
  A0_RETURN_ERR_ON_ERR(a0_transport_lock(&reader_sync_zc->_transport, &tlk));

  //duplicate arena header
  a0_buf_t arena_buf={0};
  a0_transport_duplicate_arena_head(tlk, alloc, &arena_buf);

  //duplicate tail frame
  a0_buf_t frame_buf={0};
  a0_transport_frame_hdr_t fhdr;
  bool empty;
  A0_RETURN_ERR_ON_ERR(a0_transport_empty(tlk, &empty));
  if(!empty)
  {// not empty
      //a0_transport_jump_tail(tlk);
      reader_sync_zc->_first_read_done = true;

      a0_transport_frame_t frame;
      a0_transport_tail_frame(tlk, &frame);

      fhdr = frame.hdr;
      a0_alloc(alloc, frame.hdr.data_size, &frame_buf);
      memcpy(frame_buf.ptr,frame.data,frame_buf.size);

      //also may do... : duplicate head frame...
  }
  A0_RETURN_ERR_ON_ERR(a0_transport_unlock(tlk));

  //convert arena head to text, need place first line, because it get now time.
  a0_transport_debugstr_arena_head(arena_buf, &out->arena_head.payload);

  //parse frame
  if(frame_buf.ptr)
  {
      //deserialize frame data
      a0_flat_packet_t flat_packet = {
              .buf = frame_buf,
      };
      a0_buf_t unused;
      a0_packet_deserialize(flat_packet, alloc, &out->frame_data, &unused);

      //convert frame head to text
      char bytes[512]={0};
      #ifndef NO_SEQ
      sprintf(bytes, "seq=%lu, off=%lu, next_off=%lu, prev_off=%lu, data_size=%lu",
              fhdr.seq,
              fhdr.off,
              fhdr.next_off,
              fhdr.prev_off,
              fhdr.data_size);
      #else
      sprintf(bytes, " off=%lu, next_off=%lu, prev_off=%lu, data_size=%lu",
              fhdr.off,
              fhdr.next_off,
              fhdr.prev_off,
              fhdr.data_size);
      #endif
      a0_alloc(alloc, strlen(bytes)+1, &out->frame_head.payload);
      memcpy(out->frame_head.payload.ptr,bytes,out->frame_head.payload.size);
      out->frame_head.payload.ptr[out->frame_head.payload.size]='\0';
  }

  if(arena_buf.ptr)
    a0_dealloc(alloc, arena_buf);

  if(frame_buf.ptr)
    a0_dealloc(alloc, frame_buf);

  //@lxf add, following...
  A0_RETURN_ERR_ON_ERR(a0_transport_lock(&reader_sync_zc->_transport, &tlk));
  a0_time_mono_t tm_now;
  a0_time_mono_now(&tm_now);
  a0_transport_debugstr_reader_update_time_count(tlk, tm_now);
  a0_transport_debugstr_writer_update_time_count(tlk, tm_now);
  A0_RETURN_ERR_ON_ERR(a0_transport_unlock(tlk));

  return A0_OK;
}

// Synchronous version.


a0_err_t a0_reader_sync_init(a0_reader_sync_t* reader_sync,
                             a0_user_identify_t* user/*@lxf add*/,
                             a0_arena_t arena,
                             a0_alloc_t alloc,
                             a0_reader_init_t init,
                             a0_reader_iter_t iter) {                        
  reader_sync->_alloc = alloc;
  //lxf modify
  return a0_reader_sync_zc_init(&reader_sync->_reader_sync_zc, user, arena, init, iter);
}

a0_err_t a0_reader_sync_close(a0_reader_sync_t* reader_sync) {
#ifdef DEBUG
  A0_ASSERT(reader_sync, "Cannot close from null reader (sync).");
#endif

  return a0_reader_sync_zc_close(&reader_sync->_reader_sync_zc);
}

a0_err_t a0_reader_sync_has_next(a0_reader_sync_t* reader_sync, bool* has_next) {
#ifdef DEBUG
  A0_ASSERT(reader_sync, "Cannot read from null reader (sync).");
#endif

  return a0_reader_sync_zc_has_next(&reader_sync->_reader_sync_zc, has_next);
}

typedef struct a0_reader_sync_next_data_s {
  a0_alloc_t alloc;
  a0_packet_t* out_pkt;
} a0_reader_sync_next_data_t;

A0_STATIC_INLINE
void a0_reader_sync_next_impl(void* user_data, a0_transport_locked_t tlk, a0_flat_packet_t fpkt) {
  A0_MAYBE_UNUSED(tlk);
  a0_reader_sync_next_data_t* data = (a0_reader_sync_next_data_t*)user_data;
  a0_buf_t unused;
  a0_packet_deserialize(fpkt, data->alloc, data->out_pkt, &unused);
}

a0_err_t a0_reader_sync_next(a0_reader_sync_t* reader_sync, a0_packet_t* pkt) {
#ifdef DEBUG
  A0_ASSERT(reader_sync, "Cannot read from null reader (sync).");
#endif

  a0_reader_sync_next_data_t data = (a0_reader_sync_next_data_t){
      .alloc = reader_sync->_alloc,
      .out_pkt = pkt,
  };
  a0_zero_copy_callback_t zc_cb = (a0_zero_copy_callback_t){
      .user_data = &data,
      .fn = a0_reader_sync_next_impl,
  };
  return a0_reader_sync_zc_next(&reader_sync->_reader_sync_zc, zc_cb);
}

//@lxf add
A0_STATIC_INLINE
a0_err_t a0_reader_sync_arena_log(a0_reader_sync_t* reader_sync, a0_arena_log_t* out) {
#ifdef DEBUG
  A0_ASSERT(reader_sync, "Cannot read from null reader (sync).");
#endif

  return a0_reader_sync_zc_arena_log(&reader_sync->_reader_sync_zc, reader_sync->_alloc, out);
}

// Threaded zero-copy version.

A0_STATIC_INLINE
void a0_reader_zc_thread_handle_pkt(a0_reader_zc_t* reader_zc, a0_transport_locked_t tlk) {
  a0_transport_frame_t frame;
  a0_transport_read_frame(tlk, &frame);

  a0_flat_packet_t fpkt = {
      .buf = {
          .ptr = frame.data,
          .size = frame.hdr.data_size,
      },
  };

  reader_zc->_onpacket.fn(reader_zc->_onpacket.user_data, tlk, fpkt);
}

A0_STATIC_INLINE
void a0_reader_zc_thread_handle_curr_multi_pkts(a0_reader_zc_t* reader_zc, a0_transport_locked_t tlk) {
    a0_off_t frame_off;
    size_t frame_count;
#ifdef READ_LOCK_ONLY_COPY_AREA
    a0_transport_handle_curr_multi_one_pkt(tlk, reader_zc->_cache.size - a0_max_align(sizeof(a0_multi_flat_packets_header_t)),\
                                         &frame_off, &frame_count);
#else
    a0_transport_handle_curr_multi_one_pkt(tlk, reader_zc->_cache.size - a0_max_align(sizeof(a0_multi_flat_packets_header_t)),\
                                        reader_zc->_cache.items_count, &frame_off,&frame_count);
#endif
    reader_zc->_onpacket.fn_multi_pkt(reader_zc->_onpacket.user_data, tlk, frame_off, frame_count);
}

A0_STATIC_INLINE
bool a0_reader_zc_thread_handle_first_pkt(a0_reader_zc_t* reader_zc, a0_transport_locked_t tlk) {
  if (a0_transport_wait(tlk, a0_transport_nonempty_pred(&tlk)) == A0_OK) {
    bool reset = false;
    if (reader_zc->_started_empty) {
      reset = true;
    } else {
      bool ptr_valid;
      a0_transport_ptr_valid(tlk, &ptr_valid);
      reset = !ptr_valid;
    }

    if (reset) {
      a0_transport_jump_head(tlk);
    }

    //lxf add
    a0_transport_update_reader_mono_time(tlk);

    //lxf: consider reader restart, cannot read history data, so not include  reader_zc->_init == A0_INIT_AWAIT_NEW
    if (reset
    || reader_zc->_init == A0_INIT_OLDEST
    || reader_zc->_init == A0_INIT_MOST_RECENT) {
        if(A0_ZEROCOPY_MODE_FORWARD_MULTI_PKTS == reader_zc->_cpymode
        || A0_ZEROCOPY_MODE_FORWARD_MULTI_PKTS_ASSEMBLE == reader_zc->_cpymode){
            a0_reader_zc_thread_handle_curr_multi_pkts(reader_zc, tlk);
        }else{
            a0_reader_zc_thread_handle_pkt(reader_zc, tlk);
        }
    }

    return true;
  }

  return false;
}

A0_STATIC_INLINE
bool a0_reader_zc_thread_handle_next_pkt(a0_reader_zc_t* reader_zc, a0_transport_locked_t tlk) {
  if (a0_transport_wait(tlk, a0_transport_has_next_pred(&tlk)) == A0_OK) {
    if (reader_zc->_iter == A0_ITER_NEXT) {
      a0_transport_step_next(tlk);
    } else if (reader_zc->_iter == A0_ITER_NEWEST) {
      a0_transport_jump_tail(tlk);
    }

    a0_reader_zc_thread_handle_pkt(reader_zc, tlk);

    return true;
  }

  return false;
}

///@lxf add
A0_STATIC_INLINE
bool a0_reader_zc_thread_handle_next_multi_pkts(a0_reader_zc_t* reader_zc, a0_transport_locked_t tlk) {
  if (a0_transport_wait(tlk, a0_transport_has_next_pred(&tlk)) == A0_OK) {
      a0_off_t frame_off;
      size_t frame_count;

    if (reader_zc->_iter == A0_ITER_NEXT) {
#ifdef READ_LOCK_ONLY_COPY_AREA
        if(A0_OK!=a0_transport_handle_next_multi_pkts(tlk, \
          reader_zc->_cache.size - a0_max_align(sizeof(a0_multi_flat_packets_header_t)), \
          &frame_off,&frame_count))
        {
            return false;
        }
#else
      if(A0_OK!=a0_transport_handle_next_multi_pkts(tlk, \
          reader_zc->_cache.size - a0_max_align(sizeof(a0_multi_flat_packets_header_t)), \
          reader_zc->_cache.items_count, &frame_off,&frame_count))
      {//如果写进程没有写入数据就发出通知，则退出读线程
        return false;
      }
#endif
    } else{
      //assert(0);
      return false;
    }

    reader_zc->_onpacket.fn_multi_pkt(reader_zc->_onpacket.user_data, tlk, frame_off, frame_count);
    return true;
  }

  return false;
}

A0_STATIC_INLINE
void* a0_reader_zc_thread_main(void* data) {
  a0_reader_zc_t* reader_zc = (a0_reader_zc_t*)data;
  // Alert that the thread has started.
  reader_zc->_thread_id = a0_tid();
  a0_event_set(&reader_zc->_thread_start_event);

  // Lock until shutdown.
  // Lock will release lock while awaiting packets.
  a0_transport_locked_t tlk;
  a0_transport_lock(&reader_zc->_transport, &tlk);

  // Loop until shutdown is triggered.
  if (a0_reader_zc_thread_handle_first_pkt(reader_zc, tlk)) {
    ///@lxf modify
    if(A0_ZEROCOPY_MODE_FORWARD_MULTI_PKTS == reader_zc->_cpymode
    || A0_ZEROCOPY_MODE_FORWARD_MULTI_PKTS_ASSEMBLE == reader_zc->_cpymode)
    {
        while (a0_reader_zc_thread_handle_next_multi_pkts(reader_zc, tlk)) {}
    }else
    {
        while(a0_reader_zc_thread_handle_next_pkt(reader_zc, tlk)){}
    }
  }

  // Shutting down.
  a0_transport_unlock(tlk);

  return NULL;
}

a0_err_t a0_reader_zc_init(a0_reader_zc_t* reader_zc,
                           a0_user_identify_t* user,
                           a0_arena_t arena,
                           a0_reader_init_t init,
                           a0_reader_iter_t iter,
                           a0_zero_copy_mode_t cpymode/*@lxf add*/,
                           a0_cache_t cache/*@lxf add*/,
                           a0_zero_copy_callback_t onpacket) {
  reader_zc->_init = init;
  reader_zc->_iter = iter;
  reader_zc->_cpymode = cpymode;
  reader_zc->_cache = cache;
  reader_zc->_onpacket = onpacket;

  A0_RETURN_ERR_ON_ERR(a0_transport_init(&reader_zc->_transport, A0_USER_TYPE_READ, user->reader_name, arena));

#ifdef DEBUG
  a0_ref_cnt_inc(arena.buf.ptr, NULL);
#endif

  a0_transport_locked_t tlk;
  a0_transport_lock(&reader_zc->_transport, &tlk);

  a0_transport_empty(tlk, &reader_zc->_started_empty);
  if (!reader_zc->_started_empty) {
    if (init == A0_INIT_OLDEST) {
      a0_transport_jump_head(tlk);
    } else if (init == A0_INIT_MOST_RECENT || init == A0_INIT_AWAIT_NEW) {
      a0_transport_jump_tail(tlk);
    }
  }

  a0_transport_unlock(tlk);

  a0_event_init(&reader_zc->_thread_start_event);

  pthread_create(
      &reader_zc->_thread,
      NULL,
      a0_reader_zc_thread_main,
      reader_zc);

  return A0_OK;
}

a0_err_t a0_reader_zc_close(a0_reader_zc_t* reader_zc) {
  a0_event_wait(&reader_zc->_thread_start_event);
  if (pthread_equal(pthread_self(), reader_zc->_thread_id)) {
    return A0_MAKE_SYSERR(EDEADLK);
  }
#ifdef DEBUG
  a0_ref_cnt_dec(reader_zc->_transport._arena.buf.ptr, NULL);
#endif

  a0_transport_locked_t tlk;
  a0_transport_lock(&reader_zc->_transport, &tlk);
  a0_transport_shutdown(tlk);
  //lxf add
  a0_transport_close_reader_key(tlk);
  a0_transport_unlock(tlk);

  a0_event_close(&reader_zc->_thread_start_event);
  pthread_join(reader_zc->_thread, NULL);

  return A0_OK;
}

// Threaded version.

///@lxf add
A0_STATIC_INLINE
void a0_copy_to_flat_pkt(a0_reader_t* reader,a0_transport_frame_hdr_t* frame_hdr,a0_flat_packet_t* out_fpkt)
{
  // alloc to external message space
  a0_alloc(reader->_alloc, frame_hdr->data_size, &out_fpkt->buf);
  // save in external message space
  memcpy(out_fpkt->buf.ptr, (uint8_t*)frame_hdr+a0_max_align(sizeof(a0_transport_frame_hdr_t)), frame_hdr->data_size);
}

//@lxf add
A0_STATIC_INLINE
void a0_reader_onpacket_flatcopy_wrapper(void* user_data, a0_transport_locked_t tlk, a0_flat_packet_t fpkt) {
  a0_reader_t* reader = (a0_reader_t*)user_data;
  a0_packet_t pkt;
  a0_buf_t buf;

  // alloc to external message space
  a0_alloc(reader->_alloc, fpkt.buf.size, &buf);
  // save in external message space
  memcpy(buf.ptr, fpkt.buf.ptr, fpkt.buf.size);
  a0_transport_unlock(tlk);

  //notify
  a0_flat_packet_callback_call(reader->_onpacket);
  a0_transport_lock(tlk.transport, &tlk);
}

A0_STATIC_INLINE
void a0_reader_onpacket_wrapper(void* user_data, a0_transport_locked_t tlk, a0_flat_packet_t fpkt) {
  a0_reader_t* reader = (a0_reader_t*)user_data;
  a0_packet_t pkt;
  a0_buf_t buf;
  a0_packet_deserialize(fpkt, reader->_alloc, &pkt, &buf);
  a0_transport_unlock(tlk);

  a0_packet_callback_call(reader->_onpacket, pkt);
  a0_dealloc(reader->_alloc, buf);

  a0_transport_lock(tlk.transport, &tlk);
}

///@lxf add
A0_STATIC_INLINE
void a0_reader_onpacket_deploy_wrapper(void* user_data, a0_transport_locked_t tlk, a0_flat_packet_t fpkt) {
  a0_reader_t* reader = (a0_reader_t*)user_data;
  a0_packet_t pkt;
  a0_buf_t buf;

  //TODO lxf,  recoganize simple head first field, so following which step.
  if(1)
  {//temp, lxf
      a0_reader_onpacket_flatcopy_wrapper(user_data, tlk, fpkt);
  }
  else
  {
     a0_reader_onpacket_wrapper(user_data, tlk, fpkt);
  }
}

///@lxf add
A0_STATIC_INLINE
void a0_reader_onpacket_multi_pkts_wrapper(void* user_data, a0_transport_locked_t tlk,\
                                           a0_off_t frame_off,size_t frame_count) {
  a0_reader_t* reader = (a0_reader_t*)user_data;

    if(frame_off.size > reader->_reader_zc._cache.size){
        //当前帧大于缓存时就自动准备获取下一帧，丢弃当前帧。
        return;
    }

    //static int n=0;
    //copy flat packets into cache
  a0_buf_t buf;
  a0_transport_get_frame_ptr(tlk, frame_off, &buf);
  uint8_t *cache_data = reader->_reader_zc._cache.data;
  //TODO...if only one frame, not memcpy large data, direct call function<a0_copy_to_flat_pkt>
  memcpy(cache_data, buf.ptr, frame_off.size);
  a0_transport_unlock(tlk);

  //for test receive multiple packets.
#ifdef DEBUG_FORWARD_MULTIPLE_PACKETS
  thrd_sleep(&(struct timespec){.tv_sec=1}, NULL);
#endif

  //split to multiple flat packet
  a0_flat_packet_t fpkt;
  a0_transport_frame_hdr_t* frame_hdr;

  a0_transport_frame_hdr_t* frame1_hdr =(a0_transport_frame_hdr_t* )cache_data;
  size_t frame1_off = frame1_hdr->off;
  if(1 == frame_count)
  {// only include 1 frame
    a0_copy_to_flat_pkt(reader,frame1_hdr,&fpkt);
    a0_flat_packet_callback_call(reader->_onpacket);
      //n++;
  }
  else
  {
    // only include multiple frames
    ///@lxf add, maybe TODO... put packet buckets into queue.
    size_t curr_off = 0;
    for(int i=0;i<frame_count;i++)
    {
      frame_hdr =(a0_transport_frame_hdr_t* )(cache_data + curr_off);
      a0_copy_to_flat_pkt(reader,frame_hdr,&fpkt);
        /*if(n==10)
        {
            printf("flat=%x, %x\n",fpkt.buf.ptr[fpkt.buf.size-2], fpkt.buf.ptr[fpkt.buf.size-1]);
        }*/

        //each packet calls one callback, not good(TODO...)
      a0_flat_packet_callback_call(reader->_onpacket);
      //n++;
      //because here multiple flat packets's addresses are increase incremental.
      curr_off = frame_hdr->next_off - frame1_off;
    }
  }

  a0_transport_lock(tlk.transport, &tlk);
}

///@lxf add
void a0_reader_get_one_flat_packet_from_assemble(uint8_t* assem_data, size_t off,\
                                        size_t* out_next_off, a0_flat_packet_t *out_flat_pkt)
{
    a0_transport_frame_hdr_t* frame_hdr = (a0_transport_frame_hdr_t*)(assem_data+a0_max_align(sizeof(a0_multi_flat_packets_header_t))+off);
    *out_next_off = frame_hdr->next_off;
    *out_flat_pkt = (a0_flat_packet_t){
        .buf ={.ptr=(uint8_t*)frame_hdr+ a0_max_align(sizeof(a0_transport_frame_hdr_t)),
               .size= frame_hdr->data_size},
    };
}

#ifdef READ_LOCK_ONLY_COPY_AREA
///@lxf add
A0_STATIC_INLINE
void a0_reader_onpacket_multi_pkts_assemble_wrapper(void* user_data, a0_transport_locked_t tlk,\
                                           a0_off_t copy_off,size_t write_off_tail) {
    a0_reader_t* reader = (a0_reader_t*)user_data;
    //static int n=0;
    //copy flat packets into cache
    a0_buf_t buf;
    a0_transport_get_frame_ptr(tlk, copy_off, &buf);
#ifdef USE_CACHE_WHEN_ASSEMBLE
    uint8_t *cache_data = reader->_reader_zc._cache.data+a0_max_align(sizeof(a0_multi_flat_packets_header_t));
    //TODO...if only one frame, not memcpy large data, direct call function<a0_copy_to_flat_pkt>
    memcpy(cache_data, buf.ptr, copy_off.size);
    a0_transport_unlock(tlk);
#endif

    //for test receive multiple packets.
#ifdef DEBUG_FORWARD_MULTIPLE_PACKETS
    thrd_sleep(&(struct timespec){.tv_sec=1}, NULL);
#endif

    size_t frames_count, frames_end, read_off;
    if(A0_OK == a0_cache_find_frames(cache_data, copy_off.size, write_off_tail, \
                                        &frames_count, &frames_end, &read_off)){
        tlk.transport->_read_result = A0_READ_COPY_OK;
        tlk.transport->_read_off = read_off;
        a0_buf_t out_fbuf;
        a0_alloc(reader->_alloc,frames_end + a0_max_align(sizeof(a0_multi_flat_packets_header_t)), &out_fbuf);
        uint8_t *data = out_fbuf.ptr+a0_max_align(sizeof(a0_multi_flat_packets_header_t));

        // save in external message space
        memcpy(data, cache_data, frames_end);

        a0_multi_flat_packets_header_t* packets_header=(a0_multi_flat_packets_header_t*)out_fbuf.ptr;
        packets_header->bytes_count = frames_end;
        packets_header->pkts_count = frames_count;

        a0_flat_packet_callback_call(reader->_onpacket);
    }else{
        //当前帧大于缓存时就自动准备获取下一帧，丢弃当前帧。
        //tlk.transport->_read_off = 0;
        tlk.transport->_read_result = A0_READ_COPY_NO_FRAME;
    }

    a0_transport_lock(tlk.transport, &tlk);

#ifdef READ_LOCK_ONLY_COPY_AREA
    //更新读指针
    a0_transport_update_copy_read_off(tlk, frames_count);
#endif

}
#else
///@lxf add
A0_STATIC_INLINE
void a0_reader_onpacket_multi_pkts_assemble_wrapper(void* user_data, a0_transport_locked_t tlk,\
                                           a0_off_t frame_off,size_t frame_count) {
  a0_reader_t* reader = (a0_reader_t*)user_data;
    //static int n=0;

    if(frame_off.size > reader->_reader_zc._cache.size - a0_max_align(sizeof(a0_multi_flat_packets_header_t))){
       //当前帧大于缓存时就自动准备获取下一帧，丢弃当前帧。
    }

    //copy flat packets into cache
  a0_buf_t buf;
  a0_transport_get_frame_ptr(tlk, frame_off, &buf);
#ifdef USE_CACHE_WHEN_ASSEMBLE
  uint8_t *cache_data = reader->_reader_zc._cache.data+a0_max_align(sizeof(a0_multi_flat_packets_header_t));
  //TODO...if only one frame, not memcpy large data, direct call function<a0_copy_to_flat_pkt>
  memcpy(cache_data, buf.ptr, frame_off.size);
  a0_transport_unlock(tlk);
#endif

  //for test receive multiple packets.
#ifdef DEBUG_FORWARD_MULTIPLE_PACKETS
  thrd_sleep(&(struct timespec){.tv_sec=1}, NULL);
#endif

 // alloc to external message space
 /*if (frame_count>1)
 {
     int n=0;
     n=1;
 }*/
  a0_buf_t out_fbuf;
  a0_alloc(reader->_alloc,  frame_off.size + a0_max_align(sizeof(a0_multi_flat_packets_header_t)), &out_fbuf);
  uint8_t *data = out_fbuf.ptr+a0_max_align(sizeof(a0_multi_flat_packets_header_t));

#ifdef USE_CACHE_WHEN_ASSEMBLE
  // save in external message space
  memcpy(data, cache_data, frame_off.size);
#else
    memcpy(data, buf.ptr, frame_off.size);
    a0_transport_unlock(tlk);
#endif

  a0_multi_flat_packets_header_t* packets_header=(a0_multi_flat_packets_header_t*)out_fbuf.ptr;
  packets_header->bytes_count = frame_off.size;
  packets_header->pkts_count = frame_count;

    a0_alter_frameoffs_relative_first_frame((a0_transport_frame_hdr_t*)data,frame_count);
  a0_flat_packet_callback_call(reader->_onpacket);

  a0_transport_lock(tlk.transport, &tlk);
}
#endif

a0_err_t a0_reader_init(a0_reader_t* reader,
                        a0_user_identify_t* user,
                        a0_arena_t arena,
                        a0_alloc_t alloc,
                        a0_reader_init_t init,
                        a0_reader_iter_t iter,
                        a0_packet_callback_t onpacket,
                        a0_zero_copy_mode_t cpymode/*@lxf add*/,
                        a0_cache_t cache/*@lxf add*/) {
  reader->_alloc = alloc;
  reader->_onpacket = onpacket;

  a0_zero_copy_callback_t onpacket_wrapper = (a0_zero_copy_callback_t){
      .user_data = reader,
      .fn = NULL,
  };

///@lxf add
  switch(cpymode)
  {
    case A0_ZEROCOPY_MODE_PARSE:
    onpacket_wrapper.fn = a0_reader_onpacket_wrapper;
    break;

    case A0_ZEROCOPY_MODE_FORWARD_SINGLE_PKT:
    onpacket_wrapper.fn = a0_reader_onpacket_deploy_wrapper;
    break;

    case A0_ZEROCOPY_MODE_FORWARD_MULTI_PKTS:
    onpacket_wrapper.fn_multi_pkt = a0_reader_onpacket_multi_pkts_wrapper;
    break;

    case A0_ZEROCOPY_MODE_FORWARD_MULTI_PKTS_ASSEMBLE:
    onpacket_wrapper.fn_multi_pkt = a0_reader_onpacket_multi_pkts_assemble_wrapper;
    break;

    default:
    onpacket_wrapper.fn = NULL;
    onpacket_wrapper.fn_multi_pkt = NULL;
    return A0_ERRCODE_INVALID_ARG;
    break;
  }
  
  return a0_reader_zc_init(&reader->_reader_zc, user, arena, init, iter, cpymode, cache, onpacket_wrapper);
}

a0_err_t a0_reader_close(a0_reader_t* reader) {
  return a0_reader_zc_close(&reader->_reader_zc);
}

// Read one version.

typedef struct a0_reader_read_one_data_s {
  a0_packet_t* pkt;
  a0_event_t done_event;
} a0_reader_read_one_data_t;

void a0_reader_read_one_callback(void* user_data, a0_packet_t pkt) {
  a0_reader_read_one_data_t* data = (a0_reader_read_one_data_t*)user_data;
  if (a0_event_is_set(&data->done_event)) {
    return;
  }

  *data->pkt = pkt;
  a0_event_set(&data->done_event);
}

A0_STATIC_INLINE
a0_err_t a0_reader_read_one_blocking(a0_user_identify_t* user/*@lxf add*/,
                                     a0_arena_t arena,
                                     a0_alloc_t alloc,
                                     a0_reader_init_t init,
                                     a0_packet_t* out) {
  a0_reader_read_one_data_t data;
  data.pkt = out;
  a0_event_init(&data.done_event);

  a0_packet_callback_t onpacket = {
      .user_data = &data,
      .fn = a0_reader_read_one_callback,
  };

  a0_reader_t reader;
  a0_err_t err = a0_reader_init(&reader,
                                user,
                                arena,
                                alloc,
                                init,
                                A0_ITER_NEXT,
                                onpacket,
                                A0_ZEROCOPY_MODE_PARSE/*@lxf add*/,
                                (a0_cache_t){}/*@lxf add*/);
  if (err) {
    a0_event_close(&data.done_event);
    return err;
  }

  a0_event_wait(&data.done_event);

  a0_reader_close(&reader);
  a0_event_close(&data.done_event);

  return A0_OK;
}

A0_STATIC_INLINE
a0_err_t a0_reader_read_one_nonblocking(a0_user_identify_t* user/*@lxf add*/,
                                        a0_arena_t arena,
                                        a0_alloc_t alloc,
                                        a0_reader_init_t init,
                                        a0_packet_t* out) {
  if (init == A0_INIT_AWAIT_NEW) {
    return A0_MAKE_SYSERR(EAGAIN);
  }

  a0_reader_sync_t reader_sync;
  //@lxf modify
  A0_RETURN_ERR_ON_ERR(a0_reader_sync_init(&reader_sync, user, arena, alloc, init, A0_ITER_NEXT));

  bool has_next;
  a0_err_t err = a0_reader_sync_has_next(&reader_sync, &has_next);
  if (!err) {
    if (has_next) {
      a0_reader_sync_next(&reader_sync, out);
    } else {
      err = A0_MAKE_SYSERR(EAGAIN);
    }
  }
  a0_reader_sync_close(&reader_sync);
  return err;
}

a0_err_t a0_reader_read_one(a0_user_identify_t* user/*@lxf add*/,
                            a0_arena_t arena,
                            a0_alloc_t alloc,
                            a0_reader_init_t init,
                            int flags,
                            a0_packet_t* out) {
  if (flags & O_NONBLOCK) {
    //@lxf modify
    return a0_reader_read_one_nonblocking(user, arena, alloc, init, out);
  }
  //@lxf modify
  return a0_reader_read_one_blocking(user, arena, alloc, init, out);
}

//@lxf add, log, 由函数<a0_reader_read_one_nonblocking>修改而来
A0_STATIC_INLINE
a0_err_t a0_reader_read_nonblocking_arena_log(a0_user_identify_t* user/*@lxf add*/,
                                        a0_arena_t arena,
                                        a0_alloc_t alloc,
                                        a0_reader_init_t init,
                                        a0_arena_log_t *out) {
  a0_reader_sync_t reader_sync;
  //@lxf modify
  A0_RETURN_ERR_ON_ERR(a0_reader_sync_init(&reader_sync, user, arena, alloc, init, A0_ITER_NEXT));

  a0_err_t err=a0_reader_sync_arena_log(&reader_sync, out);
  a0_reader_sync_close(&reader_sync);
  return err;
}

//@lxf add, for log
a0_err_t a0_reader_read_arena_log(a0_user_identify_t* user,
                            a0_arena_t arena,
                            a0_alloc_t alloc,
                            a0_reader_init_t init,
                            int flags,                                  
                           a0_arena_log_t *out) {
  if (flags & O_NONBLOCK) {
    //@lxf modify
    return a0_reader_read_nonblocking_arena_log(user, arena, alloc, init, out);
  }

  //@lxf modify, now not support
  return A0_MAKE_SYSERR(EBADF);
  //return a0_reader_read_one_blocking(user, arena, alloc, init, out);
}