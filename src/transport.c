#include <a0/alloc.h>
#include <a0/arena.h>
#include <a0/buf.h>
#include <a0/callback.h>
#include <a0/err.h>
#include <a0/inline.h>
#include <a0/mtx.h>
#include <a0/time.h>
#include <a0/transport.h>
#include <a0/user.h> ///@lxf add

#include <errno.h>
#include <stdalign.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/param.h> //only for MIN micro

#include "clock.h"
#include "err_macro.h"

///@lxf add
/*-------------------------------------
    a0_transport_hdr_t                |
  ____________________________________|    
    a0_transport_state_area_t         |
  ____________________________________|
    a0_transport_user_hdr_t(reader)   |
  ____________________________________|
    a0_reader_loc_states_t[2](reader) |
    {                                 |
      array,                          |
      bool committed_page_idx;        |
    }                                 |
  ____________________________________|
    a0_user_key_states_t[2](reader)   |
    {                                 |
      array,                          |
      bool committed_page_idx;        |
    }                                 |
  ____________________________________|
    a0_transport_user_hdr_t(writer)   |
  ____________________________________|
    a0_user_key_states_t[2](writer)   |
    bool committed_page_idx;          |
  ____________________________________|
    workspace (data area)             |
---------------------------------------*/


///@lxf add
enum{
    MAX_USER_COUNT = 4,  /*MAX_READ_USER_COUNT和MAX_WRITE_USER_COUNT的最大值*/

    /*最大读者数量，要尽量小，因为：
    1. 连接的读者越多，读写越慢;
    2. 考虑安全性,提交commit时对整体数组提交，不对单个元素提交
    3. 改动数量时，要参考涉及的结构体xxxstates：
        要保证涉及的数组的字节对齐，目前只要是2的倍数即可保证8字节对齐*/
    MAX_READ_USER_COUNT = MAX_USER_COUNT,
    MAX_WRITE_USER_COUNT = 2, 
};

/*committed_page_idx:
  要防止进程突然崩溃导致该变量出现第三态（非0非1值）,故
  1. 使用时采用!!读取其值, 要注意不要被编译优化掉!!
  2. 采用bool代替uint8_t更好。
  3. 原子变量如果被突然中断，无资料说明会保证不会出现第三态，而且效率也低一点。
  故不采用原子变量*/

typedef struct a0_transport_state_s {
  #ifndef NO_SEQ
  SEQ_TYPE seq_low;  //lxf deprecated, only for test
  SEQ_TYPE seq_high; //lxf deprecated, only for 
  #endif
  size_t off_head;
  size_t off_tail;
  /*可以观察写的数据条数, 并且如果这个数量在变化，说明进程在运行;
    只是个粗略统计值，不需要非常精准*/
  size_t write_stat_count;
  /*统计值：已使用空间，
  但是如果tail在上，head在下，
  则已使用空间为head下面接近底部的frame尾部,
  此时不扣除tail和Head之间的空白间隙*/
  size_t high_water_mark;
} a0_transport_state_t;

typedef struct a0_transport_version_s {
  uint8_t major;
  uint8_t minor;
  uint8_t patch;
} a0_transport_version_t;

#ifdef ARENA_INCLUDE_TEST_DATA
enum LOG_RESET_WRITE_OFF_CAUSE{
    RESET_WRITE_OFF_NONE=0,
    RESET_WRITE_OFF_ORDER=1,
    RESET_WRITE_OFF_OF_EVICT=2,
    RESET_WRITE_OFF_OF_ALL_READ=3,
    RESET_WRITE_ERROR=4,
    RESET_WRITE_OFF_MAX=5,
};
#endif

typedef struct a0_transport_hdr_s {
  char magic[14]; /* ALEPHZERO, include tail zero, read as string*/
  a0_transport_version_t version;
  bool initialized;

  a0_mtx_t mtx;
  a0_cnd_t cnd;

  //lxf 移动到别处是考虑乒乓缓存的影响
  //a0_transport_state_t state_pages[2];
  //bool committed_page_idx;
  size_t arena_size;

  //log, test data
#ifdef ARENA_INCLUDE_TEST_DATA
  size_t log_write_off_reset_count;
  size_t log_write_pkts_total_count;
  uint8_t log_reset_write_off_cause; //LOG_RESET_WRITE_OFF_CAUSE
#endif
} a0_transport_hdr_t;

//lxf add
typedef struct a0_transport_states_area_s {
  a0_transport_state_t state_pages[2];
  bool committed_page_idx; //@lxf modify uint8_t to bool
} a0_transport_states_area_t;

///@lxf add for future reader backward_compatiblility
typedef struct a0_transport_user_hdr_s {
  a0_transport_version_t version;
  size_t max_user_count;
} a0_transport_user_hdr_t;

///@lxf add, 这个用户信息结构体可以比较大，只是登录和关闭时改写一次
typedef struct a0_user_key_s {
    uint8_t name[USER_NAME_LENGTH+1]; //include tail zero, byte align in 8 bytes
    size_t ref_count; //同名的进程数量，但是如果进程崩溃，则该数量不会减1，故不准确，只做参考
    uint8_t occupy_times; //statistics info, 占领次数
    a0_time_mono_t tm_mono; //statistics info, mono time
    //a0_time_wall_t tm_wall; //statistics info, wall time
}a0_user_key_t;

///@lxf add
typedef struct a0_user_key_vec_s {
    a0_user_key_t impl[MAX_USER_COUNT];
}a0_user_key_vec_t;

///@lxf add
typedef struct a0_user_key_states_s {
    a0_user_key_vec_t pages[2];
    bool committed_page_idx;
} a0_user_key_states_t;

///@lxf add, 尺寸尽量小
typedef struct a0_reader_loc_s {
    size_t off; // 读指针
    size_t next_copy_off; //下一次复制的数据的偏移，最多只复制到共享内存的底部，不做环绕复制.
    bool evicting_head_in_copy_area; //开始在复制区域里踢写head数据
    /*可以观察读的数据条数, 并且如果这个数量在变化，说明进程在运行;
    只是个粗略统计值，不需要非常精准*/
    size_t stat_count;
    size_t keep_alive; // now not use
}a0_reader_loc_t;

///@lxf add, 尺寸尽量小
typedef struct a0_reader_loc_vec_s {
    a0_reader_loc_t impl[MAX_READ_USER_COUNT];
}a0_reader_loc_vec_t; //__attribute((aligned

///@lxf add
typedef struct a0_reader_loc_states_s {
    a0_reader_loc_vec_t pages[2];
    bool committed_page_idx;
} a0_reader_loc_states_t;

// TODO(lshamis): Consider packing or reordering fields to reduce this.
//lxf remove
//_Static_assert(sizeof(a0_transport_hdr_t) == 144, "Unexpected transport binary representation.");

/*
 * the same as function <a0_max_align> in reader.c
 */

A0_STATIC_INLINE
size_t a0_max_align(size_t off) {
    return ((off + alignof(max_align_t) - 1) & ~(alignof(max_align_t) - 1));
}

///@lxf add
A0_STATIC_INLINE
size_t a0_transport_hdr_size() {
    return a0_max_align(sizeof(a0_transport_hdr_t));
}

///@lxf add
A0_STATIC_INLINE
a0_transport_states_area_t* a0_transport_states_area(a0_transport_locked_t lk) {
    a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
    return (a0_transport_states_area_t*)((uint8_t*)hdr + a0_max_align(sizeof(a0_transport_hdr_t)));
}

///@lxf add
A0_STATIC_INLINE
size_t a0_transport_states_area_size() {
    return a0_max_align(sizeof(a0_transport_states_area_t));
}

//lxf modify
A0_STATIC_INLINE
a0_transport_state_t* a0_transport_committed_page(a0_transport_locked_t lk) {
  a0_transport_states_area_t* states =a0_transport_states_area(lk);
  return &states->state_pages[!!states->committed_page_idx];
}

//lxf modify
A0_STATIC_INLINE
a0_transport_state_t* a0_transport_working_page(a0_transport_locked_t lk) {
  a0_transport_states_area_t* states =a0_transport_states_area(lk);
  return &states->state_pages[!states->committed_page_idx];
}

///@lxf modify
A0_STATIC_INLINE
size_t a0_transport_workspace_off() {
  return a0_max_align(sizeof(a0_transport_hdr_t)) \
  /*-----states area-------*/
   +a0_max_align(sizeof(a0_transport_states_area_t)) \
  /*-----reader-------*/
   +a0_max_align(sizeof(a0_transport_user_hdr_t)) \
   +a0_max_align(sizeof(a0_reader_loc_states_t)) \
   +a0_max_align(sizeof(a0_user_key_states_t)) \
   /*-----writer-------*/
   +a0_max_align(sizeof(a0_transport_user_hdr_t)) \
   +a0_max_align(sizeof(a0_user_key_states_t));
}

//@lxf add for log
a0_err_t a0_transport_duplicate_arena_head(a0_transport_locked_t lk, a0_alloc_t alloc, a0_buf_t *out)
{
   a0_alloc(alloc, a0_transport_workspace_off(), out);
   memcpy(out->ptr, lk.transport->_arena.buf.ptr, out->size);
   return A0_OK;
}

/*正式提交修改,不执行此函数，则一切修改无效，
不会发生第三态变化的数据，所以不会导致其他程序读取崩溃，
但可能会丢失几条未读数据(如果之前发生了a0_transport_evict，并且随后执行过a0_transport_commit)*/
a0_err_t a0_transport_commit(a0_transport_locked_t lk) {
    a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
    // Assume page A was the previously committed page and page B is the working
    // page that is ready to be committed. Both represent a valid state for the
    // transport. It's possible that the copying of B into A will fail (prog crash),
    // leaving A in an inconsistent state. We set B as the committed page, before
    // copying the page info.
    //lxf modify
    a0_transport_states_area_t* states = a0_transport_states_area(lk);
    states->committed_page_idx = !states->committed_page_idx;
    states->state_pages[!states->committed_page_idx]=states->state_pages[!!states->committed_page_idx];
    //*a0_transport_working_page(lk) = *a0_transport_committed_page(lk); //lxf remove

    a0_cnd_broadcast(&hdr->cnd, &hdr->mtx);

    return A0_OK;
}

//lxf add, only remove a0_cnd_broadcast from function <a0_transport_commit>
a0_err_t a0_transport_commit_not_broadcast(a0_transport_locked_t lk) {
    a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
    // Assume page A was the previously committed page and page B is the working
    // page that is ready to be committed. Both represent a valid state for the
    // transport. It's possible that the copying of B into A will fail (prog crash),
    // leaving A in an inconsistent state. We set B as the committed page, before
    // copying the page info.
    //lxf modify
    a0_transport_states_area_t* states = a0_transport_states_area(lk);
    states->committed_page_idx = !states->committed_page_idx;
    states->state_pages[!states->committed_page_idx]=states->state_pages[!!states->committed_page_idx];
    //*a0_transport_working_page(lk) = *a0_transport_committed_page(lk); //lxf remove

    return A0_OK;
}

///@lxf add
A0_STATIC_INLINE
size_t a0_transport_reader_hdr_off() {
  return a0_max_align(sizeof(a0_transport_hdr_t)) \
  /*-----states area-------*/
  +a0_max_align(sizeof(a0_transport_states_area_t));
}

///@lxf add
A0_STATIC_INLINE
size_t a0_transport_reader_loc_states_size() {
  return a0_max_align(sizeof(a0_reader_loc_states_t));
}

///@lxf add
A0_STATIC_INLINE
size_t a0_transport_user_key_states_size() {
  return a0_max_align(sizeof(a0_user_key_states_t));
}

///@lxf add
A0_STATIC_INLINE
a0_reader_loc_states_t* a0_transport_reader_loc_states(a0_transport_locked_t lk) {
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  return (a0_reader_loc_states_t*)((uint8_t*)hdr\
                                   +a0_max_align(sizeof(a0_transport_hdr_t))
                                     /*-----states area-------*/
                                   +a0_max_align(sizeof(a0_transport_states_area_t)) \
                                   /*-----reader-------*/
                                   +a0_max_align(sizeof(a0_transport_user_hdr_t)));
}

///@lxf add
A0_STATIC_INLINE
a0_user_key_states_t* a0_transport_reader_key_states(a0_transport_locked_t lk) {
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  return (a0_user_key_states_t*)((uint8_t*)hdr\
                                    +a0_max_align(sizeof(a0_transport_hdr_t))\
                                    /*-----states area-------*/
                                    +a0_max_align(sizeof(a0_transport_states_area_t)) \
                                    /*-----reader-------*/
                                    +a0_max_align(sizeof(a0_transport_user_hdr_t)) \
                                    +a0_max_align(sizeof(a0_reader_loc_states_t)));
}

///@lxf add
A0_STATIC_INLINE
a0_reader_loc_vec_t* a0_transport_committed_reader_loc_page(a0_transport_locked_t lk) {
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  a0_reader_loc_states_t* loc_states = (a0_reader_loc_states_t*)((uint8_t*)hdr\
                                      +a0_max_align(sizeof(a0_transport_hdr_t))
                                      /*-----states area-------*/
                                      +a0_max_align(sizeof(a0_transport_states_area_t)) \
                                      /*-----reader-------*/
                                      +a0_max_align(sizeof(a0_transport_user_hdr_t)));
  return &loc_states->pages[!!loc_states->committed_page_idx];
}

///@lxf add
A0_STATIC_INLINE
a0_reader_loc_vec_t* a0_transport_working_reader_loc_page(a0_transport_locked_t lk) {
  a0_reader_loc_states_t* loc_states = a0_transport_reader_loc_states(lk);
  return &loc_states->pages[!loc_states->committed_page_idx];
}

///@lxf add
A0_STATIC_INLINE
a0_user_key_vec_t* a0_transport_committed_reader_key_page(a0_transport_locked_t lk) {
  a0_user_key_states_t* key_states = a0_transport_reader_key_states(lk);
  return &key_states->pages[!!key_states->committed_page_idx];
}

///@lxf add
A0_STATIC_INLINE
a0_user_key_vec_t* a0_transport_working_reader_key_page(a0_transport_locked_t lk) {
  a0_user_key_states_t* key_states = a0_transport_reader_key_states(lk);
  return &key_states->pages[!key_states->committed_page_idx];
}

///@lxf add
A0_STATIC_INLINE
a0_reader_loc_t* a0_transport_reader_loc(a0_transport_locked_t lk) {
    a0_reader_loc_vec_t* loc_vec = a0_transport_working_reader_loc_page(lk);
    return &loc_vec->impl[lk.transport->_reader_index];
}

///@lxf add
A0_STATIC_INLINE
a0_user_key_t* a0_transport_reader_key(a0_transport_locked_t lk) {
    a0_user_key_vec_t* key_vec = a0_transport_working_reader_key_page(lk);
    return &key_vec->impl[lk.transport->_reader_index];
}

///@lxf add
A0_STATIC_INLINE
size_t a0_transport_writer_hdr_off() {
    return a0_max_align(sizeof(a0_transport_hdr_t)) \
    /*-----states area-------*/
    +a0_max_align(sizeof(a0_transport_states_area_t)) \
    /*-----reader-------*/
    +a0_max_align(sizeof(a0_transport_user_hdr_t)) \
    +a0_max_align(sizeof(a0_reader_loc_states_t)) \
    +a0_max_align(sizeof(a0_user_key_states_t));
}

///@lxf add
A0_STATIC_INLINE
a0_user_key_states_t* a0_transport_writer_key_states(a0_transport_locked_t lk) {
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  return (a0_user_key_states_t*)((uint8_t*)hdr\
                                    +a0_max_align(sizeof(a0_transport_hdr_t)) \
                                    /*-----states area-------*/
                                    +a0_max_align(sizeof(a0_transport_states_area_t)) \
                                     /*-----reader-------*/
                                    +a0_max_align(sizeof(a0_transport_user_hdr_t)) \
                                    +a0_max_align(sizeof(a0_reader_loc_states_t)) \
                                    +a0_max_align(sizeof(a0_user_key_states_t)) \
                                    /*-----writer-------*/
                                    +a0_max_align(sizeof(a0_transport_user_hdr_t)));
}

///@lxf add
A0_STATIC_INLINE
a0_user_key_vec_t* a0_transport_committed_writer_key_page(a0_transport_locked_t lk) {
  a0_user_key_states_t* key_states = a0_transport_writer_key_states(lk);
  return &key_states->pages[!!key_states->committed_page_idx];
}

///@lxf add
A0_STATIC_INLINE
a0_user_key_vec_t* a0_transport_working_writer_key_page(a0_transport_locked_t lk) {
  a0_user_key_states_t* key_states = a0_transport_writer_key_states(lk);
  return &key_states->pages[!key_states->committed_page_idx];
}

#ifdef READ_LOCK_ONLY_COPY_AREA
///@lxf add
//使得所有的reader的读指针复位为0
A0_STATIC_INLINE
void a0_transport_invalid_all_readers_loc(a0_transport_locked_t lk)
{
    a0_reader_loc_vec_t* loc_vec = a0_transport_working_reader_loc_page(lk);
    a0_reader_loc_t* loc;
    for(size_t i=0; i<MAX_READ_USER_COUNT; i++)
    {
        loc = &loc_vec->impl[i];
        if(loc->off) {
            loc->off = 0;
            loc->next_copy_off = 0;
            loc->evicting_head_in_copy_area = false;
        }
    }
}


/*******************************************
 * 关于function<a0_transport_invalid_some_readers_loc_before>和
 * function<a0_transport_invalid_some_readers_loc_after>的设计说明:

更改off_head时，如果next_copy_off非0，则意味着先前有拷贝（off!=0），
就要判断如果off_head在[off, next_copy_off]，则意味着开始冲掉读指针了，
读指针做个开始被冲掉的标记，当被标记了，并且冲掉后，再判断如果新的off_head离开了[off, next_copy_off）这个区域，
则复位该读指针；
如果读指针还在该区域里，由于时逐条冲掉，所以off_head之后的next_off依然是有效的，
所以读copy_off里由于已被读了，这个区域已经包含了off_head,所以依然可以使用在该区域里的读指针。
因此冲掉前后要判断两次。
 *********************************************/

///@lxf add
A0_STATIC_INLINE
void a0_transport_invalid_some_readers_loc_before(a0_transport_locked_t lk, size_t head_off)
{
    /*
    准备更改off_head时，如果next_copy_off非0，则意味着先前有拷贝（off!=0），
   就要判断如果off_head在[off, next_copy_off]，则意味着开始冲掉读指针了，
   读指针做个开始被冲掉的标记.
     */
    a0_reader_loc_vec_t* loc_vec = a0_transport_working_reader_loc_page(lk);
    a0_reader_loc_t* loc;
    for(int i=0; i<MAX_READ_USER_COUNT; i++)
    {
        loc = &loc_vec->impl[i];
        if(loc->next_copy_off){
            if(loc->off <= head_off && head_off <= loc->next_copy_off){
                loc->evicting_head_in_copy_area = true;
            }
        }else if(head_off == loc_vec->impl[i].off) {
            //还没有读取过任何数据
            loc->off = 0;
        }
    }
}

///@lxf add
//使得reader.off==off的reader的读指针复位为0
A0_STATIC_INLINE
void a0_transport_invalid_some_readers_loc_after(a0_transport_locked_t lk, size_t head_off)
{
    /*
    更改off_head时，如果next_copy_off非0，则意味着先前有拷贝（off!=0），
   就要判断如果已经标记了正在被冲掉，就要再判断如果新的off_head离开了[off, next_copy_off）这个区域，
   则复位该读指针.
     */
    a0_reader_loc_vec_t* loc_vec = a0_transport_working_reader_loc_page(lk);
    a0_reader_loc_t* loc;
    for(int i=0; i<MAX_READ_USER_COUNT; i++)
    {
        loc = &loc_vec->impl[i];
        if(loc->next_copy_off && loc->evicting_head_in_copy_area){
            if(loc->off <= head_off && head_off < loc->next_copy_off){
                loc->off = 0;
                loc->next_copy_off = 0;
                loc->evicting_head_in_copy_area = false;
            }
        }
    }
}

///@lxf add ?????? to modify
//使得reader.off在[start_off, end_off]之间的reader的读指针复位为0
A0_STATIC_INLINE
void a0_transport_invalid_some_readers_loc_between(a0_transport_locked_t lk, size_t start_off, size_t end_off)
{
    a0_reader_loc_vec_t* loc_vec = a0_transport_working_reader_loc_page(lk);
    a0_reader_loc_t* loc;
    size_t off;
    for(int i=0; i<MAX_READ_USER_COUNT; i++)
    {
        loc = &loc_vec->impl[i];
        off = loc->off;
        if(off
           && start_off <= off && off <= end_off) {
            loc->off = 0;
            loc->next_copy_off = 0;
            loc->evicting_head_in_copy_area = false;
        }
    }
}
#else/*READ_LOCK_ONLY_COPY_AREA*/
///@lxf add
//使得所有的reader的读指针复位为0
A0_STATIC_INLINE
void a0_transport_invalid_all_readers_loc(a0_transport_locked_t lk)
{
  a0_reader_loc_vec_t* loc_vec = a0_transport_working_reader_loc_page(lk);
  for(size_t i=0; i<MAX_READ_USER_COUNT; i++)
  {
    if(loc_vec->impl[i].off) {
        loc_vec->impl[i].off = 0;
    }
  }
}

///@lxf add
//使得reader.off==off的reader的读指针复位为0
A0_STATIC_INLINE
void a0_transport_invalid_some_readers_loc(a0_transport_locked_t lk, size_t off)
{
  a0_reader_loc_vec_t* loc_vec = a0_transport_working_reader_loc_page(lk);
  for(int i=0; i<MAX_READ_USER_COUNT; i++)
  {
      if(off == loc_vec->impl[i].off) {
          loc_vec->impl[i].off = 0;
      }
  }
}

///@lxf add
//使得reader.off在[start_off, end_off]之间的reader的读指针复位为0
A0_STATIC_INLINE
void a0_transport_invalid_some_readers_loc_between(a0_transport_locked_t lk, size_t start_off, size_t end_off)
{
  a0_reader_loc_vec_t* loc_vec = a0_transport_working_reader_loc_page(lk);
  size_t off;
  for(int i=0; i<MAX_READ_USER_COUNT; i++)
  {
      off = loc_vec->impl[i].off;
      if(off
          && start_off <= off && off <= end_off) {
          loc_vec->impl[i].off = 0;
      }
  }
}
#endif

///@lxf add
//使得所有的reader的读指针复位为0
A0_STATIC_INLINE
a0_err_t a0_transport_all_readers_have_read_to_tail(a0_transport_locked_t lk, size_t write_off_tail, bool *out)
{
  a0_reader_loc_vec_t* loc_vec = a0_transport_working_reader_loc_page(lk);
  size_t tmp;
  *out = true;
  for(size_t i=0; i<MAX_READ_USER_COUNT; i++)
  {
    /*检查是否有指向写head和写tail之间的读指针:
    但是如果有读进程崩溃了，将会导致其读指针可能永远不指向写尾部，导致永远写指针不能复位，如何检测读进程崩溃也许是个需要做的事情，但是如果该读进程随后能自动重启，
    那么就可以不需要检测读进程崩溃了，读进程重启后会自动复位读指针，
    我们的项目一般都会自动重启进程。
    */
    tmp = loc_vec->impl[i].off;
    if(0 != tmp && write_off_tail != tmp)
    {//有指向写head和写tail之间的读指针
      *out = false;
      break;
    }
  }

  return A0_OK;
}

///@lxf add
//使得索引号为pos的reader的读指针复位为0
A0_STATIC_INLINE
void a0_transport_reset_one_reader_loc(a0_transport_locked_t lk, size_t pos)
{
  a0_reader_loc_vec_t* loc_vec = a0_transport_working_reader_loc_page(lk);
   loc_vec->impl[pos].off = 0;
    loc_vec->impl[pos].next_copy_off = 0;
   loc_vec->impl[pos].stat_count = 0;
   loc_vec->impl[pos].keep_alive = 1;
}

///@lxf add
//使得索引号为pos的reader的读指针复位为0
A0_STATIC_INLINE
void a0_transport_close_one_reader_loc(a0_transport_locked_t lk, size_t pos)
{
    a0_reader_loc_vec_t* loc_vec = a0_transport_working_reader_loc_page(lk);
    //loc_vec->impl[pos].off = 0;
    //loc_vec->impl[pos].stat_count = 0;
    loc_vec->impl[pos].keep_alive = 0;
}

///@lxf add
a0_err_t a0_transport_reader_loc_commit(a0_transport_locked_t lk) {
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  // Assume page A was the previously committed page and page B is the working
  // page that is ready to be committed. Both represent a valid state for the
  // transport. It's possible that the copying of B into A will fail (prog crash),
  // leaving A in an inconsistent state. We set B as the committed page, before
  // copying the page info.

  a0_reader_loc_states_t* loc_states = a0_transport_reader_loc_states(lk);
  loc_states->committed_page_idx = !loc_states->committed_page_idx;
  loc_states->pages[!loc_states->committed_page_idx]=loc_states->pages[!!loc_states->committed_page_idx];

  return A0_OK;
}

///@lxf add
a0_err_t a0_transport_reader_key_commit(a0_transport_locked_t lk) {
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  // Assume page A was the previously committed page and page B is the working
  // page that is ready to be committed. Both represent a valid state for the
  // transport. It's possible that the copying of B into A will fail (prog crash),
  // leaving A in an inconsistent state. We set B as the committed page, before
  // copying the page info.
  a0_user_key_states_t* key_states = a0_transport_reader_key_states(lk);
  key_states->committed_page_idx = !key_states->committed_page_idx;
  key_states->pages[!key_states->committed_page_idx]=key_states->pages[!!key_states->committed_page_idx];

  return A0_OK;
}

///@lxf add
a0_err_t a0_transport_writer_key_commit(a0_transport_locked_t lk) {
    a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
    // Assume page A was the previously committed page and page B is the working
    // page that is ready to be committed. Both represent a valid state for the
    // transport. It's possible that the copying of B into A will fail (prog crash),
    // leaving A in an inconsistent state. We set B as the committed page, before
    // copying the page info.
    a0_user_key_states_t* key_states = a0_transport_writer_key_states(lk);
    key_states->committed_page_idx = !key_states->committed_page_idx;
    key_states->pages[!key_states->committed_page_idx]=key_states->pages[!!key_states->committed_page_idx];

    return A0_OK;
}

#if 0 //@lxf remove
// Converts a 0.2 transport into a 0.3 transport.
// Note: This does not allow 0.2 and 0.3 to run simultaniously.
//       0.2 transport will no longer work after this.
A0_STATIC_INLINE
void a0_backward_compatiblility_update_from_0_2(a0_arena_t arena) {
  if (*(uint16_t*)arena.buf.ptr != 0x0101) {
    // Not 0.2 format.
    return;
  }

  uint32_t committed_page_idx = *(uint32_t*)(&arena.buf.ptr[120]);

  uint32_t page_offset = committed_page_idx ? 88 : 56;
  uint8_t* ptr = &arena.buf.ptr[page_offset];

  SEQ_TYPE seq_low = *(SEQ_TYPE*)ptr;
  ptr += sizeof(SEQ_TYPE);

  SEQ_TYPE seq_high = *(SEQ_TYPE*)ptr;
  ptr += sizeof(SEQ_TYPE);

  uintptr_t off_head = *(uintptr_t*)ptr;
  ptr += sizeof(uintptr_t);

  uintptr_t off_tail = *(uintptr_t*)ptr;
  ptr += sizeof(uintptr_t);

  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)arena.buf.ptr;
  memset(hdr, 0, sizeof(a0_transport_hdr_t));

  memcpy(hdr->magic, "ALEPHZERO", 9);
  hdr->version.major = 0;
  hdr->version.minor = 3;
  hdr->version.patch = 0;

  hdr->arena_size = arena.buf.size;
  hdr->state_pages[0].seq_low = seq_low;
  hdr->state_pages[0].seq_high = seq_high;
  hdr->state_pages[0].off_head = off_head;
  hdr->state_pages[0].off_tail = off_tail;
  hdr->state_pages[0].high_water_mark = hdr->arena_size;

  hdr->state_pages[1] = hdr->state_pages[0];
  hdr->initialized = true;
}
#endif

/*
lxf add.
首先找同名的位置，改变里面的信息；
如果找不到，则找没有使用过的（空字符）位置，否则，就报错
*/
A0_STATIC_INLINE
a0_err_t a0_transport_insert_new_user(a0_transport_locked_t lk, a0_user_key_vec_t* key_vec,\
                                       size_t max_user_count, const char* name, size_t* pos)
{
  //首选同名位置，其次空位置
  a0_user_key_t*key_temp;
  size_t null_pos;
  bool has_null_pos = false;
  size_t i;
  for(i=0; i<max_user_count;i++)
  {
    key_temp = &key_vec->impl[i];
    if('\0'==key_temp->name[0])
    {
      if(!has_null_pos)
      {
        null_pos= i;
        has_null_pos=true;
      } 
    }else if(0==strcmp(name, key_vec->impl[i].name))
    {
      break;
    }
  }

  a0_user_key_t* key = NULL;
  if(i<max_user_count)
  {
    //找到了一个同名, 则记录次数，时间点
      *pos = i;
    key = &key_vec->impl[i];
    key->ref_count++;
    key->occupy_times++;
    a0_time_mono_now(&key->tm_mono);
  }
  else if(has_null_pos)
  {//找打了一个空位置, 则记录次数，时间点
      *pos = null_pos;
    key = &key_vec->impl[null_pos];
    strcpy(key->name,name);
    key->ref_count=1;
    key->occupy_times=1;
    a0_time_mono_now(&key->tm_mono);
  }else{
      a0_transport_unlock(lk);
    return A0_ERRCODE_USER_COUNT_FULL;
  }

  return A0_OK;
}

//lxf add
A0_STATIC_INLINE
a0_err_t a0_transport_reader_restart(a0_transport_locked_t lk,const char* user_name)
{
  /*读进程重启后，只复位该进程的读指针，意味着开始等候读取下一条最新数据.
  */

    //插入或替换读用户的信息
  a0_user_key_vec_t* key_vec = a0_transport_working_reader_key_page(lk);
  A0_RETURN_ERR_ON_ERR(a0_transport_insert_new_user(lk, key_vec, MAX_READ_USER_COUNT,\
                                                    user_name, &lk.transport->_reader_index));
  a0_transport_reader_key_commit(lk);

  //只复位该进程的读指针
  a0_transport_reset_one_reader_loc(lk, lk.transport->_reader_index);
  a0_transport_reader_loc_commit(lk);

  return A0_OK;
}

//lxf add
A0_STATIC_INLINE
a0_err_t a0_transport_writer_restart(a0_transport_locked_t lk,const char* user_name)
{
    /*写进程，通常只有一个写进程，当写进程重启后，复位所有读指针和写指针更安全，
    所以写进程重启后就清空所有数据，同时发出写通知，以便让读进程恢复下（但是futex是没办法处理了，保持原样）.
  */

    //插入或替换写用户的信息
  a0_user_key_vec_t* key_vec = a0_transport_working_writer_key_page(lk);
  A0_RETURN_ERR_ON_ERR(a0_transport_insert_new_user(lk, key_vec, MAX_WRITE_USER_COUNT,\
                        user_name, &lk.transport->_writer_index));
    a0_transport_writer_key_commit(lk);

  //复位所有读指针
  a0_transport_invalid_all_readers_loc(lk);
  a0_transport_reader_loc_commit(lk);

  //复位所有写指针,清除所有内存数据
  a0_transport_state_t* working_page = a0_transport_working_page(lk);
  #ifndef NO_SEQ
  working_page->seq_low = 1;
  working_page->seq_high = 1;
  #endif
  working_page->off_head = 0; //标识无内存数据了
  working_page->off_tail = 0; //标识无内存数据了
  working_page->write_stat_count = 0; //重新计数写的条数，不管有几个写进程
  working_page->high_water_mark = a0_transport_workspace_off();
  //此时没有数据写入，不需要广播
  a0_transport_commit_not_broadcast(lk);

  return A0_OK;
}

a0_err_t a0_transport_init(a0_transport_t* transport, a0_user_type_t user_type, const char* user_name, a0_arena_t arena) {
    a0_err_t ret = A0_OK;

  //a0_backward_compatiblility_update_from_0_2(arena); //@lxf remove

  // The arena is expected to be either:
  // 1) all null bytes.
  //    this is guaranteed by ftruncate, as is used in a0/file.h
  // 2) a pre-initialized buffer.
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)arena.buf.ptr;

  memset(transport, 0, sizeof(a0_transport_t));
  transport->_arena = arena;

  if (transport->_arena.mode == A0_ARENA_MODE_EXCLUSIVE) {
    memset(&hdr->mtx, 0, sizeof(hdr->mtx));
  }

  a0_transport_locked_t lk;
  A0_RETURN_ERR_ON_ERR(a0_transport_lock(transport, &lk));

  if (!hdr->initialized) {
    /*在Linux环境中，对开始申请的共享内存空间进行了初始化，初始值为0x00,
    所以不需要对共享内存值初始化为0*/

    //memset((uint8_t*)hdr, 0, a0_transport_hdr_size());
#if defined(NO_PACKET_HEADER)&&defined(NO_PACKET_ID)
    memcpy(hdr->magic, "EXD_IPC", 7);
#else
    memcpy(hdr->magic, "EXD_IPC_PKT_H", 13);
#endif
    hdr->version.major = 0;
    hdr->version.minor = 3;
    hdr->version.patch = 0;
    hdr->arena_size = transport->_arena.buf.size;

    //a0_transport_states_area_size
    a0_transport_states_area_t* state_pages = a0_transport_states_area(lk);
    //memset((uint8_t*)state_pages, 0, a0_transport_states_area_size());
    state_pages->state_pages[0].high_water_mark = a0_transport_workspace_off();
    state_pages->state_pages[1].high_water_mark = a0_transport_workspace_off();

    ///@lxf add
    //initialize readers header
    a0_transport_user_hdr_t* reader_hdr = (a0_transport_user_hdr_t*)((uint8_t*)hdr \
                                                    +a0_transport_reader_hdr_off());
    reader_hdr->version.major = 0;
    reader_hdr->version.minor = 0;
    reader_hdr->version.patch = 1;
    reader_hdr->max_user_count = MAX_READ_USER_COUNT;

    ///@lxf add
    //initialize readers state space with 0
    //memset((uint8_t*)a0_transport_reader_loc_states(lk), 0, a0_transport_reader_loc_states_size());
    //memset((uint8_t*)a0_transport_reader_key_states(lk), 0, a0_transport_user_key_states_size());

  ///@lxf add
  //initialize writers header
    a0_transport_user_hdr_t* writer_hdr = (a0_transport_user_hdr_t*)((uint8_t*)hdr \
                                                +a0_transport_writer_hdr_off());
    writer_hdr->version.major = 0;
    writer_hdr->version.minor = 0;
    writer_hdr->version.patch = 1;
    writer_hdr->max_user_count = MAX_WRITE_USER_COUNT;

    //initialize writers state space with 0
    //memset((uint8_t*)a0_transport_writer_key_states(lk), 0, a0_transport_user_key_states_size());

    hdr->initialized = true;
  }
  // TODO(lshamis): Verify magic + version.
    /*进程重启处理*/
    if(A0_USER_TYPE_READ == user_type){
       //读进程重启
      a0_transport_reader_restart(lk,user_name);
    }
    else if(A0_USER_TYPE_WRITE == user_type){
       //写进程重启
        a0_transport_writer_restart(lk,user_name);
        //log, test data.....
    #ifdef ARENA_INCLUDE_TEST_DATA
        hdr->log_write_off_reset_count = 0;
        hdr->log_write_pkts_total_count = 0;
    #endif
    }else{
        //assert(0);
        ret = A0_ERRCODE_INVALID_ARG;
    }

  a0_transport_unlock(lk);

  return ret;
}

a0_err_t a0_transport_shutdown(a0_transport_locked_t lk) {
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;

  lk.transport->_shutdown = true;
  a0_cnd_broadcast(&hdr->cnd, &hdr->mtx);

  while (lk.transport->_wait_cnt) {
    a0_cnd_wait(&hdr->cnd, &hdr->mtx);
  }
  return A0_OK;
}

///lxf add
#if 0
a0_err_t a0_transport_shutdown_particular_ipc(a0_transport_locked_t lk, pthread_t* thread) {
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;

  lk.transport->_shutdown = true;
  a0_cnd_broadcast(&hdr->cnd, &hdr->mtx);

  while (lk.transport->_wait_cnt) {
    if(0 != pthread_tryjoin_np(*thread, NULL)){
      //lxf, TODO...需要想办法复现验证是否有效, 如果线程thread没有崩溃结束则等候，该线程在此特指ipc里的读线程
      a0_cnd_wait(&hdr->cnd, &hdr->mtx);
    }
    
  }
  return A0_OK;
}
#endif

a0_err_t a0_transport_shutdown_requested(a0_transport_locked_t lk, bool* out) {
  *out = lk.transport->_shutdown;
  return A0_OK;
}

a0_err_t a0_transport_close_reader_key(a0_transport_locked_t lk)
{
  a0_user_key_vec_t* key_vec = a0_transport_working_reader_key_page(lk);
  if(--key_vec->impl[lk.transport->_reader_index].ref_count == 0)
  {
    memset(&key_vec->impl[lk.transport->_reader_index], 0 ,sizeof(a0_user_key_t));

    a0_transport_close_one_reader_loc(lk, lk.transport->_reader_index);
    a0_transport_reader_loc_commit(lk);
  }

  a0_transport_reader_key_commit(lk);
  return A0_OK;
}

a0_err_t a0_transport_close_writer_key(a0_transport_locked_t lk)
{
  a0_user_key_vec_t* key_vec = a0_transport_working_writer_key_page(lk);
  if(--key_vec->impl[lk.transport->_writer_index].ref_count == 0)
  {
    memset(&key_vec->impl[lk.transport->_writer_index], 0 ,sizeof(a0_user_key_t));

    //重启写进程时，会重新设置下面的值，所以没必要修改了
     //复位所有写指针,清除所有内存数据
      /*a0_transport_state_t* working_page = a0_transport_working_page(lk);
      working_page->stat_count = 0;
      #ifndef NO_SEQ
      working_page->seq_low = 1;
      working_page->seq_high = 1;
      #endif
      working_page->off_head = 0;//标识无内存数据了
      working_page->off_tail = 0; //标识无内存数据了
      working_page->high_water_mark = a0_transport_workspace_off();
      a0_transport_commit(lk);*/
  }
  a0_transport_writer_key_commit(lk);
  return A0_OK;
}

a0_err_t a0_transport_lock(a0_transport_t* transport, a0_transport_locked_t* lk_out) {
  lk_out->transport = transport;

  if (transport->_arena.mode != A0_ARENA_MODE_SHARED) {
    return A0_OK;
  }

  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)transport->_arena.buf.ptr;

  a0_err_t lock_status = a0_mtx_lock(&hdr->mtx);
  if (A0_SYSERR(lock_status) == EOWNERDEAD) {
    // The data is always consistent by design.
    lock_status = a0_mtx_consistent(&hdr->mtx);
  }

  /*Clear any incomplete changes;
  首先用提交页覆盖工作页，防止进程崩溃造成的工作页不完整。*/
  *a0_transport_working_page(*lk_out) = *a0_transport_committed_page(*lk_out);

  return lock_status;
}

a0_err_t a0_transport_unlock(a0_transport_locked_t lk) {
  if (lk.transport->_arena.mode != A0_ARENA_MODE_SHARED) {
    return A0_OK;
  }

  /*lxf remove, 提交页不需要时刻去覆盖工作页，尤其是解锁前;
  因为工作页总是要当锁住后才使用，而且当锁住时会首先用提交页去覆盖工作页;
  任何进程只要修改了工作页面都会立即主动提交页面,不需要解锁前再做一次这个动作。*/
  //*a0_transport_working_page(lk) = *a0_transport_committed_page(lk);
  
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  a0_mtx_unlock(&hdr->mtx);
  return A0_OK;
}

a0_err_t a0_transport_empty(a0_transport_locked_t lk, bool* out) {
  a0_transport_state_t* working_page = a0_transport_working_page(lk);

  ///lxf modify
  #if 1
  *out = !working_page->off_tail;
  #else
  *out = !working_page->seq_high | (working_page->seq_low > working_page->seq_high);
  #endif

  return A0_OK;
}

a0_err_t a0_transport_nonempty(a0_transport_locked_t lk, bool* out) {
  a0_err_t err = a0_transport_empty(lk, out);
  *out = !*out;
  return err;
}

a0_err_t a0_transport_ptr_valid(a0_transport_locked_t lk, bool* out) {
  a0_transport_state_t* working_page = a0_transport_working_page(lk);
    //@lxf modify
#if 1
    /* 无效是因为被冲写了，但是如果冲写了就会设置为0（表示无效）,
    所以只要是非0就表示有效。*/
    a0_reader_loc_t* reader_loc = a0_transport_reader_loc(lk);
    *out = !!reader_loc->off;
#else
  *out = (working_page->seq_low <= lk.transport->_seq) &&
         (lk.transport->_seq <= working_page->seq_high);
#endif
  return A0_OK;
}

///@lxf add
void a0_transport_get_frame_ptr(a0_transport_locked_t lk, a0_off_t frame_off,a0_buf_t *out)
{
    a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
    out->ptr = (uint8_t*)hdr + frame_off.off;
    out->size = 0; //not need extra packet size
}

///@lxf add
void a0_transport_update_reader_mono_time(a0_transport_locked_t lk)
{
    a0_user_key_vec_t* key_vec = a0_transport_working_reader_key_page(lk);
    a0_time_mono_now(&key_vec->impl[lk.transport->_reader_index].tm_mono);
    a0_transport_reader_key_commit(lk);
}

/*lxf add 解决一旦共享内存满就会永远冲写的问题。
解决方法，采用读触发检查，而不是写触发检查, 可能会使得写指针和所有读指针都复位:

 每次读完后，就要检查如果读到了写tail，并且如果写tail指针在写head上面，就要下面处理，否则不处理：
先检查所有读指针是否已到写尾，即都读完了，则：
  把写指针复位到0，从数据区起始位置开始写，这样能减少冲换写的概率，所以尽量减少最大用户数，就能减少遍历读指针的数量。

读指针已读完：
      读指针off如果非0，则指向已读的数据，所以off非0时如果等于写tail就代表已读完。

写指针复位到0:
    受影响的是指向写head和写tail之间的读指针，所以这些读指针也要复位，
    因此所有读指针都复位一次即可，还有个high_water_mark变量(只是个统计变量，没实际作用)也要复位到最初位置。
*/
A0_STATIC_INLINE
void a0_transport_may_loop_write_read_reset(a0_transport_locked_t lk, a0_transport_state_t* working_page,\
                                      a0_reader_loc_t* reader_loc, bool* committed)
{
  *committed = false;

    //如果读到了写tail，并且如果写tail指针在写head上面,则准备复位写指针,否则返回
  if(reader_loc->off != working_page->off_tail || working_page->off_tail >= working_page->off_head)
    return;

  bool all_read_to_tail;
  a0_transport_all_readers_have_read_to_tail(lk, working_page->off_tail, &all_read_to_tail);
  if(all_read_to_tail)
  {//所有读指针都读到写尾部了
#ifdef ARENA_INCLUDE_TEST_DATA
      a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
      hdr->log_write_off_reset_count++;
#endif
    //写指针复位到0
    //working_page->seq_low = 1;
    //working_page->seq_high = 1;
    working_page->off_head = 0; //标识无内存数据了
    working_page->off_tail = 0; //标识无内存数据了
#ifdef ARENA_INCLUDE_TEST_DATA
      hdr->log_reset_write_off_cause = RESET_WRITE_OFF_OF_ALL_READ;
#endif
    working_page->high_water_mark = a0_transport_workspace_off();

    //读指针复位
    a0_transport_invalid_all_readers_loc(lk);
    a0_transport_reader_loc_commit(lk);
    //由于此时实际没有写入数据，所以不需要广播
    a0_transport_commit_not_broadcast(lk);
    *committed = true;
  }
}

a0_err_t a0_transport_jump_head(a0_transport_locked_t lk) {
  a0_transport_state_t* state = a0_transport_working_page(lk);

  bool empty;
  A0_RETURN_ERR_ON_ERR(a0_transport_empty(lk, &empty));
  if (empty) {
    return A0_ERRCODE_TRANSPORT_CANNOT_MOVE_POINTER;
  }

    //@lxf modify
#if 1
    a0_reader_loc_t* reader_loc = a0_transport_reader_loc(lk);
    #ifndef NO_SEQ
    lk.transport->_seq = state->seq_low;
    #endif
    reader_loc->off = state->off_head;
#else
  lk.transport->_seq = state->seq_low;
  lk.transport->_off = state->off_head;
#endif
  return A0_OK;
}

a0_err_t a0_transport_jump_tail(a0_transport_locked_t lk) {
  a0_transport_state_t* state = a0_transport_working_page(lk);

  bool empty;
  A0_RETURN_ERR_ON_ERR(a0_transport_empty(lk, &empty));
  if (empty) {
    return A0_ERRCODE_TRANSPORT_CANNOT_MOVE_POINTER;
  }

  //@lxf modify
#if 1
    a0_reader_loc_t* reader_loc = a0_transport_reader_loc(lk);
    #ifndef NO_SEQ
    lk.transport->_seq = state->seq_high;
    #endif
    reader_loc->off = state->off_tail;
#else
  lk.transport->_seq = state->seq_high;
  lk.transport->_off = state->off_tail;
#endif
  return A0_OK;
}

a0_err_t a0_transport_has_next(a0_transport_locked_t lk, bool* out) {
  bool empty;
  A0_RETURN_ERR_ON_ERR(a0_transport_empty(lk, &empty));

  a0_transport_state_t* state = a0_transport_working_page(lk);

  #if 1
  a0_reader_loc_t* reader_loc = a0_transport_reader_loc(lk);
  *out =  !empty && (!reader_loc->off || reader_loc->off != state->off_tail);
  #else
  *out = !empty && (lk.transport->_seq < state->seq_high);
  #endif

  return A0_OK;
}

a0_err_t a0_transport_step_next(a0_transport_locked_t lk) {
  a0_transport_state_t* state = a0_transport_working_page(lk);

  bool has_next;
  A0_RETURN_ERR_ON_ERR(a0_transport_has_next(lk, &has_next));
  if (!has_next) {
    return A0_ERRCODE_TRANSPORT_CANNOT_MOVE_POINTER;
  }

//lxf modify
#if 1
  a0_reader_loc_t* reader_loc = a0_transport_reader_loc(lk);
  if(!reader_loc->off)
  {
#ifndef NO_SEQ
    lk.transport->_seq = state->seq_low;
#endif
    reader_loc->off = state->off_head;
    return A0_OK; //退出
  }
#else
  if (lk.transport->_seq < state->seq_low) {
    lk.transport->_seq = state->seq_low;
    lk.transport->_off = state->off_head;
    return A0_OK;
  }
#endif

  //lxf modify
#if 1
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  a0_transport_frame_hdr_t* curr_frame_hdr =
      (a0_transport_frame_hdr_t*)((uint8_t*)hdr + reader_loc->off);
  reader_loc->off = curr_frame_hdr->next_off;

  //TODO...lxf to remove
  a0_transport_frame_hdr_t* next_frame_hdr =
      (a0_transport_frame_hdr_t*)((uint8_t*)hdr + reader_loc->off);
    #ifndef NO_SEQ
    lk.transport->_seq = next_frame_hdr->seq;
    #endif
#else
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  a0_transport_frame_hdr_t* curr_frame_hdr =
      (a0_transport_frame_hdr_t*)((uint8_t*)hdr + lk.transport->_off);
  lk.transport->_off = curr_frame_hdr->next_off;

  a0_transport_frame_hdr_t* next_frame_hdr =
      (a0_transport_frame_hdr_t*)((uint8_t*)hdr + lk.transport->_off);
  lk.transport->_seq = next_frame_hdr->seq;
#endif

  return A0_OK;
}

a0_err_t a0_transport_has_prev(a0_transport_locked_t lk, bool* out) {
  bool empty;
  A0_RETURN_ERR_ON_ERR(a0_transport_empty(lk, &empty));

  a0_transport_state_t* state = a0_transport_working_page(lk);
//lxf modify
#if 1
  a0_reader_loc_t* reader_loc = a0_transport_reader_loc(lk);
  *out = !empty && reader_loc->off != state->off_head;
#else
  *out = !empty && (lk.transport->_seq > state->seq_low);
#endif
  return A0_OK;
}

//不能执行循环写复位a0_transport_may_loop_write_read_reset，因为这是向prev跳转
a0_err_t a0_transport_step_prev(a0_transport_locked_t lk) {
  bool has_prev;
  A0_RETURN_ERR_ON_ERR(a0_transport_has_prev(lk, &has_prev));
  if (!has_prev) {
    return A0_ERRCODE_TRANSPORT_CANNOT_MOVE_POINTER;
  }

  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;

  //@lxf modify
  #if 1
  a0_reader_loc_t* reader_loc = a0_transport_reader_loc(lk);

  a0_transport_frame_hdr_t* curr_frame_hdr =
     (a0_transport_frame_hdr_t*)((uint8_t*)hdr + reader_loc->off);
  reader_loc->off = curr_frame_hdr->prev_off;

  //TODO...lxf to remove
  a0_transport_frame_hdr_t* prev_frame_hdr =
      (a0_transport_frame_hdr_t*)((uint8_t*)hdr + reader_loc->off);
  #ifndef NO_SEQ
  lk.transport->_seq = prev_frame_hdr->seq;
  #endif
  #else
  a0_transport_frame_hdr_t* curr_frame_hdr =
      (a0_transport_frame_hdr_t*)((uint8_t*)hdr + lk.transport->_off);
  lk.transport->_off = curr_frame_hdr->prev_off;

  a0_transport_frame_hdr_t* prev_frame_hdr =
      (a0_transport_frame_hdr_t*)((uint8_t*)hdr + lk.transport->_off);
  lk.transport->_seq = prev_frame_hdr->seq;
  #endif

  return A0_OK;
}

typedef struct a0_transport_timedwait_data_s {
  a0_predicate_t user_pred;
  a0_time_mono_t timeout;
} a0_transport_timedwait_data_t;

A0_STATIC_INLINE
a0_err_t a0_transport_timedwait_predicate(void* data_, bool* out) {
  a0_transport_timedwait_data_t* data = (a0_transport_timedwait_data_t*)data_;

  a0_time_mono_t now;
  a0_time_mono_now(&now);

  uint64_t now_ns = now.ts.tv_sec * NS_PER_SEC + now.ts.tv_nsec;
  uint64_t timeout_ns = data->timeout.ts.tv_sec * NS_PER_SEC + data->timeout.ts.tv_nsec;

  if (now_ns >= timeout_ns) {
    *out = true;
    return A0_OK;
  }
  return a0_predicate_eval(data->user_pred, out);
}

A0_STATIC_INLINE
a0_err_t a0_transport_timedwait_impl(a0_transport_locked_t lk, a0_predicate_t pred, const a0_time_mono_t* timeout) {
  if (lk.transport->_shutdown) {
    return A0_MAKE_SYSERR(ESHUTDOWN);
  }
  if (lk.transport->_arena.mode != A0_ARENA_MODE_SHARED) {
    return A0_MAKE_SYSERR(EPERM);
  }
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;

  bool sat = false;
  a0_err_t err = a0_predicate_eval(pred, &sat);
  if (err | sat) {
    return err;
  }

  lk.transport->_wait_cnt++;

  while (!lk.transport->_shutdown) {
    if (timeout) {
      err = a0_cnd_timedwait(&hdr->cnd, &hdr->mtx, *timeout);
      if (A0_SYSERR(err) == ETIMEDOUT) {
        break;
      }
    } else {
      a0_cnd_wait(&hdr->cnd, &hdr->mtx);
    }

    err = a0_predicate_eval(pred, &sat);
    if (err | sat) {
      break;
    }
  }
  if (!err && lk.transport->_shutdown) {
    err = A0_MAKE_SYSERR(ESHUTDOWN);
  }

  lk.transport->_wait_cnt--;
  a0_cnd_broadcast(&hdr->cnd, &hdr->mtx);

  return err;
}

a0_err_t a0_transport_timedwait(a0_transport_locked_t lk, a0_predicate_t pred, a0_time_mono_t timeout) {
  a0_transport_timedwait_data_t data = (a0_transport_timedwait_data_t){
      .user_pred = pred,
      .timeout = timeout,
  };
  a0_predicate_t full_pred = (a0_predicate_t){
      .user_data = &data,
      .fn = a0_transport_timedwait_predicate,
  };

  return a0_transport_timedwait_impl(lk, full_pred, &timeout);
}

a0_err_t a0_transport_wait(a0_transport_locked_t lk, a0_predicate_t pred) {
  return a0_transport_timedwait_impl(lk, pred, NULL);
}

A0_STATIC_INLINE
a0_err_t a0_transport_empty_pred_fn(void* user_data, bool* out) {
  return a0_transport_empty(*(a0_transport_locked_t*)user_data, out);
}

a0_predicate_t a0_transport_empty_pred(a0_transport_locked_t* lk) {
  return (a0_predicate_t){
      .user_data = lk,
      .fn = a0_transport_empty_pred_fn,
  };
}

A0_STATIC_INLINE
a0_err_t a0_transport_nonempty_pred_fn(void* user_data, bool* out) {
  return a0_transport_nonempty(*(a0_transport_locked_t*)user_data, out);
}

a0_predicate_t a0_transport_nonempty_pred(a0_transport_locked_t* lk) {
  return (a0_predicate_t){
      .user_data = lk,
      .fn = a0_transport_nonempty_pred_fn,
  };
}

A0_STATIC_INLINE
a0_err_t a0_transport_has_next_pred_fn(void* user_data, bool* out) {
  return a0_transport_has_next(*(a0_transport_locked_t*)user_data, out);
}

a0_predicate_t a0_transport_has_next_pred(a0_transport_locked_t* lk) {
  return (a0_predicate_t){
      .user_data = lk,
      .fn = a0_transport_has_next_pred_fn,
  };
}

#ifndef NO_SEQ
a0_err_t a0_transport_seq_low(a0_transport_locked_t lk, SEQ_TYPE* out) {
  a0_transport_state_t* state = a0_transport_working_page(lk);
  *out = state->seq_low;
  return A0_OK;
}
#endif

#ifndef NO_SEQ
a0_err_t a0_transport_seq_high(a0_transport_locked_t lk, SEQ_TYPE* out) {
  a0_transport_state_t* state = a0_transport_working_page(lk);
  *out = state->seq_high;
  return A0_OK;
}
#endif

/*@lxf rename function <a0_transport_read_frame> as <a0_transport_read_frame>.
读取完成一帧数据
*/
a0_err_t a0_transport_read_frame(a0_transport_locked_t lk, a0_transport_frame_t* frame_out) {
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  a0_transport_state_t* state = a0_transport_working_page(lk);
  a0_reader_loc_t* reader_loc = a0_transport_reader_loc(lk);

#if 1
    if(!reader_loc->off)
        return A0_MAKE_SYSERR(ESPIPE);
#else
  if (lk.transport->_seq < state->seq_low) {
    return A0_MAKE_SYSERR(ESPIPE);
  }
#endif
  a0_transport_frame_hdr_t* frame_hdr =
      (a0_transport_frame_hdr_t*)((uint8_t*)hdr + reader_loc->off);

  frame_out->hdr = *frame_hdr;
  frame_out->data = (uint8_t*)frame_hdr + a0_max_align(sizeof(a0_transport_frame_hdr_t));
  reader_loc->stat_count++;

  //可能复位循环写的写指针和所有读指针
  bool committed;
  a0_transport_may_loop_write_read_reset(lk,state,reader_loc,&committed);
  return A0_OK;
}

///@lxf add
a0_err_t a0_transport_tail_frame(a0_transport_locked_t lk, a0_transport_frame_t* frame_out) {
    a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
    a0_transport_state_t* state = a0_transport_working_page(lk);

    a0_transport_frame_hdr_t* frame_hdr =
            (a0_transport_frame_hdr_t*)((uint8_t*)hdr + state->off_tail);

    frame_out->hdr = *frame_hdr;
    frame_out->data = (uint8_t*)frame_hdr + a0_max_align(sizeof(a0_transport_frame_hdr_t));

    return A0_OK;
}

A0_STATIC_INLINE
size_t a0_transport_frame_end(a0_transport_hdr_t* hdr, size_t frame_off) {
  a0_transport_frame_hdr_t* frame_hdr = (a0_transport_frame_hdr_t*)((uint8_t*)hdr + frame_off);
  //lxf modify
  return frame_off + a0_max_align(sizeof(a0_transport_frame_hdr_t)) + a0_max_align(frame_hdr->data_size);
}

A0_STATIC_INLINE
bool a0_transport_frame_intersects(size_t frame1_start,
                                   size_t frame1_size,
                                   size_t frame2_start,
                                   size_t frame2_size) {
  size_t frame1_end = frame1_start + frame1_size;
  size_t frame2_end = frame2_start + frame2_size;
  return (frame1_start < frame2_end) && (frame2_start < frame1_end);
}

A0_STATIC_INLINE
bool a0_transport_head_interval(a0_transport_locked_t lk,
                                a0_transport_state_t* state,
                                size_t* head_off,
                                size_t* head_size) {
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;

  bool empty;
  a0_transport_empty(lk, &empty);
  if (empty) {
    return false;
  }

  *head_off = state->off_head;
  a0_transport_frame_hdr_t* head_hdr = (a0_transport_frame_hdr_t*)((uint8_t*)hdr + *head_off);
  *head_size = a0_max_align(sizeof(a0_transport_frame_hdr_t)) +  a0_max_align(head_hdr->data_size);
  return true;
}

#ifdef READ_LOCK_ONLY_COPY_AREA
A0_STATIC_INLINE
void a0_transport_remove_head(a0_transport_locked_t lk, a0_transport_state_t* state) {
    a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;

    if (state->off_head == state->off_tail) {
        /*只剩下1个frame了：
        此时有1种情况：
        1.state->off_head等于a0_transport_workspace_off()，并且只有1个frame；
        2.head 在上，tail在下，tail下面放不下一帧了，就开始跳到a0_transport_workspace_off()开始删除，
        导致不断地删除head，直到head等于tail。
        */
        a0_transport_invalid_all_readers_loc(lk); // a0_transport_invalid_all_readers_loc(lk);
        state->off_head = 0;//标识无内存数据了
        state->off_tail = 0; //标识无内存数据了
#ifdef ARENA_INCLUDE_TEST_DATA
        hdr->log_reset_write_off_cause = RESET_WRITE_OFF_OF_EVICT;
#endif
        state->high_water_mark = a0_transport_workspace_off();
        //@lxf remove, because always is modifying workingplace.
        //a0_transport_reader_loc_commit(lk);
    } else {
        a0_transport_invalid_some_readers_loc_before(lk, state->off_head);
        a0_transport_frame_hdr_t* head_hdr = (a0_transport_frame_hdr_t*)((uint8_t*)hdr + state->off_head);
        state->off_head = head_hdr->next_off;
        a0_transport_invalid_some_readers_loc_after(lk, state->off_head);

        // Check whether the old head frame was responsible for the high water mark.
        size_t head_end = a0_transport_frame_end(hdr, head_hdr->off);
        if (state->high_water_mark == head_end) {
            // The high water mark is always set by a tail element.
            state->high_water_mark = a0_transport_frame_end(hdr, state->off_tail);
        }
        //@lxf remove, because always is modifying workingplace.
        //a0_transport_reader_loc_commit(lk);
    }
#ifndef NO_SEQ
    state->seq_low++;
#endif

    //@lxf remove, because always is modifying workingplace.
    //a0_transport_commit(lk);
}
#else
A0_STATIC_INLINE
void a0_transport_remove_head(a0_transport_locked_t lk, a0_transport_state_t* state) {
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;

  if (state->off_head == state->off_tail) {
    /*只剩下1个frame了：
    此时有1种情况：
    1.state->off_head等于a0_transport_workspace_off()，并且只有1个frame；
    2.head 在上，tail在下，tail下面放不下一帧了，就开始跳到a0_transport_workspace_off()开始删除，
    导致不断地删除head，直到head等于tail。
    */
    a0_transport_invalid_some_readers_loc(lk, state->off_head); // a0_transport_invalid_all_readers_loc(lk);
    state->off_head = 0;//标识无内存数据了
    state->off_tail = 0; //标识无内存数据了
#ifdef ARENA_INCLUDE_TEST_DATA
    hdr->log_reset_write_off_cause = RESET_WRITE_OFF_OF_EVICT;
#endif
    state->high_water_mark = a0_transport_workspace_off();
    //@lxf remove, because always is modifying workingplace.
    //a0_transport_reader_loc_commit(lk);
  } else {
    a0_transport_invalid_some_readers_loc(lk, state->off_head);
    a0_transport_frame_hdr_t* head_hdr = (a0_transport_frame_hdr_t*)((uint8_t*)hdr + state->off_head);
    state->off_head = head_hdr->next_off;

    // Check whether the old head frame was responsible for the high water mark.
    size_t head_end = a0_transport_frame_end(hdr, head_hdr->off);
    if (state->high_water_mark == head_end) {
      // The high water mark is always set by a tail element.
      state->high_water_mark = a0_transport_frame_end(hdr, state->off_tail);
    }
     //@lxf remove, because always is modifying workingplace.
    //a0_transport_reader_loc_commit(lk);
  }
  #ifndef NO_SEQ
  state->seq_low++;
  #endif

  //@lxf remove, because always is modifying workingplace.
  //a0_transport_commit(lk);
}
#endif

A0_STATIC_INLINE
a0_err_t a0_transport_find_slot(a0_transport_locked_t lk, size_t frame_size, size_t* off, bool* tail_space_full) {
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  a0_transport_state_t* state = a0_transport_working_page(lk);

  *tail_space_full=false;
  bool empty;
  A0_RETURN_ERR_ON_ERR(a0_transport_empty(lk, &empty));

  if (empty) {
    *off = a0_transport_workspace_off();
  } else {
      //lxf modify
    *off = a0_transport_frame_end(hdr, state->off_tail);
    //lxf modify, remove "="
    //if (*off + frame_size >= hdr->arena_size)
    if (*off + frame_size > hdr->arena_size) {
       /*tail到共享内存底部的空间不足，需要从内存顶部开始保存数据*/
      *off = a0_transport_workspace_off();
      *tail_space_full = true;
    }
  }

  if (*off + frame_size > hdr->arena_size) {
    return A0_ERRCODE_TRANSPORT_FRAME_GT_ARENA;
  }

  return A0_OK;
}

A0_STATIC_INLINE
bool a0_transport_evict(a0_transport_locked_t lk, size_t off, size_t frame_size) {
  size_t head_off;
  size_t head_size;
  bool bModified = false; //@lxf add
  a0_transport_state_t* state = a0_transport_working_page(lk);
  while (a0_transport_head_interval(lk, state, &head_off, &head_size) &&
         a0_transport_frame_intersects(off, frame_size, head_off, head_size)) {
    a0_transport_remove_head(lk, state);
    state = a0_transport_working_page(lk);
    if(!bModified)
      bModified=true;
  }

  return bModified;
}

A0_STATIC_INLINE
void a0_transport_slot_init(a0_transport_state_t* state,
                            a0_transport_frame_hdr_t* frame_hdr,
                            size_t off,
                            size_t size) {
  memset(frame_hdr, 0, a0_max_align(sizeof(a0_transport_frame_hdr_t)));

#ifndef NO_SEQ
  frame_hdr->seq = ++state->seq_high;
  if (!state->seq_low) {
    state->seq_low = frame_hdr->seq;
  }
#endif

  frame_hdr->off = off;
  //frame_hdr->next_off = 0;

  frame_hdr->data_size = size;
}

A0_STATIC_INLINE
void a0_transport_maybe_set_head(a0_transport_state_t* state, a0_transport_frame_hdr_t* frame_hdr) {
  if (!state->off_head) {
    state->off_head = frame_hdr->off;
  }
}

A0_STATIC_INLINE
void a0_transport_update_tail(a0_transport_hdr_t* hdr,
                              a0_transport_state_t* state,
                              a0_transport_frame_hdr_t* frame_hdr) {
  if (state->off_tail) {
    a0_transport_frame_hdr_t* tail_frame_hdr =
        (a0_transport_frame_hdr_t*)((uint8_t*)hdr + state->off_tail);
    tail_frame_hdr->next_off = frame_hdr->off;
    frame_hdr->prev_off = state->off_tail;
  }
  state->off_tail = frame_hdr->off;
}

A0_STATIC_INLINE
void a0_transport_update_high_water_mark(a0_transport_hdr_t* hdr,
                                         a0_transport_state_t* state,
                                         a0_transport_frame_hdr_t* frame_hdr) {
  size_t high_water_mark = a0_transport_frame_end(hdr, frame_hdr->off);
  if (state->high_water_mark < high_water_mark) {
    state->high_water_mark = high_water_mark;
  }
}

//lxf modify, 这个函数目前不可用，需要参照a0_transport_alloc修改a0_transport_find_slot的后续处理
a0_err_t a0_transport_alloc_evicts(a0_transport_locked_t lk, size_t size, bool* out) {
  size_t frame_size = a0_max_align(sizeof(a0_transport_frame_hdr_t)) + size;

  size_t off;
  bool tail_space_full;
  //lxf modify, 需要特别注意tail_space_full的后续处理...
  A0_RETURN_ERR_ON_ERR(a0_transport_find_slot(lk, frame_size, &off, &tail_space_full));

  size_t head_off;
  size_t head_size;
  a0_transport_state_t* state = a0_transport_working_page(lk);
  *out = a0_transport_head_interval(lk, state, &head_off, &head_size) &&
         a0_transport_frame_intersects(off, frame_size, head_off, head_size);

  return A0_OK;
}

A0_STATIC_INLINE
a0_err_t a0_transport_find_free_off(a0_transport_locked_t lk, size_t frame_size, size_t* out_off)
{
    if (lk.transport->_arena.mode == A0_ARENA_MODE_READONLY) {
        return A0_MAKE_SYSERR(EPERM);
    }

    bool tail_space_full;
    A0_RETURN_ERR_ON_ERR(a0_transport_find_slot(lk, frame_size, out_off, &tail_space_full));

    a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
    if(tail_space_full)
    {
        a0_transport_state_t* state = a0_transport_working_page(lk);
        if(state->off_tail < state->off_head)
        {
            //tail在上，head在下
            //使区域间的读指针无效
            a0_transport_invalid_some_readers_loc_between(lk, state->off_head, hdr->arena_size);
            state->off_head =a0_transport_workspace_off();
        }else if(state->off_tail == state->off_head)
        {
            //不会出现这种情况,还是做个庸余处理
            //如果state->off_tail上面可以有足够空间容纳数据，则会丢失一条未读的state->off_tail数据
            //assert(state->off_tail == state->off_head);
#ifndef NO_SEQ
            //state->seq_low = 1;
      //state->seq_high = 1;
#endif
            state->off_head = 0; //标识无内存数据了
            state->off_tail = 0; //标识无内存数据了
            state->high_water_mark = a0_transport_workspace_off();
            a0_transport_invalid_all_readers_loc(lk);
        }
    }

#ifdef ARENA_INCLUDE_TEST_DATA
    hdr->log_reset_write_off_cause = RESET_WRITE_OFF_ORDER;
#endif

    //下面的处理仅适用于head在上，tail在下的情况
    if(a0_transport_evict(lk, *out_off, frame_size) || tail_space_full)
    {
        /*在此处之前内存数据区的数据没有发生任何改变，只是预留出了可用空间;
        下面才正式提交了可用空间*/
        a0_transport_reader_loc_commit(lk);
        //此时没有数据写入，不需要广播
        a0_transport_commit_not_broadcast(lk);
    }

    return A0_OK;
}

/*只分配一条数据空间;
为了保证进程崩溃后节点的前后指针（prev_off, next_off）的完整性，不特意修改空闲节点的前后指针为空；
所以不能使用节点的前后指针为空作为判断条件，因为有可能该空的时候却不空；
应该以读写指针为唯一的判断依据。
*/
a0_err_t a0_transport_alloc(a0_transport_locked_t lk, size_t size, a0_transport_frame_t* frame_out) {
  size_t frame_size = a0_max_align(sizeof(a0_transport_frame_hdr_t)) + a0_max_align(size);

  size_t off;
  A0_RETURN_ERR_ON_ERR(a0_transport_find_free_off(lk, frame_size, &off));

  //在此处之前内存数据区的数据没有发生任何改变，只是正式有了可用空间
  // Note: a0_transport_evict commits changes, which invalidates state.
  //       Must grab state afterwards.
  a0_transport_state_t* state = a0_transport_working_page(lk);
  state->write_stat_count++;//只分配一条数据空间

  //开始修改可用空间里的数据,初始化内存数据空间,并且在workspace修改内存数据链表的头尾
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  a0_transport_frame_hdr_t* frame_hdr = (a0_transport_frame_hdr_t*)((uint8_t*)hdr + off);
  a0_transport_slot_init(state, frame_hdr, off, size);

  a0_transport_maybe_set_head(state, frame_hdr);
  a0_transport_update_tail(hdr, state, frame_hdr);
  a0_transport_update_high_water_mark(hdr, state, frame_hdr);

  frame_out->hdr = *frame_hdr;
  frame_out->data = (uint8_t*)hdr + off + a0_max_align(sizeof(a0_transport_frame_hdr_t));

  return A0_OK;
}

/*@lxf add,
分配multiple条数据空间;
为了保证进程崩溃后节点的前后指针（prev_off, next_off）的完整性，不特意修改空闲节点的前后指针为空；
所以不能使用节点的前后指针为空作为判断条件，因为有可能该空的时候却不空；
应该以读写指针为唯一的判断依据。
*/
a0_err_t a0_transport_alloc_multi(a0_transport_locked_t lk, size_t* item_size, size_t item_count, a0_transport_frame_t* out_frames) {
    size_t frame_size = 0;
    for(size_t i=0; i< item_count; i++)
    {
        frame_size+=a0_max_align(sizeof(a0_transport_frame_hdr_t)) + a0_max_align(item_size[i]);
    }

    size_t off;
    A0_RETURN_ERR_ON_ERR(a0_transport_find_free_off(lk, frame_size, &off));

    //在此处之前内存数据区的数据没有发生任何改变，只是正式有了可用空间
    // Note: a0_transport_evict commits changes, which invalidates state.
    //       Must grab state afterwards.
    a0_transport_state_t* state = a0_transport_working_page(lk);
    state->write_stat_count += item_count;//分配multiple条数据空间

    //开始修改可用空间里的数据,初始化内存数据空间,并且在workspace修改内存数据链表的头尾
    a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;

    //set first pkg
    a0_transport_frame_hdr_t* frame_hdr = (a0_transport_frame_hdr_t*)((uint8_t*)hdr + off);
    a0_transport_slot_init(state, frame_hdr, off, item_size[0]);
    a0_transport_maybe_set_head(state, frame_hdr);
    a0_transport_update_tail(hdr, state, frame_hdr);
    out_frames[0].hdr = *frame_hdr;
    out_frames[0].data = (uint8_t*)hdr + off + a0_max_align(sizeof(a0_transport_frame_hdr_t));

    //set the rest of pkgs
    size_t curr_off = off;
    for(int i=1; i<item_count; i++)
    {
        curr_off += a0_max_align(sizeof(a0_transport_frame_hdr_t)) + a0_max_align(frame_hdr->data_size);
        frame_hdr = (a0_transport_frame_hdr_t*)((uint8_t*)hdr + curr_off);
        a0_transport_slot_init(state, frame_hdr, curr_off, item_size[i]);
        a0_transport_update_tail(hdr, state, frame_hdr);
        out_frames[i].hdr = *frame_hdr;
        out_frames[i].data = (uint8_t*)hdr + curr_off + a0_max_align(sizeof(a0_transport_frame_hdr_t));
    }
    a0_transport_update_high_water_mark(hdr, state, frame_hdr);

    return A0_OK;
}

A0_STATIC_INLINE
a0_err_t a0_transport_allocator_impl(void* user_data, size_t size, a0_buf_t* buf_out) {
  a0_transport_frame_t frame;
  A0_RETURN_ERR_ON_ERR(a0_transport_alloc(*(a0_transport_locked_t*)user_data, size, &frame));
  buf_out->ptr = frame.data;
  buf_out->size = frame.hdr.data_size;
  return A0_OK;
}

///@lxf add
A0_STATIC_INLINE
a0_err_t a0_transport_allocator_multi_impl(void* user_data, size_t* item_size, \
                                            size_t item_count, a0_buf_t* outs) {
    a0_transport_frame_t frames[item_count];
    A0_RETURN_ERR_ON_ERR(a0_transport_alloc_multi(*(a0_transport_locked_t*)user_data, item_size,\
                                                        item_count, frames));

    for(size_t i=0; i<item_count; i++){
        outs[i].ptr = frames[i].data;
        outs[i].size = frames[i].hdr.data_size;
    }
    return A0_OK;
}

a0_err_t a0_transport_allocator(a0_transport_locked_t* lk, a0_alloc_t* alloc_out) {
  alloc_out->user_data = lk;
  alloc_out->alloc = a0_transport_allocator_impl;
  alloc_out->dealloc = NULL;
  return A0_OK;
}

///lxf add
a0_err_t a0_transport_allocator_multi(a0_transport_locked_t* lk, a0_alloc_multi_t* alloc_out) {
    alloc_out->user_data = lk;
    alloc_out->alloc = a0_transport_allocator_multi_impl;
    alloc_out->dealloc = NULL;
    return A0_OK;
}

a0_err_t a0_transport_used_space(a0_transport_locked_t lk, size_t* out) {
  a0_transport_state_t* state = a0_transport_working_page(lk);
  *out = state->high_water_mark;
  return A0_OK;
}

a0_err_t a0_transport_resize(a0_transport_locked_t lk, size_t arena_size) {
  size_t used_space;
  A0_RETURN_ERR_ON_ERR(a0_transport_used_space(lk, &used_space));
  if (arena_size < used_space) {
    return A0_ERRCODE_INVALID_ARG;
  }

  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  hdr->arena_size = arena_size;
  return A0_OK;
}

A0_STATIC_INLINE
void write_limited(FILE* f, a0_buf_t str) {
  size_t line_size = str.size;
  bool overflow = false;
  if (line_size > 32) {
    line_size = 29;
    overflow = true;
  }
  fwrite(str.ptr, sizeof(char), line_size, f);
  if (overflow) {
    fprintf(f, "...");
  }
}

///@lxf add
A0_STATIC_INLINE  
size_t a0_transport_framesize(a0_transport_frame_hdr_t* frame)
{
  return a0_max_align(sizeof(a0_transport_frame_hdr_t)) + a0_max_align(frame->data_size);
}

///@lxf add
A0_STATIC_INLINE
a0_err_t a0_transport_has_next_quick( size_t off, a0_transport_state_t* state, bool* out) {
  #if 1
  *out =  off != state->off_tail;
  #else
  *out = seq < state->seq_high;
  #endif
  return A0_OK;
}

///@lxf add
void a0_alter_frameoffs_relative_first_frame(a0_transport_frame_hdr_t* frame_hdr, size_t framecount)
{
    size_t first_frame_off=frame_hdr->off;
    size_t curr_off = 0;
    a0_transport_frame_hdr_t* curr_frame_hdr;
    for(int i=0; i<framecount; i++)
    {
        curr_frame_hdr =(a0_transport_frame_hdr_t* )((uint8_t*)frame_hdr + curr_off);
        curr_frame_hdr->off -= first_frame_off;

        //if(curr_frame_hdr->prev_off)
        curr_frame_hdr->prev_off -= first_frame_off;

        //if(curr_frame_hdr->next_off)
        curr_frame_hdr->next_off -= first_frame_off;

        curr_off = curr_frame_hdr->next_off;
    }
};

#ifdef READ_LOCK_ONLY_COPY_AREA
a0_err_t a0_transport_handle_curr_multi_one_pkt(a0_transport_locked_t lk, size_t maxsize,\
                                                a0_off_t* out_copy_off, size_t* out_write_tail_off) {
    a0_transport_state_t* state = a0_transport_working_page(lk);
    *out_write_tail_off = state->off_tail;

    a0_reader_loc_t* reader_loc = a0_transport_reader_loc(lk);
#ifndef NO_SEQ
    SEQ_TYPE seq, transp_seq;
#endif
    size_t off, first_frame_off;
    a0_transport_frame_hdr_t *curr_frame_hdr;

    a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
    //get curent frame seq
    if (!reader_loc->off) {
        // update to first data, get new seq
        off = state->off_head;
        reader_loc->off = off;
    }else{
        off = reader_loc->off;
    }

    curr_frame_hdr =
            (a0_transport_frame_hdr_t*)((uint8_t*)hdr + off);
#ifndef NO_SEQ
    seq = curr_frame_hdr->seq;
#endif

    //found one frame
#ifndef NO_SEQ
    transp_seq = seq;
#endif
    //transp_off = off;
    first_frame_off = off;

    size_t copy_off_end;
    size_t off_tail = 0;
    if(off <= state->off_tail){
        //沿着地址顺序向下读
        copy_off_end = a0_transport_frame_end(hdr, state->off_tail);
        off_tail = copy_off_end;
    }else{
        //需要环绕才能读，所以最多只读到shm的底部
        copy_off_end = hdr->arena_size;
    }
    size_t copy_size = MIN(maxsize, copy_off_end - off);
    bool read_to_tail = false;
    if(off + copy_size == off_tail) {
        //读到写指针的尾部了
        read_to_tail = true;
        //这里很特别，提前修改了读指针，只为了可能的复原写指针
        reader_loc->off = state->off_tail;
    }
    out_copy_off->off = off;
    out_copy_off->size =  copy_size;

    //更新读指针
    reader_loc->next_copy_off = off + copy_size;

#if 1
    //更新读指针, 下面的代码与函数a0_transport_step_next_multi相同
#ifndef NO_SEQ
    lk.transport->_seq = transp_seq;
#endif
    //reader_loc->stat_count++;
#endif

    //可能复位循环写的写指针和所有读指针, but lxf too early, not copy now, to modify....
    bool committed = false;
    if(read_to_tail) {
        //读到写指针的尾部了，此时需要检查其他指针是否也读完了。
        a0_transport_may_loop_write_read_reset(lk,state,reader_loc,&committed);
    }
    if(!committed){
        a0_transport_reader_loc_commit(lk);
    }
}
#else/*READ_LOCK_ONLY_COPY_AREA*/
/*@lxf add,读取当前一条数据,这个是用于在刚启动时读第一条数据;
为了维护方便，变量名字和流程与函数a0_transport_step_next_multi尽量相同；
修改时，需要同时修改函数a0_transport_step_next_multi,有些代码直接复制过去即可;
如果当前帧大于缓存时，此处不做处理，但是在复制数据到缓存时会丢弃该帧, 因为此处并没有真正的复制数据.
*/
void a0_transport_handle_curr_multi_one_pkt(a0_transport_locked_t lk, size_t maxsize, size_t maxcount,\
                                      a0_off_t* out_frame_off, size_t* out_frame_count) {
    *out_frame_count = 0;
    a0_transport_state_t* state = a0_transport_working_page(lk);

    a0_reader_loc_t* reader_loc = a0_transport_reader_loc(lk);
    size_t bytecount=0;
    #ifndef NO_SEQ
    SEQ_TYPE seq, transp_seq;
    #endif
    size_t off, first_frame_off;
    a0_transport_frame_hdr_t *curr_frame_hdr;

    a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
    //get curent frame seq
    if (!reader_loc->off) {
        // update to first data, get new seq
        off = state->off_head;
        reader_loc->off = off;
    }else{
        off = reader_loc->off;
    }

    curr_frame_hdr =
            (a0_transport_frame_hdr_t*)((uint8_t*)hdr + off);
    #ifndef NO_SEQ
    seq = curr_frame_hdr->seq;
    #endif
    bytecount = a0_transport_framesize(curr_frame_hdr);

    //found one frame
    //itemcount++;
    #ifndef NO_SEQ
    transp_seq = seq;
    #endif
    //transp_off = off;
    //first_frame_off = off;

    out_frame_off->off = off;//first_frame_off;
    out_frame_off->size = bytecount;
    *out_frame_count = 1;//itemcount;

    //更新读指针, 下面的代码与函数a0_transport_step_next_multi相同
    #ifndef NO_SEQ
    lk.transport->_seq = transp_seq;
    #endif
    reader_loc->off = off;
    reader_loc->stat_count++;

    //可能复位循环写的写指针和所有读指针, but lxf too early, not copy now, to modify....
    bool committed;
    a0_transport_may_loop_write_read_reset(lk,state,reader_loc,&committed);
    if(!committed){
        a0_transport_reader_loc_commit(lk);
    }
}
#endif

#ifdef READ_LOCK_ONLY_COPY_AREA
a0_err_t a0_transport_update_copy_read_off(a0_transport_locked_t lk, size_t frames_count) {
    a0_transport_state_t* state = a0_transport_working_page(lk);

    a0_reader_loc_t* reader_loc = a0_transport_reader_loc(lk);
    if(A0_READ_COPY_NO_FRAME == lk.transport->_read_result){
        //上次没有拷贝出一个帧，因为cache不够大，所以这次需要丢弃上一次的帧，从下一帧开始拷贝
    }else{
        //更新当前的读指针
        if(reader_loc->off){
            if(reader_loc->off != lk.transport->_read_off){
                reader_loc->off = lk.transport->_read_off;

                //如果没有reader_loc->stat_count，则需要下面的commit
                //a0_transport_reader_loc_commit(lk);
            }
        }

#ifdef ARENA_INCLUDE_TEST_DATA
        a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
        hdr->log_write_pkts_total_count++;
#endif
        reader_loc->stat_count += frames_count;
        //如果没有reader_loc->stat_count，则不需要下面的commit
        a0_transport_reader_loc_commit(lk);
    }

    return A0_OK;
}

/*@lxf add,读取当前的下一条的连续多条数据;
修改时，需要同时修改函数a0_transport_step_curr_for_multi,有些代码直接复制过去即可;
如果当前帧大于缓存时，此处不做处理，但是在复制数据到缓存时会丢弃该帧, 因为此处并没有真正的复制数据.
*/
a0_err_t a0_transport_handle_next_multi_pkts(a0_transport_locked_t lk, size_t maxsize,\
                                                a0_off_t* out_copy_off, size_t* out_write_tail_off) {
  bool has_next;
  A0_RETURN_ERR_ON_ERR(a0_transport_has_next(lk, &has_next));
  if (!has_next) {
    return A0_ERRCODE_TRANSPORT_CANNOT_MOVE_POINTER;
  }

  a0_transport_state_t* state = a0_transport_working_page(lk);
  a0_reader_loc_t* reader_loc = a0_transport_reader_loc(lk);
  *out_write_tail_off = state->off_tail;

  //get next frame
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  size_t off = lk.transport->_read_off;
  a0_transport_frame_hdr_t *curr_frame_hdr, *next_frame_hdr;
  if (!off) {
      // update to first seq, get new seq
      off = state->off_head;
      reader_loc->off = off;
      curr_frame_hdr = (a0_transport_frame_hdr_t*)((uint8_t*)hdr + off);
    #ifndef NO_SEQ
      seq = curr_frame_hdr->seq;
    #endif
  }
  else{
        //get new seq
        curr_frame_hdr =
                (a0_transport_frame_hdr_t*)((uint8_t*)hdr + off);
        off = curr_frame_hdr->next_off;
        reader_loc->off = off;

        next_frame_hdr = (a0_transport_frame_hdr_t*)((uint8_t*)hdr + off);
    #ifndef NO_SEQ
        seq = next_frame_hdr->seq;
    #endif
  }

    size_t copy_off_end;
    size_t off_tail = 0;
    if(off <= state->off_tail){
        //沿着地址顺序向下读
        copy_off_end = a0_transport_frame_end(hdr, state->off_tail);
        off_tail = copy_off_end;
    }else{
        //需要环绕才能读，所以最多只读到shm的底部
        copy_off_end = hdr->arena_size;
    }
    size_t copy_size = MIN(maxsize, copy_off_end - off);
    bool read_to_tail = false;
    if(off + copy_size == off_tail) {
        //读到写指针的尾部了
        read_to_tail = true;
        //这里很特别，提前修改了读指针，只为了可能的复原写指针
        reader_loc->off = state->off_tail;
    }
    out_copy_off->off = off;
    out_copy_off->size =  copy_size;

    //更新读指针
    reader_loc->next_copy_off = off + copy_size;

#if 1

   //更新读指针, 下面的代码与函数a0_transport_step_curr_for_multi相同
  #ifndef NO_SEQ
    lk.transport->_seq = transp_seq;
  #endif
    //reader_loc->stat_count += itemcount;

#endif

    bool committed = false;
    //可能复位循环写的写指针和所有读指针, but lxf too early, not copy now, to modify....
    if(read_to_tail) {
        //读到写指针的尾部了，此时需要检查其他指针是否也读完了。
        a0_transport_may_loop_write_read_reset(lk,state,reader_loc,&committed);
    }
    if(!committed){
        a0_transport_reader_loc_commit(lk);
    }

    return A0_OK;
}
#else/*READ_LOCK_ONLY_COPY_AREA*/
/*@lxf add,读取当前的下一条的连续多条数据;
修改时，需要同时修改函数a0_transport_step_curr_for_multi,有些代码直接复制过去即可;
如果当前帧大于缓存时，此处不做处理，但是在复制数据到缓存时会丢弃该帧, 因为此处并没有真正的复制数据.
*/
a0_err_t a0_transport_handle_next_multi_pkts(a0_transport_locked_t lk, size_t maxsize, size_t maxcount,\
                                      a0_off_t* out_frame_off, size_t* out_frame_count) {
  *out_frame_count = 0;
  a0_transport_state_t* state = a0_transport_working_page(lk);

  bool has_next;
  A0_RETURN_ERR_ON_ERR(a0_transport_has_next(lk, &has_next));
  if (!has_next) {
    return A0_ERRCODE_TRANSPORT_CANNOT_MOVE_POINTER;
  }

  a0_reader_loc_t* reader_loc = a0_transport_reader_loc(lk);
  size_t bytecount=0, itemcount=0;
  #ifndef NO_SEQ
  SEQ_TYPE seq, next_seq, transp_seq;
  #endif
  size_t transp_off,off, first_frame_off, next_off;
  a0_transport_frame_hdr_t *curr_frame_hdr, *next_frame_hdr;

  //get next frame
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;
  if (!reader_loc->off) {
    // update to first seq, get new seq
    off = state->off_head;
    //reader_loc->off = off;
    curr_frame_hdr =
        (a0_transport_frame_hdr_t*)((uint8_t*)hdr + off);
   #ifndef NO_SEQ
    seq = curr_frame_hdr->seq;
  #endif
    bytecount = a0_transport_framesize(curr_frame_hdr);
  }
  else{
    //get new seq
    off = reader_loc->off;
    curr_frame_hdr =
      (a0_transport_frame_hdr_t*)((uint8_t*)hdr + off);
    off = curr_frame_hdr->next_off;

    next_frame_hdr =
          (a0_transport_frame_hdr_t*)((uint8_t*)hdr + off);
    #ifndef NO_SEQ
    seq = next_frame_hdr->seq;
    #endif
    bytecount = a0_transport_framesize(next_frame_hdr);
  }

  //found one frame
  itemcount++;
  #ifndef NO_SEQ
  transp_seq = seq;
  #endif
  transp_off = off;
  first_frame_off = off;

  //continue to find next frame
  while(bytecount < maxsize && itemcount < maxcount\
        && A0_OK == a0_transport_has_next_quick(off,state,&has_next) && has_next)
   {
      //next frame is exist
      curr_frame_hdr =
      (a0_transport_frame_hdr_t*)((uint8_t*)hdr + off);
      //get next frame off
      next_off = curr_frame_hdr->next_off;
      if(next_off < off) //include next_off=0
      {
        //cycle to above the off, or next_off=0(no next frame), break
        break;
      }

      //found next frame
       next_frame_hdr =
               (a0_transport_frame_hdr_t*)((uint8_t*)hdr + next_off);
#ifndef NO_SEQ
       next_seq = next_frame_hdr->seq;
#endif

      size_t val = next_off - first_frame_off + a0_transport_framesize(next_frame_hdr);
      if(val > maxsize)
          break;
      bytecount = val;
      itemcount++;
#ifndef NO_SEQ
      transp_seq = next_seq;
#endif
      transp_off = next_off;

      //prepare next decide
      off=next_off;
      //reader_loc->off = off;
      #ifndef NO_SEQ
      seq=next_seq;
      #endif
    }

    out_frame_off->off = first_frame_off;
    out_frame_off->size = bytecount;
    *out_frame_count = itemcount;

#ifdef ARENA_INCLUDE_TEST_DATA
    hdr->log_write_pkts_total_count += itemcount;
#endif

   //更新读指针, 下面的代码与函数a0_transport_step_curr_for_multi相同
  #ifndef NO_SEQ
    lk.transport->_seq = transp_seq;
  #endif
    reader_loc->off = transp_off;
    reader_loc->stat_count += itemcount;

    //可能复位循环写的写指针和所有读指针, but lxf too early, not copy now, to modify....
    bool committed;
    a0_transport_may_loop_write_read_reset(lk,state,reader_loc,&committed);
    if(!committed){
        a0_transport_reader_loc_commit(lk);
    }

  return A0_OK;
}
#endif

//lxf remove, not use
#if 0
void a0_transport_debugstr(a0_transport_locked_t lk, a0_buf_t* out) {
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)lk.transport->_arena.buf.ptr;

  a0_transport_state_t* committed_state = a0_transport_committed_page(lk);
  a0_transport_state_t* working_state = a0_transport_working_page(lk);

  FILE* ss = open_memstream((char**)&out->ptr, &out->size);
  // clang-format off
  fprintf(ss, "\n{\n");
  fprintf(ss, "  \"header\": {\n");
  fprintf(ss, "    \"arena_size\": %lu,\n", hdr->arena_size);
  fprintf(ss, "    \"committed_state\": {\n");

  fprintf(ss, "      \"seq_low\": %lu,\n", committed_state->seq_low);
  fprintf(ss, "      \"seq_high\": %lu,\n", committed_state->seq_high);

  fprintf(ss, "      \"off_head\": %lu,\n", committed_state->off_head);
  fprintf(ss, "      \"off_tail\": %lu,\n", committed_state->off_tail);
  fprintf(ss, "      \"high_water_mark\": %lu\n", committed_state->high_water_mark);
  fprintf(ss, "    },\n");
  fprintf(ss, "    \"working_state\": {\n");


  fprintf(ss, "      \"seq_low\": %lu,\n", working_state->seq_low);
  fprintf(ss, "      \"seq_high\": %lu,\n", working_state->seq_high);

  fprintf(ss, "      \"off_head\": %lu,\n", working_state->off_head);
  fprintf(ss, "      \"off_tail\": %lu,\n", working_state->off_tail);
  fprintf(ss, "      \"high_water_mark\": %lu\n", working_state->high_water_mark);
  fprintf(ss, "    }\n");
  fprintf(ss, "  },\n");
  fprintf(ss, "  \"data\": [\n");
  // clang-format on

  if (working_state->off_head) {
    size_t off = working_state->off_head;
    bool first = true;
    while (true) {
      a0_transport_frame_hdr_t* frame_hdr = (a0_transport_frame_hdr_t*)((uint8_t*)hdr + off);
      SEQ_TYPE seq = frame_hdr->seq;

      if (!first) {
        fprintf(ss, "    },\n");
      }
      first = false;

      fprintf(ss, "    {\n");
      if (seq > committed_state->seq_high) {
        fprintf(ss, "      \"committed\": false,\n");
      }
      fprintf(ss, "      \"off\": %lu,\n", frame_hdr->off);
      fprintf(ss, "      \"seq\": %lu,\n", frame_hdr->seq);
      fprintf(ss, "      \"prev_off\": %lu,\n", frame_hdr->prev_off);
      fprintf(ss, "      \"next_off\": %lu,\n", frame_hdr->next_off);
      fprintf(ss, "      \"data_size\": %lu,\n", frame_hdr->data_size);
      a0_buf_t data = {
          .ptr = (uint8_t*)hdr + frame_hdr->off + a0_max_align(sizeof(a0_transport_frame_hdr_t)),
          .size = frame_hdr->data_size,
      };
      fprintf(ss, "      \"data\": \"");
      write_limited(ss, data);
      fprintf(ss, "\"\n");

      off = frame_hdr->next_off;

      if (seq == working_state->seq_high) {
        fprintf(ss, "    }\n");
        break;
      }
    }
  }
  fprintf(ss, "  ]\n");
  fprintf(ss, "}\n");
  fflush(ss);
  fclose(ss);
}
#endif

//@lxf add
void a0_transport_debugstr_states(a0_transport_locked_t fake_lk,FILE *ss) {
    //convert state to text
    a0_transport_states_area_t *states = a0_transport_states_area(fake_lk);
    a0_transport_state_t *committed_state = a0_transport_committed_page(fake_lk);
    a0_transport_state_t *working_state = a0_transport_working_page(fake_lk);

    int working_print_index = 0;
    a0_transport_state_t *state;
    for (int i = 0; i < 2; i++) {
        if (i == working_print_index) {
            state = working_state;
            fprintf(ss, "    \"data working_state\": {\n");
        } else {
            state = committed_state;
            fprintf(ss, "    \"data committed_state [%d]\": {\n", states->committed_page_idx);
        }

#ifndef NO_SEQ
        fprintf(ss, "      \"seq_low\": %lu, \"seq_high\": %lu,\n", state->seq_low, state->seq_high);
#endif
        fprintf(ss, "      \"write_stat_count\": %lu, \"off_head\": %lu, \"off_tail\": %lu, \"high_water_mark\": %lu\n",
                state->write_stat_count, state->off_head, state->off_tail, state->high_water_mark);
        fprintf(ss, "    },\n\n");

    }
}

//@lxf add
A0_STATIC_INLINE
void a0_transport_debugstr_user_key(a0_user_key_vec_t *key_vec, size_t count, FILE *ss) {
    a0_user_key_t* key;
    for(int j=0; j<count; j++)
    {
        key = &key_vec->impl[j];
        fprintf(ss, "      [%d] \"name\": %s, \"ref_count\": %lu, \"occupy_times\": %d"
                    ", \"tv_sec\": %lu, \"tv_nsec\": %lu\n",
                j, key->name, key->ref_count, key->occupy_times,
                key->tm_mono.ts.tv_sec,key->tm_mono.ts.tv_nsec);
    }
}

//@lxf add
void a0_transport_debugstr_reader_key_states(a0_transport_locked_t fake_lk,FILE *ss) {
    a0_user_key_states_t *key_states = a0_transport_reader_key_states(fake_lk);
    a0_user_key_vec_t *committed_key_vec = a0_transport_committed_reader_key_page(fake_lk);
    a0_user_key_vec_t *working_key_vec = a0_transport_working_reader_key_page(fake_lk);

    int working_print_index = 0;
    a0_user_key_vec_t *key_vec;
    a0_user_key_t* key;
    for (int i = 0; i < 2; i++) {
        if (i == working_print_index) {
            key_vec = working_key_vec;
            fprintf(ss, "    \"reader working_key_state\": {\n");
        } else {
            key_vec = committed_key_vec;
            fprintf(ss, "    \"reader committed_key_state [%d]\": {\n", key_states->committed_page_idx);
        }

        fprintf(ss, "    \"reader_users [%d]\": {\n", MAX_READ_USER_COUNT);
        a0_transport_debugstr_user_key(key_vec,MAX_READ_USER_COUNT,ss);
        fprintf(ss, "   },\n\n");
    }
    fprintf(ss, "  },\n\n");
}

//@lxf add
void a0_transport_debugstr_writer_key_states(a0_transport_locked_t fake_lk,FILE *ss) {
    a0_user_key_states_t *key_states = a0_transport_writer_key_states(fake_lk);
    a0_user_key_vec_t *committed_key_vec = a0_transport_committed_writer_key_page(fake_lk);
    a0_user_key_vec_t *working_key_vec = a0_transport_working_writer_key_page(fake_lk);

    int working_print_index = 0;
    a0_user_key_vec_t *key_vec;
    a0_user_key_t* key;
    for (int i = 0; i < 2; i++) {
        if (i == working_print_index) {
            key_vec = working_key_vec;
            fprintf(ss, "    \"writer working_key_state\": {\n");
        } else {
            key_vec = committed_key_vec;
            fprintf(ss, "    \"writer committed_key_state [%d]\": {\n", key_states->committed_page_idx);
        }

        fprintf(ss, "    \"writer_users [%d]\": {\n", MAX_WRITE_USER_COUNT);
        a0_transport_debugstr_user_key(key_vec,MAX_WRITE_USER_COUNT,ss);
        fprintf(ss, "   },\n\n");
    }
    fprintf(ss, "  },\n\n");
}

//@lxf add
A0_STATIC_INLINE
void a0_transport_debugstr_user_hdr_detail(a0_transport_user_hdr_t* user_hdr, FILE *ss) {
    fprintf(ss, "      \"max_user_count\": %lu, \"version\": %d.%d.%d\n",
            user_hdr->max_user_count, user_hdr->version.major, user_hdr->version.minor, user_hdr->version.patch);
}

//@lxf add
void a0_transport_debugstr_reader_hdr(a0_transport_locked_t fake_lk,FILE *ss) {
    a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)fake_lk.transport->_arena.buf.ptr;
    a0_transport_user_hdr_t* user_hdr = (a0_transport_user_hdr_t*)((uint8_t*)hdr \
                                                    +a0_transport_reader_hdr_off());

    fprintf(ss, "    \"reader_hdr\": {\n");
    a0_transport_debugstr_user_hdr_detail(user_hdr, ss);
    fprintf(ss, "   },\n\n");
}

//@lxf add
void a0_transport_debugstr_writer_hdr(a0_transport_locked_t fake_lk,FILE *ss) {
    a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)fake_lk.transport->_arena.buf.ptr;
    a0_transport_user_hdr_t* user_hdr = (a0_transport_user_hdr_t*)((uint8_t*)hdr \
                                                    +a0_transport_writer_hdr_off());

    fprintf(ss, "    \"writer_hdr\": {\n");
    a0_transport_debugstr_user_hdr_detail(user_hdr, ss);
    fprintf(ss, "   },\n\n");
}

//@lxf add
void a0_transport_debugstr_reader_loc_states(a0_transport_locked_t fake_lk,FILE *ss) {
    a0_reader_loc_states_t *loc_states = a0_transport_reader_loc_states(fake_lk);
    a0_reader_loc_vec_t *committed_loc_vec = a0_transport_committed_reader_loc_page(fake_lk);
    a0_reader_loc_vec_t *working_loc_vec = a0_transport_working_reader_loc_page(fake_lk);

    int working_print_index = 0;
    a0_reader_loc_vec_t *loc_vec;
    a0_reader_loc_t* loc;
    for (int i = 0; i < 2; i++) {
        if(i == working_print_index){
            loc_vec = working_loc_vec;
            fprintf(ss, "    \"reader working_loc_state\": {\n");
        }else{
            loc_vec = committed_loc_vec;
            fprintf(ss, "    \"reader committed_loc_state [%d]\": {\n", loc_states->committed_page_idx);
        }

        fprintf(ss, "    \"reader_users [%d]\": {\n", MAX_READ_USER_COUNT);
        for(int j=0; j<MAX_READ_USER_COUNT; j++)
        {
            loc = &loc_vec->impl[j];
            fprintf(ss, "      [%d] \"off\": %lu, \"stat_count\": %lu, \"keep_alive\": %lu\n",
                    j, loc->off, loc->stat_count, loc->keep_alive);
        }
        fprintf(ss, "   },\n\n");
    }
    fprintf(ss, "  },\n\n");
}

//@lxf add
void a0_transport_debugstr_arena_hdr(a0_transport_locked_t fake_lk,FILE *ss) {
    a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)fake_lk.transport->_arena.buf.ptr;
    fprintf(ss, "\n{\n");
    fprintf(ss, "  \"arena_header\": {\n");
    fprintf(ss, "    \"arena_size\": %s, \"arena_size\": %lu, \"init\": %d \n", hdr->magic, hdr->arena_size,hdr->initialized);
    fprintf(ss, "    \"mtx\": %d, \"cnd\": %d\n", hdr->mtx.ftx, hdr->cnd);
    fprintf(ss, "    \"version\": %d.%d.%d\n", hdr->version.major, hdr->version.minor, hdr->version.patch);

    //log, test data.....
#ifdef ARENA_INCLUDE_TEST_DATA
    const char* reset_write_cause[RESET_WRITE_OFF_MAX] ={
            "None write",
            "order write",
            "Evict write",
            "All read",
            "error value"
    };

    if(hdr->log_reset_write_off_cause >= RESET_WRITE_OFF_MAX )
        hdr->log_reset_write_off_cause = RESET_WRITE_ERROR;
    fprintf(ss, "    \"log_reset_write_off_cause\": %s, \"log_write_off_reset_count\": %lu, \"log_write_pkts_total_count\": %lu\n",
            reset_write_cause[hdr->log_reset_write_off_cause], hdr->log_write_off_reset_count, hdr->log_write_pkts_total_count);
#endif
    fprintf(ss, "}\n\n");
}

//@lxf add
void a0_transport_debugstr_reader_speed(a0_transport_locked_t fake_lk, a0_time_mono_t tm_now, FILE *ss) {
    a0_user_key_vec_t *committed_key_vec = a0_transport_committed_reader_key_page(fake_lk);
    a0_user_key_vec_t *working_key_vec = a0_transport_working_reader_key_page(fake_lk);

    a0_reader_loc_vec_t *committed_loc_vec = a0_transport_committed_reader_loc_page(fake_lk);
    a0_reader_loc_vec_t *working_loc_vec = a0_transport_working_reader_loc_page(fake_lk);

    //data block count every ms.
    size_t speed[2][MAX_READ_USER_COUNT] ={0};

    int working_print_index = 0;
    a0_user_key_vec_t* key_vec;
    a0_reader_loc_vec_t* loc_vec;
    for (int i = 0; i < 2; i++) {
        if(i == working_print_index){
            key_vec = working_key_vec;
            loc_vec = working_loc_vec;
            fprintf(ss, "    \"reader working_speed(item_count/s) [%d]\": {\n", MAX_READ_USER_COUNT);
        }else{
            key_vec = committed_key_vec;
            loc_vec = committed_loc_vec;
            fprintf(ss, "    \"reader committed_speed(item_count/s) [%d]\": {\n", MAX_READ_USER_COUNT);
        }

        for(int j=0; j<MAX_READ_USER_COUNT; j++)
        {
            if('\0'!=key_vec->impl[j].name[0]){
                size_t cost;
                a0_time_mono_difftime(key_vec->impl[j].tm_mono, tm_now, &cost);
                if(0 != cost){
                    speed[i][j] = (loc_vec->impl[j].stat_count / (double)cost) * 1000;
                }
            }

            if(0 != speed[i][j]){
                fprintf(ss, "    [%d] \"speed\": %lu\n", j, speed[i][j]);
            }else{
                fprintf(ss, "    [%d] \"speed\": N/A\n", j);
            }
        }
        fprintf(ss, "   },\n");
        fprintf(ss,"     [now]  \"tv_sec\": %lu, \"tv_nsec\": %lu\n\n",tm_now.ts.tv_sec, tm_now.ts.tv_nsec);
    }
}

//@lxf add
void a0_transport_debugstr_writer_speed(a0_transport_locked_t fake_lk, a0_time_mono_t tm_now,  FILE *ss) {
    a0_user_key_vec_t *committed_key_vec = a0_transport_committed_writer_key_page(fake_lk);
    a0_user_key_vec_t *working_key_vec = a0_transport_working_writer_key_page(fake_lk);

    a0_transport_state_t* committed_state = a0_transport_committed_page(fake_lk);
    a0_transport_state_t* working_state = a0_transport_working_page(fake_lk);

    //data block count every ms.
    size_t speed[2][MAX_WRITE_USER_COUNT] ={0};

    int working_print_index = 0;
    a0_user_key_vec_t* key_vec;
    a0_transport_state_t* state;
    for (int i = 0; i < 2; i++) {
        if(i==working_print_index){
            key_vec = working_key_vec;
            state = working_state;
            fprintf(ss, "    \"writer working_speed(item_count/s) [%d]\": {\n", MAX_WRITE_USER_COUNT);
        }else{
            key_vec = committed_key_vec;
            state = committed_state;
            fprintf(ss, "    \"writer committed_speed(item_count/s) [%d]\": {\n", MAX_WRITE_USER_COUNT);
        }

        for(int j=0; j<MAX_WRITE_USER_COUNT; j++)
        {
            if('\0'!=key_vec->impl[j].name[0]){
                size_t cost;
                a0_time_mono_difftime(key_vec->impl[j].tm_mono, tm_now, &cost);
                if(0 != cost){
                    speed[i][j] = (state->write_stat_count / (double)cost) * 1000;
                }
            }

            if(0 != speed[i][j]){
                fprintf(ss, "    [%d] \"speed\": %lu\n", j, speed[i][j]);
            }else{
                fprintf(ss, "    [%d] \"speed\": N/A\n", j);
            }
        }
        fprintf(ss, "   },\n");
        fprintf(ss,"     [now]  \"tv_sec\": %lu, \"tv_nsec\": %lu\n\n",tm_now.ts.tv_sec, tm_now.ts.tv_nsec);
    }
}

//@lxf add
a0_err_t a0_transport_debugstr_reader_update_time_count(a0_transport_locked_t lk, a0_time_mono_t tm_now) {
    a0_user_key_vec_t *key_vec = a0_transport_working_reader_key_page(lk);
    a0_reader_loc_vec_t *loc_vec = a0_transport_working_reader_loc_page(lk);

    for(int i=0; i<MAX_READ_USER_COUNT; i++){
        if('\0'!=key_vec->impl[i].name[0]){
            //update key time, stat_count
            a0_time_mono_now(&key_vec->impl[i].tm_mono);
            loc_vec->impl[i].stat_count = 0;
        }
    }

    a0_transport_reader_loc_commit(lk);
    a0_transport_reader_key_commit(lk);
    return A0_OK;
}

//@lxf add
a0_err_t a0_transport_debugstr_writer_update_time_count(a0_transport_locked_t lk, a0_time_mono_t tm_now) {
    a0_user_key_vec_t *key_vec = a0_transport_working_writer_key_page(lk);
    a0_transport_state_t* state = a0_transport_working_page(lk);

    for(int i=0; i<MAX_WRITE_USER_COUNT; i++)
    {
        if('\0'!=key_vec->impl[i].name[0]){
            //update key time, count
            a0_time_mono_now(&key_vec->impl[i].tm_mono);
        }
    }

    state->write_stat_count = 0;
    a0_transport_writer_key_commit(lk);
    a0_transport_commit_not_broadcast(lk);
    return A0_OK;
}

//a0_transport_update_reader_mono_time
//@lxf add
void a0_transport_debugstr_arena_head(a0_buf_t arena_head, a0_buf_t* out) {
  a0_transport_hdr_t* hdr = (a0_transport_hdr_t*)arena_head.ptr;
  a0_time_mono_t tm_now;
  a0_time_mono_now(&tm_now);

  FILE* ss = open_memstream((char**)&out->ptr, &out->size);

  a0_transport_t transport;
  a0_transport_locked_t fake_lk={.transport=&transport};
  transport._arena.buf = arena_head;

  //convert reader speed to text, place first line, because it update key time
  a0_transport_debugstr_reader_speed(fake_lk, tm_now, ss);
  fprintf(ss,"-----------------\n");

  //convert writer speed to text, place first line, because it update key time
  a0_transport_debugstr_writer_speed(fake_lk, tm_now, ss);
  fprintf(ss,"-----------------\n");

  //convert arena header to text
  a0_transport_debugstr_arena_hdr(fake_lk, ss);
  fprintf(ss,"-----------------\n");

  //convert state to text
  a0_transport_debugstr_states(fake_lk, ss);
  fprintf(ss,"-----------------\n");

  //convert reader loc state to text
  a0_transport_debugstr_reader_loc_states(fake_lk, ss);
  fprintf(ss,"-----------------\n");

  //convert reader key state to text
  a0_transport_debugstr_reader_key_states(fake_lk, ss);
  fprintf(ss,"-----------------\n");

  //convert writer key state to text
  a0_transport_debugstr_writer_key_states(fake_lk, ss);
  fprintf(ss,"-----------------\n");

  //convert reader head to text
  a0_transport_debugstr_reader_hdr(fake_lk, ss);
  fprintf(ss,"-----------------\n");

  //convert writer head to text
  a0_transport_debugstr_writer_hdr(fake_lk, ss);
  fprintf(ss,"-----------------\n");

  //log end
  fflush(ss);
  fclose(ss);
}


///@lxf add
A0_STATIC_INLINE
void a0_cache_have_first_frame( size_t cache_size, a0_transport_frame_hdr_t* frame, bool *out)
{
    *out = true;
    if(cache_size <= a0_max_align(sizeof(a0_transport_frame_hdr_t))){
        *out = false;
    }

    size_t used_bytes_count;
    if(a0_max_align(sizeof(a0_transport_frame_hdr_t)) + a0_max_align(frame->data_size) > cache_size){
        *out = false;
    }
}

///@lxf add
A0_STATIC_INLINE
void a0_cache_have_next_frame(uint8_t* cache_data, size_t cache_size, a0_transport_frame_hdr_t* curr_frame_hdr,\
                            size_t first_frame_off, size_t write_off_tail, bool *out_next)
{
    //current frame is valid.
    *out_next = true;
    size_t used_bytes_count,rest_data_size;
    a0_transport_frame_hdr_t *next_frame_hdr;

    if(curr_frame_hdr->off != write_off_tail){
        //有下一条数据, 并且缓存里可能下一条数据
        used_bytes_count = curr_frame_hdr->off - first_frame_off + a0_max_align(sizeof(a0_transport_frame_hdr_t)) + a0_max_align(curr_frame_hdr->data_size);
        if(used_bytes_count + a0_max_align(sizeof(a0_transport_frame_hdr_t)) >= cache_size){
            //cache包含不了下一帧的帧头
            *out_next = false;
            return;
        }

        if(curr_frame_hdr->next_off < curr_frame_hdr->off){
            //下一帧的偏移环绕到共享内存数据区顶部了
            *out_next = false;
            return;
        }else if(curr_frame_hdr->next_off > curr_frame_hdr->off){
            //下一帧的偏移在当前帧的下方
            used_bytes_count = curr_frame_hdr->next_off - first_frame_off + a0_max_align(sizeof(a0_transport_frame_hdr_t));
            if(used_bytes_count >= cache_size){
               //下一帧的帧头等于或超出了cache
                *out_next = false;
                return;
            }

            next_frame_hdr = (a0_transport_frame_hdr_t*)(cache_data + curr_frame_hdr->next_off - first_frame_off);
            used_bytes_count = next_frame_hdr->off - first_frame_off + a0_max_align(sizeof(a0_transport_frame_hdr_t)) + a0_max_align(next_frame_hdr->data_size);
            if(used_bytes_count > cache_size){
                //下一个完整帧超出了cache
                *out_next = false;
            }
        }else{
            //不会发生
            //assert(0);
        }

    }else{
        //当前帧是尾帧
        *out_next = false;
    }
}

a0_err_t a0_cache_find_frames(uint8_t* cache_data, size_t cache_size,size_t write_off_tail,\
                                size_t* out_frames_count, size_t* out_frames_end, size_t* out_read_off)
{
    a0_transport_frame_hdr_t *curr_frame_hdr;
    curr_frame_hdr =(a0_transport_frame_hdr_t*)(cache_data);
    size_t first_frame_off = curr_frame_hdr->off;
    size_t last_org_off;
    size_t frames_count = 0;

    bool have_first_frame;
    a0_cache_have_first_frame(cache_size, curr_frame_hdr, &have_first_frame);
    if(!have_first_frame){
        //所以shm必须保存上次的off
        return A0_ERRCODE_NOT_FOUND;
    }

    bool out_next;
    do{
        last_org_off = curr_frame_hdr->off;
        a0_cache_have_next_frame(cache_data, cache_size, curr_frame_hdr,\
                            first_frame_off, write_off_tail, &out_next);
        curr_frame_hdr->off -= first_frame_off;

        //if(curr_frame_hdr->prev_off)
        curr_frame_hdr->prev_off -= first_frame_off;

        //if(curr_frame_hdr->next_off)
        curr_frame_hdr->next_off -= first_frame_off;

        if(out_next){
            curr_frame_hdr =(a0_transport_frame_hdr_t*)(cache_data+curr_frame_hdr->next_off);
        }
        frames_count++;
    }while(out_next);
    *out_frames_count = frames_count;
    *out_frames_end = curr_frame_hdr->off + a0_max_align(sizeof(a0_transport_frame_hdr_t)) + a0_max_align(curr_frame_hdr->data_size);
    *out_read_off = last_org_off;

    return A0_OK;
}