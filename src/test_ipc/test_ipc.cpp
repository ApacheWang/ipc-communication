#include <a0/file.h>
#include <a0/buf.h>
#include <a0/packet.h>
#include <a0/rpc.h>
//#include <a0/rpc.hpp>
//#include <a0/string_view.hpp>
#include <a0/uuid.h>
#include <a0/unused.h>

#include <queue/blockingconcurrentqueue.h>

#include <iostream>
#include <fcntl.h>
#include <map>
#include <chrono>
#include <condition_variable>
#include <cstring>
#include <functional>
#include <mutex>
#include <string>
#include <thread>

//#include "src/test_util.hpp"

#include <csignal>
#include<iostream>
#include <algorithm>

#undef REQUIRE_OK
#define REQUIRE_OK(err)\
 if((err) != A0_OK) \
    do {\
         return (-1); \
    } while (0)


//#define DEBUG_READ_OLDEST_PACKET
//#define FEATURE_PRE_ENQUEUE_SEND_QUEUE

#define MAX_LOOP_COUNT  (1000)

//#define DEBUG_MULTI_PKTS_SEND

#define SLEEP_TIME (1)
#define LOG_SLEEP_TIME      (60 * 3) /*in second*/

/*每次读取一个包*/
//#define FOWARD_PACKET_MODE  A0_ZEROCOPY_MODE_FORWARD_SINGLE_PKT
/*每次读取多个包*/
//#define FOWARD_PACKET_MODE  A0_ZEROCOPY_MODE_FORWARD_MULTI_PKTS
/*每次读取多个包*/
#define FOWARD_PACKET_MODE  A0_ZEROCOPY_MODE_FORWARD_MULTI_PKTS_ASSEMBLE

//#define _DEBUG_WRITER_CLOSE
//#define _DEBUG_READER_CLOSE

#define SIMULATOR_PERIOD_SIGNAL
#define USE_GEN_DATA_THREAD
//#define NOT_ECHO_DATA

//调用尝试队列尝试
//#define TRY_DEQUEUE_
//#define TRY_DEQUEUE_SLEEP_

enum{
    SIGNAL_PERIOD_TIME = 10, //singal period, in ms

    MAX_TEXT_SIZE=32,
    SEND_PACKET_SIZE = 4*1024+5,/* need SEND_PACKET_SIZE>=2*MAX_TEXT_SIZE */

    //read cache
    READ_CACHE_SIZE = 200*1024,/*必须大于SEND_PACKET_SIZE，否则常会启动后死机。*/
    READ_CACHE_ITME_COUNT = 12,

    //write
    WRITE_ITEM_COUNT = 10,

    MAX_BULK_COUNT = 10,

    PRE_SEND_QUEUE_LENGTH = 3500000,//50000,
    };

/////shm defination

struct RpcFixture {
    a0_rpc_topic_t topic = {"test", nullptr};
    std::string name;
    //const char* topic_path = "alephzero/test.rpc.a0";

    void append_name_suffix(const std::string& suffix){
        if(!suffix.empty())
        {
            name=topic.name;
            name+=".";
            name+=suffix;
            topic.name = name.c_str();
        }
    }

    RpcFixture() {
        //clear();
    }

    ~RpcFixture() {
        //clear();
    }

    void clear() {
        //a0_file_remove(topic_path);
    }
};

///////alloc

typedef struct a0_string_alloc_s
{
    a0_alloc_t  alloc;  // need be first field
    std::string data;
}a0_string_alloc_t;

A0_STATIC_INLINE
void a0_string_alloc_init(a0_string_alloc_t* body)
{
    body->alloc.user_data = (void*)&body->data;
    body->alloc.alloc =
          [](void* user_data, size_t size, a0_buf_t* out) {
            std::string *p=(std::string*)user_data;
            //TODO...plan to modify into static string,first decide if(size > p->size()){ p->resize(size);}
            //assert(0== p->size());
            p->resize(size);
            out->size = size;
            out->ptr = (uint8_t*)(p->c_str());
            return A0_OK;
          };
    body->alloc.dealloc = NULL;
}

typedef struct a0_dyn_alloc_s
{
    a0_alloc_t  alloc;  // need be first field
}a0_dyn_alloc_t;

A0_STATIC_INLINE
void a0_string_alloc_init(a0_dyn_alloc_t* body)
{
    body->alloc.user_data = NULL;
    body->alloc.alloc =
          [](void* user_data, size_t size, a0_buf_t* out) {
            A0_MAYBE_UNUSED(user_data);
            out->size = size;
            out->ptr = (uint8_t*)(malloc(size));
            return A0_OK;
          };
    body->alloc.dealloc = [](void* user_data, a0_buf_t buf)
    {
        A0_MAYBE_UNUSED(user_data);
        if(buf.ptr)
            free(buf.ptr);
        return A0_OK;
    };
}

/*inline a0_alloc_t string_alloc() {
  return (a0_alloc_t){
  };
};*/

typedef struct a0_string_cache_s {
    a0_cache_t cache; // need be first field
    std::string  str;
}a0_string_cache_t;

static void a0_read_cache_init(a0_string_cache_t* body,
                        size_t maxsize,
                        size_t max_items_count)
{
    body->str.resize(maxsize);
    body->cache.size = body->str.size();
    body->cache.data = (uint8_t*)body->str.c_str();

    body->cache.items_count = max_items_count;
}

//@data must is static of global var.
inline a0_alloc_t deseri_alloc(std::string& data) {
    return (a0_alloc_t){
            .user_data = &data,
            .alloc =
            [](void* user_data, size_t size, a0_buf_t* out) {
                std::string *p=(std::string*)user_data;

                // use max size and not ofen free
                if(p->size()<size)
                {
                    p->resize(size);
                }
                out->size = size;
                out->ptr = (uint8_t*)(p->c_str());
                return A0_OK;
            },
            .dealloc = nullptr,
    };
};

//////packet

inline void pkt_view(std::string *payload, a0_packet_t *out) {
  a0_packet_init(out);
  out->payload.ptr = (uint8_t*)payload->c_str();
  out->payload.size = payload->size();
}

inline std::string buf_str(a0_buf_t buf) {
  return std::string((char*)buf.ptr, buf.size);
}

///////signal handle
static volatile std::sig_atomic_t last_signal_(0);
static void signal_handler(const int signal)
{
    last_signal_ = signal;
    printf("\nctrl break,sig=%d;\n", last_signal_);
}

////////print

static void print_packet(const a0_packet_t& pkt)
{
#ifdef NOT_ECHO_DATA
    return;
#endif

#ifndef USE_GEN_DATA_THREAD
    static int n = 1;
    std::printf("no echo real msg, msg = %d\n", n++);
    return;
#endif

    char buf1[MAX_TEXT_SIZE];
    char buf2[MAX_TEXT_SIZE];
    memcpy(buf1, pkt.payload.ptr, MAX_TEXT_SIZE);
    memcpy(buf2, pkt.payload.ptr+pkt.payload.size-MAX_TEXT_SIZE, MAX_TEXT_SIZE);

#ifdef NO_PACKET_HEADER
                if(pkt.payload.size)
                {
                    //assert(0);
                    std::printf("\nsize=%lu, data_head=%s,\ndata_tail=%s\n",
                                pkt.payload.size,
                                buf1,
                                buf2);
                }else
                {
                    std::printf("\nsize=%lu, data_head=NULL,\ndata_tail=NULL\n",
                                pkt.payload.size);
                }
#elif defined(NO_PACKET_ID)
                if(pkt.payload.size)
                {
                    std::printf("\nkey1=%s, v1=%s, size=%lu, data_head=%s,\ndata_tail=%s\n",
                                pkt.headers_block.headers->key,
                                pkt.headers_block.headers->val,
                                pkt.payload.size,
                                buf1,
                                buf2);
                }else
                {
                    std::printf("\nsize=%lu, data_head=NULL,\ndata_tail=NULL\n",
                                pkt.payload.size);
                }
#else
                if(pkt.payload.size)
                {
                    std::printf("\nuuid=%s, key1=%s, v1=%s, size=%lu, data_head=%s,\ndata_tail=%s\n",
                                pkt.id,
                                pkt.headers_block.headers->key,
                                pkt.headers_block.headers->val,
                                pkt.payload.size,
                                buf1,
                                buf2);
                }else
                {
                    std::printf("\nuuid=%s, size=%lu, data_head=NULL,\ndata_tail=NULL\n\n",
                                pkt.id,
                                pkt.payload.size);
                }
#endif
}

static void print_packet_full(const a0_packet_t& pkt)
{
#ifdef NO_PACKET_HEADER
    if(pkt.payload.size)
    {
        std::printf("\nsize=%lu, data=%s\n",
                    pkt.payload.size,
                    pkt.payload.ptr);
    }else
    {
        std::printf("\nsize=%lu, data=NULL\n",
                    pkt.payload.size);
    }
#elif defined(NO_PACKET_ID)
    if(pkt.headers_block.size >0)
    {
        std::printf("\nkey1=%s, v1=%s, size=%lu, data_head=%s,\ndata_tail=%s\n",
                    pkt.headers_block.headers->key,
                    pkt.headers_block.headers->val,
                    pkt.payload.size,
                    buf1,
                    buf2);
    }else
    {
        std::printf("\nsize=%lu, data_head=NULL,\ndata_tail=NULL\n",
                    pkt.payload.size);
    }
#else
    if(pkt.headers_block.size >0)
    {
        std::printf("\nuuid=%s, key1=%s, v1=%s, size=%lu, data=%s\n",
                    pkt.id,
                    pkt.headers_block.headers->key,
                    pkt.headers_block.headers->val,
                    pkt.payload.size,
                    pkt.payload.ptr);
    }else
    {
        std::printf("\nuuid=%s, size=%lu, data=NULL\n",
                    pkt.id,
                    pkt.payload.size);
    }
#endif
}

/////////////////staic variation is only compile test app easily.
////////////////////
static int write(const std::string& name_suffix, std::thread& packet_thd, std::thread& send_thd)
{
    static int debug_max_loop_count = MAX_LOOP_COUNT;

#ifdef USE_GEN_DATA_THREAD
    //1. client packet message thread: loop addd send queue
    static moodycamel::BlockingConcurrentQueue<std::string> queue_send;
    packet_thd =std::move(std::thread([]() {
        std::string str_head("message--");
        std::string str_tail("tail-msg--");
        size_t n = 1;
        std::string buf;
        size_t max_text_size = MAX_TEXT_SIZE;
#ifdef FEATURE_PRE_ENQUEUE_SEND_QUEUE
        printf("Start Pre Enqueued Send queue %d, wait ...\n", PRE_SEND_QUEUE_LENGTH);
#endif

        do
        {
            buf.resize(SEND_PACKET_SIZE);
            uint8_t* p=(uint8_t*)(const_cast<char*>(buf.c_str()));

            std::string str;
            size_t len;
            str = str_head + std::to_string(n);
            len = str.length();
            memset(p,'*',MAX_TEXT_SIZE);
            memcpy(p, str.c_str(),std::min(len,max_text_size-1));
            p[MAX_TEXT_SIZE-2] = '#';
            p[MAX_TEXT_SIZE-1] = '\0';

            str = str_tail + std::to_string(n);
            len = str.length();
            memset(p+SEND_PACKET_SIZE-MAX_TEXT_SIZE,'*',MAX_TEXT_SIZE);
            memcpy(p+SEND_PACKET_SIZE-MAX_TEXT_SIZE, str.c_str(),std::min(len,max_text_size-1));
            p[SEND_PACKET_SIZE-2] = '#';
            p[SEND_PACKET_SIZE-1] = '\0';

#ifdef FEATURE_PRE_ENQUEUE_SEND_QUEUE
            if(n <= PRE_SEND_QUEUE_LENGTH) {
                queue_send.enqueue(std::move(buf));
                printf("Add %lu data\n", n);
            }else{
                //release memory, also allow not release
                buf.clear();
                printf("Finished Pre Enqueued Send queue %d, please press any key:", PRE_SEND_QUEUE_LENGTH);
                printf("....\n");
                //std::this_thread::sleep_for(std::chrono::milliseconds(500));
                //std::fflush(stdin);
                break;
            }
#else
            if(queue_send.size_approx()<1024*100){
                queue_send.enqueue(std::move(buf));
            }
            else
            {
                //release memory, also allow not release
                buf.clear();
            }
#endif

#ifndef FEATURE_PRE_ENQUEUE_SEND_QUEUE
#ifdef SIMULATOR_PERIOD_SIGNAL
            std::this_thread::sleep_for(std::chrono::milliseconds(SIGNAL_PERIOD_TIME));
#endif
#endif
            n++;
        }while(!last_signal_);
    }));
#endif

    //2. client send thread:
#if 1
    static a0_string_alloc_t onreply_alloc; //deprecated
#else
    static a0_string_alloc_t onreply_alloc;
    a0_string_alloc_init(&onreply_alloc);
#endif

#if 1
#else
    a0_packet_callback_t onreply = {
            .user_data = &data,
            .fn =
            [](void* user_data, a0_packet_t) {
                auto* data = (data_t*)user_data;
                std::unique_lock<std::mutex> lk{data->mu};
                data->reply_cnt++;
                data->cv.notify_all();
            },
    };
#endif

    //TODO impliment A0_ZEROCOPY_MODE_FORWARD_SINGLE_PKT mode to receive reply
    static a0_rpc_client_t client;
    std::string writer_name = "test_concurrent_w";
    if(!name_suffix.empty())
    {
        writer_name.append("_");
        writer_name.append(name_suffix);
    }

    REQUIRE_OK(a0_rpc_init_user(&client._user,
                            A0_USER_TYPE_WRITE,
                            NULL,
                            writer_name.c_str()));
    static RpcFixture fixture;
    fixture.append_name_suffix(name_suffix.c_str());
    REQUIRE_OK(a0_rpc_client_init(&client, fixture.topic, onreply_alloc.alloc));
    
    send_thd=std::move(std::thread([]() {
        static size_t write_count = 0;
        const int max = WRITE_ITEM_COUNT;
        int debug_loopcount = 0;

#ifdef FEATURE_PRE_ENQUEUE_SEND_QUEUE
        char ch;
        while ('\n' != (ch = getchar()));
#endif

#ifdef USE_GEN_DATA_THREAD
        while (!last_signal_) {
            std::string vDt[WRITE_ITEM_COUNT];
#ifdef DEBUG_MULTI_PKTS_SEND
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
#endif

#ifdef TRY_DEQUEUE_
            int count = queue_send.try_dequeue_bulk(vDt, max);
#else
            int count = queue_send.wait_dequeue_bulk(vDt, max);
#endif
            if(A0_ZEROCOPY_MODE_FORWARD_MULTI_PKTS_ASSEMBLE == FOWARD_PACKET_MODE)
            {
                a0_packet_t req[WRITE_ITEM_COUNT];
                for (int i = 0; i < count; i++) {
                    pkt_view(&vDt[i], &req[i]);
                }
                //send data array
                REQUIRE_OK(a0_rpc_client_send_no_ack(&client, req, count));
            }
            else
            {
                for (int i = 0; i < count; i++) {
                    a0_packet_t req;
                    pkt_view(&vDt[i], &req);
                    //send data
                    REQUIRE_OK(a0_rpc_client_send_no_ack(&client, &req, 1));
                    //despreated
                    //REQUIRE_OK(a0_rpc_client_cancel(&client, req.id));
                }
            }
            write_count+=count;

#ifdef FEATURE_PRE_ENQUEUE_SEND_QUEUE
            if(0 == queue_send.size_approx()){
                printf("Finish wrote shm %lu data\n", write_count);
                printf("...\n");
                break;
            }
#endif
            if (0 == count) {
                // this loop done, pause...
                #ifdef TRY_DEQUEUE_SLEEP_
                std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_TIME));
                #endif
            }

#ifdef _DEBUG_WRITER_CLOSE
            if(++debug_loopcount>=debug_max_loop_count)
            {
                last_signal_ = 2;
            }
#endif

#ifdef FEATURE_PRE_ENQUEUE_SEND_QUEUE
#ifdef SIMULATOR_PERIOD_SIGNAL
            std::this_thread::sleep_for(std::chrono::milliseconds(SIGNAL_PERIOD_TIME));
#endif
#endif
        }
#else/*USE_GEN_DATA_THREAD*/
        while (!last_signal_) {
            //generate random data
            std::string data;
            data.resize(SEND_PACKET_SIZE); //

            a0_packet_t req;
            pkt_view(&data, &req);
            //send data
            REQUIRE_OK(a0_rpc_client_send_no_ack(&client, &req,1));
#ifdef SIMULATOR_PERIOD_SIGNAL
            std::this_thread::sleep_for(std::chrono::milliseconds(SIGNAL_PERIOD_TIME));
#endif
        }
#endif
        //Close connection
        a0_rpc_client_close(&client);
        printf("a0_rpc_client_close\n");
        printf("...\n");

        return 0;
    }));

    return 0;
}

static int read(const std::string& name_suffix, std::thread& deseri_thd)
{
    //3. receive thread, not deserialization message
    static int debug_max_loop_count = MAX_LOOP_COUNT;

    static a0_string_alloc_t onreq_alloc;
    a0_string_alloc_init(&onreq_alloc);
    static moodycamel::BlockingConcurrentQueue<std::string> queue_recv;
    a0_rpc_request_flat_callback_t on_flat_request = {
            .user_data = &queue_recv,
            .flat_data = onreq_alloc.alloc.user_data,
            .fn = []( void* user_data, void* flat_data){
                moodycamel::BlockingConcurrentQueue<std::string> *queue=static_cast<moodycamel::BlockingConcurrentQueue<std::string>*>(user_data);
                std::string* data = static_cast<std::string*>(flat_data);
                if(queue->size_approx()<1024*100){
                    queue->enqueue(std::move(*data));
                }else
                {//release memory, also allow not release
                    data->clear();
                }
            },
    };

    #if 1
    a0_rpc_request_callback_t onrequest; //despreated
    #else
    a0_rpc_request_callback_t onrequest = {
            .user_data = nullptr,
            .fn =
            [](void*, a0_rpc_request_t req) {
                if (buf_str(req.pkt.payload) == "reply") {
                  a0_packet_t pkt;
                  std::string s("echo");
                  pkt_view(&s, &pkt);
                    REQUIRE_OK(a0_rpc_server_reply(req, pkt));
                }
            },
    };
    #endif

    #if 1
    a0_packet_id_callback_t oncancel; //deprecated
    #else
    a0_packet_id_callback_t oncancel = {
            .user_data = &data,
            .fn =
            [](void* user_data, a0_uuid_t) {
                auto* data = (data_t*)user_data;
                std::unique_lock<std::mutex> lk{data->mu};
                data->cancel_cnt++;
                data->cv.notify_all();
            },
    };
    #endif

    static a0_rpc_server_t server;
    static a0_string_cache_t cache; //cache multiple flat packet
    a0_read_cache_init(&cache, READ_CACHE_SIZE,READ_CACHE_ITME_COUNT);

    std::string reader_name = "test_concurrent_r";
    if(!name_suffix.empty())
    {
        reader_name.append("_");
        reader_name.append(name_suffix);
    }

    static RpcFixture fixture;
    fixture.append_name_suffix(name_suffix);
    REQUIRE_OK(a0_rpc_init_user(&server._user,
                            A0_USER_TYPE_READ,
                            reader_name.c_str(),
                            NULL));
#ifdef DEBUG_READ_OLDEST_PACKET
    REQUIRE_OK(a0_flat_rpc_server_init_oldest(&server, fixture.topic, onreq_alloc.alloc,
                                       on_flat_request, onrequest, oncancel,
                                       FOWARD_PACKET_MODE, cache.cache));
#else
    REQUIRE_OK(a0_flat_rpc_server_init(&server, fixture.topic, onreq_alloc.alloc,
                                        on_flat_request, onrequest, oncancel,
                                        FOWARD_PACKET_MODE, cache.cache));
#endif

    //4. deserialization message
    if(A0_ZEROCOPY_MODE_FORWARD_MULTI_PKTS_ASSEMBLE==FOWARD_PACKET_MODE)
    {
        deseri_thd=std::move(std::thread ([]() {
            static size_t read_count = 0;
            const int max = MAX_BULK_COUNT;
            int debug_loopcount = 0;
            std::string vDt[max];
            static std::string mem_block;
#ifdef NO_PACKET_HEADER
#else
            a0_alloc_t alloc = deseri_alloc(mem_block);
#endif
            while (!last_signal_) {
#ifdef TRY_DEQUEUE_
                int count = queue_recv.try_dequeue_bulk(vDt, max);
#else
                int count = queue_recv.wait_dequeue_bulk(vDt, max);
#endif
                for (int i = 0; i < count; i++) {
                    uint8_t* assem_data = (uint8_t*)vDt[i].c_str();
                    a0_multi_flat_packets_header_t* packets_header=(a0_multi_flat_packets_header_t*)assem_data;
                    size_t curr_off = 0;
                    size_t next_off;
                    for(int j=0; j<packets_header->pkts_count;j++){
                        a0_flat_packet_t fpkt;
                        a0_reader_get_one_flat_packet_from_assemble(assem_data, curr_off, &next_off, &fpkt);
                        curr_off = next_off;

                        a0_packet_t out_pkt;
                        a0_buf_t out_buf;
                        //deserialize flat paket
#ifdef NO_PACKET_HEADER
                        a0_err_t err= a0_packet_deserialize_no_header(fpkt, &out_pkt, &out_buf);
#else
                        a0_err_t err= a0_packet_deserialize(fpkt, alloc, &out_pkt, &out_buf);
#endif
                        print_packet(out_pkt);
                    }
                    read_count+=packets_header->pkts_count;
                }

                if (0 == count) {
                    // this loop done, pause...
#ifdef TRY_DEQUEUE_SLEEP_
                    std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_TIME));
#endif
                }

#ifdef _DEBUG_READER_CLOSE
                if(++debug_loopcount>=debug_max_loop_count)
            {
                last_signal_ = 2;
            }
#endif
            }

            printf("read shm %lu data\n", read_count);
            printf("...\n");
            //Close connection
            a0_rpc_server_close(&server);
            printf("a0_rpc_server_close\n");
            printf("...\n");
            return 0;
        }));
    }
    else/*A0_ZEROCOPY_MODE_FORWARD_MULTI_PKTS_ASSEMBLE==FOWARD_PACKET_MODE*/
    {
        deseri_thd=std::move(std::thread ([]() {
            static size_t read_count = 0;
            const int max = MAX_BULK_COUNT;
            int debug_loopcount = 0;
            std::string vDt[max];
            static std::string mem_block;
#ifdef NO_PACKET_HEADER
#else
            a0_alloc_t alloc = deseri_alloc(mem_block);
#endif
            while (!last_signal_) {
#ifdef TRY_DEQUEUE_
                int count = queue_recv.try_dequeue_bulk(vDt, max);
#else
                int count = queue_recv.wait_dequeue_bulk(vDt, max);
#endif
                for (int i = 0; i < count; i++) {
                    a0_flat_packet_t fpkt=
                            {
                                    .buf = {.ptr= static_cast<uint8_t*>(static_cast<void*>(const_cast<char*>(vDt[i].c_str()))),
                                            .size= vDt[i].size()},
                            };

                    a0_packet_t out_pkt;
                    a0_buf_t out_buf;
                    //deserialize flat paket
#ifdef NO_PACKET_HEADER
                    a0_err_t err= a0_packet_deserialize_no_header(fpkt, &out_pkt, &out_buf);
#else
                    a0_err_t err= a0_packet_deserialize(fpkt, alloc, &out_pkt, &out_buf);
#endif
                    print_packet(out_pkt);
                }
                read_count+=count;

                if (0 == count) {
                    // this loop done, pause...
#ifdef TRY_DEQUEUE_SLEEP_
                    std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_TIME));
#endif
                }

#ifdef _DEBUG_READER_CLOSE
                if(++debug_loopcount>=debug_max_loop_count)
            {
                last_signal_ = 2;
            }
#endif
            }

            printf("read shm %lu data\n", read_count);
            printf("...\n");
            //Close connection
            a0_rpc_server_close(&server);
            printf("a0_rpc_server_close\n");
            printf("...\n");
            return 0;
        }));
    }

    return 0;
}

static int read_log_once(a0_rpc_log_t* log_server, a0_dyn_alloc_t* dyn_alloc, a0_arena_log_t* arena_log, size_t log_count)
{
    // get log
    memset(arena_log, 0, sizeof(a0_arena_log_t));
    REQUIRE_OK(a0_rpc_log_server_read_once(log_server,
                            dyn_alloc->alloc,
                            A0_INIT_MOST_RECENT,
                            O_NONBLOCK,                                  
                            arena_log));

    //print text
    if(arena_log->arena_head.payload.ptr)
    {
        std::printf("\n--arena header, log_count [%lu]------------\n",log_count);
        print_packet_full(arena_log->arena_head);
        free(arena_log->arena_head.payload.ptr); //malloc by open_memstream
    }

    if(arena_log->frame_head.payload.ptr)
    {
        std::printf("\n--tail frame header------------\n");
        print_packet_full(arena_log->frame_head);
        dyn_alloc->alloc.dealloc(NULL, arena_log->frame_head.payload);
    }

    if(arena_log->frame_data.payload.ptr)
    {
        std::printf("\n--tail frame data------------\n");
        print_packet(arena_log->frame_data);
#ifdef NO_PACKET_HEADER
        dyn_alloc->alloc.dealloc(NULL, arena_log->frame_data.payload);
#else
        a0_buf_t buf ={.ptr = (uint8_t*)arena_log->frame_data.headers_block.headers,};
        dyn_alloc->alloc.dealloc(NULL, buf);
#endif
    }
    std::printf("\n--log end------------\n");

    return 0;
}

//run write and read threads
static int run_wr(const std::string& name_suffix)
{
    std::thread packet_thd;
    std::thread send_thd;
    write(name_suffix,packet_thd, send_thd);

    std::thread deseri_thd;
    read(name_suffix,deseri_thd);

#ifdef USE_GEN_DATA_THREAD
    packet_thd.join();
#endif
    send_thd.join();

    deseri_thd.join();
    return 0;
}

static int run_w(const std::string& name_suffix)
{
    std::thread packet_thd;
    std::thread send_thd;
    write(name_suffix, packet_thd, send_thd);

#ifdef USE_GEN_DATA_THREAD
    packet_thd.join();
#endif
    send_thd.join();
    return 0;
}

static int run_r(const std::string& name_suffix)
{
    std::thread deseri_thd;
    read(name_suffix,deseri_thd);
    deseri_thd.join();
    return 0;
}

static int run_r_log(const std::string& name_suffix, bool auto_loop)
{
    static int log_count = 1;
    static a0_rpc_log_t log_server;
    std::string reader_name = "log_test_concurrent_r";
    if(!name_suffix.empty())
    {
        reader_name.append("_");
        reader_name.append(name_suffix);
    }

    static RpcFixture fixture;
    fixture.append_name_suffix(name_suffix);
    REQUIRE_OK(a0_rpc_init_user(&log_server._user,
                            A0_USER_TYPE_READ,
                            reader_name.c_str(),
                            NULL));
    REQUIRE_OK(a0_rpc_log_server_init(&log_server, fixture.topic));

    a0_dyn_alloc_t dyn_alloc;
    a0_string_alloc_init(&dyn_alloc);

    a0_arena_log_t arena_log;
    while (!last_signal_) 
    {
        if(auto_loop)
        {
            std::cout << "print auto loop every "<<LOG_SLEEP_TIME<<" seconds, count " <<log_count<<std::endl;

            //clear screen
            //printf("\x1b[H\x1b[2J");
            printf("\033c");

            //读取一次Log
            read_log_once(&log_server, &dyn_alloc, & arena_log, log_count++);
            std::this_thread::sleep_for(std::chrono::seconds(LOG_SLEEP_TIME));
        }
        else
        {
            char ch;
            //clear screen
            //printf("\x1b[H\x1b[2J");
            printf("\033c");

            if('q' ==ch || 'Q' == ch){
                last_signal_ = 2;
                break;
            }
            else{
                //读取一次Log
                read_log_once(&log_server, &dyn_alloc, &arena_log, log_count++);
            }

            std::cout << "press any character key to print log, or q to quit, next count: " <<log_count<< ": ";
            //std::cin>>ch;
            //fflush(stdin);
            while ('\n' != (ch = getchar()));
            std::cout <<std::endl;
        }
    }
   
    a0_rpc_log_server_close(&log_server);
    printf("a0_rpc_log_server_close\n");
    printf("...\n");
    return 0;
}

using map_args_type = std::map<std::string, std::string>;
static void parse_args(const char* arg, map_args_type& args_map)
{
    if(!strcasecmp(arg, "w"))
    {
        args_map["w"] = "1";
    }else if(!strcasecmp(arg, "r"))
    {
       args_map["r"] = "1";
    }else if(!strncasecmp(arg, "f",1))
    {
       args_map["f"] = std::string(arg+1);
    }else if(!strncasecmp(arg, "log",3))
    {
       args_map["log"] = "1";
    }else if(!strncasecmp(arg, "loop",4))
    {
        args_map["loop"] = "1";
    }

}


//TODO bulk add token
#if 0
int main(int argc, char* argv[])
{
    std::string name_suffix;
    int err =-1;
    for(int i=0;i<argc;i++){
        std::printf("\"%s\" ",argv[i]);
    }
    std::printf("\n");

    // Set the signal handler first
    //const auto prev_handler = std::signal(SIGINT | SIGABRT, signal_handler);
    const auto prev_handler = std::signal(SIGINT, signal_handler);
    if(prev_handler == SIG_ERR)
        throw std::runtime_error("std::signal failed");

    std::map<std::string, std::string> args_map;
    if(1 == argc)
    {
        err = run_wr(std::string());
    }else
    {
        for(int i=1; i<argc;i++)
        {
            parse_args(argv[i], args_map);
        }

        if(args_map.end() !=args_map.find("f"))
        {//found shm file name suffix
            if(args_map["f"].length()>3)
            {
                return -1;
            }
            name_suffix=args_map["f"];
        }

        if(args_map.end() !=args_map.find("w") && args_map.end() !=args_map.find("r"))
        {
            //found write and read
            err=-2;
            goto end;
        }

        if(args_map.end() !=args_map.find("f"))
        {
            name_suffix = args_map["f"];
        }

        // start write or read thread
        if(args_map.end() !=args_map.find("w"))
        {//found write
            err = run_w(name_suffix);
        }else if(args_map.end() !=args_map.find("r"))
        {//found read
            if(args_map.end() !=args_map.find("log")){
                bool auto_loop = false;
                if(args_map.end() !=args_map.find("loop")) {
                    auto_loop = true;
                }
                err = run_r_log(name_suffix, auto_loop);
            }
            else{
                err = run_r(name_suffix);
            }
           
        }else
        {
            err=-3;
        }
    }

    end:
    std::printf("\nmain exit(%d)\n",err);
    return 0;
}
#endif