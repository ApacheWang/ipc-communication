#include <a0/alloc.h>
#include <a0/buf.h>
#include <a0/compare.h>
#include <a0/err.h>
#include <a0/file.h>
#include <a0/inline.h>
#include <a0/map.h>
#include <a0/middleware.h>
#include <a0/packet.h>
#include <a0/reader.h>
#include <a0/rpc.h>
#include <a0/uuid.h>
#include <a0/writer.h>

#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "err_macro.h"
#include "topic.h"

static const char RPC_TYPE[] = "a0_rpc_type";
static const char RPC_TYPE_REQUEST[] = "request";
static const char RPC_TYPE_RESPONSE[] = "response";
static const char RPC_TYPE_CANCEL[] = "cancel";

static const char REQUEST_ID[] = "a0_req_id";

A0_STATIC_INLINE
a0_err_t a0_rpc_topic_open(a0_rpc_topic_t topic, a0_file_t* file) {
  //lxf modify
  const char* tmpl = getenv("A0_IPC_TOPIC_TEMPLATE");
  if (!tmpl) {
    //lxf modify
    tmpl = "alephzero/ipc.a0.{topic}";
  }
  return a0_topic_open(tmpl, topic.name, topic.file_opts, file);
}

////////////
// Server //
////////////

#ifndef NO_PACKET_HEADER
A0_STATIC_INLINE
void a0_rpc_server_onpacket(void* data, a0_packet_t pkt) {
  a0_rpc_server_t* server = (a0_rpc_server_t*)data;

  a0_packet_header_t type_hdr;
  a0_packet_header_iterator_t hdr_iter;
  a0_packet_header_iterator_init(&hdr_iter, &pkt);
  if (a0_packet_header_iterator_next_match(&hdr_iter, RPC_TYPE, &type_hdr)) {
    return;
  }

  if (!strcmp(type_hdr.val, RPC_TYPE_REQUEST)) {
    server->_onrequest.fn(server->_onrequest.user_data, (a0_rpc_request_t){server, pkt});
  } else if (!strcmp(type_hdr.val, RPC_TYPE_CANCEL)) {
    //lxf remove, not use
    /*if (server->_oncancel.fn) {
      a0_uuid_t uuid;
      memcpy(uuid, pkt.payload.ptr, A0_UUID_SIZE);
      server->_oncancel.fn(server->_oncancel.user_data, uuid);
    }*/
  }
}
#endif


///@lxf add
A0_STATIC_INLINE
void a0_rpc_server_onpacket_flat(void* data) {
  a0_rpc_server_t* server = (a0_rpc_server_t*)data;

  if(server->_onrequest_flat.fn)
  {
    server->_onrequest_flat.fn(server->_onrequest_flat.user_data,server->_onrequest_flat.flat_data);
  }
}

a0_err_t a0_rpc_server_init(a0_rpc_server_t* server,
                            a0_rpc_topic_t topic,
                            a0_alloc_t alloc,
                            a0_rpc_request_callback_t onrequest,
                            a0_packet_id_callback_t oncancel) {
  server->_onrequest = onrequest;
  //lxf remove, not use
  //server->_oncancel = oncancel;

  // Response writer must be set up before the request reader to avoid a race condition.

  A0_RETURN_ERR_ON_ERR(a0_rpc_topic_open(topic, &server->_file));

  //lxf remove, not use
  /*a0_err_t err = a0_writer_init(&server->_response_writer, &server->_user, server->_file.arena);
  if (err) {
    a0_file_close(&server->_file);
    return err;
  }

  err = a0_writer_push(&server->_response_writer, a0_add_standard_headers());
  if (err) {
    a0_writer_close(&server->_response_writer);
    a0_file_close(&server->_file);
    return err;
  }*/

  //lxf add a0_err_t for compile code
  a0_err_t err = a0_reader_init(
      &server->_request_reader,
      &server->_user,
      server->_file.arena,
      alloc,
      A0_INIT_AWAIT_NEW,
      A0_ITER_NEXT,
      (a0_packet_callback_t){
          .user_data = server,
    #ifdef NO_PACKET_HEADER
            .fn = NULL,
    #else
          .fn = a0_rpc_server_onpacket,
    #endif
      },
      A0_ZEROCOPY_MODE_PARSE/*@lxf add*/,
      (a0_cache_t){}/*@lxf add*/);
  if (err) {
    //lxf remove, not use
    //a0_writer_close(&server->_response_writer);
    a0_file_close(&server->_file);
    return err;
  }

  return A0_OK;
}

//@lxf add
a0_err_t a0_rpc_init_user(a0_user_identify_t* user,
                            a0_user_type_t user_type,
                            const char* reader_name,
                            const char* writter_name)
{
  if(user_type & A0_USER_TYPE_READ)
  {
    if(!reader_name || '\0'==reader_name[0] || strlen(reader_name) > USER_NAME_LENGTH)
    {
      return A0_ERRCODE_INVALID_ARG;
    }

    strcpy(user->reader_name, reader_name);
  }

  if(user_type & A0_USER_TYPE_WRITE)
  {
    if(!writter_name || '\0'==writter_name[0] || strlen(writter_name) > USER_NAME_LENGTH)
    {
      return A0_ERRCODE_INVALID_ARG;
    }

    strcpy(user->writer_name, writter_name);
  }

  user->user_type = user_type;

  return A0_OK;
}

//@lxf Add, refer to <a0_rpc_server_init>
a0_err_t a0_flat_rpc_server_init_full(a0_rpc_server_t* server,
                            a0_rpc_topic_t topic,
                            a0_alloc_t alloc,
                            a0_reader_init_t init,
                            a0_rpc_request_flat_callback_t onrequest_flat/*@lxf add*/,
                            a0_rpc_request_callback_t onrequest,
                            a0_packet_id_callback_t oncancel,
                            a0_zero_copy_mode_t cpymode/*@lxf add*/,
                            a0_cache_t cache/*@lxf add*/) {
  server->_onrequest_flat = onrequest_flat; //@lxf add
  server->_onrequest = onrequest;
  //lxf remove, not use
  //server->_oncancel = oncancel;

  // Response writer must be set up before the request reader to avoid a race condition.

  A0_RETURN_ERR_ON_ERR(a0_rpc_topic_open(topic, &server->_file));

  a0_err_t err;
  /*lxf remove.
  不需要发送Response数据*/
/*
  a0_err_t err = a0_writer_init(&server->_response_writer, server->_file.arena);
  if (err) {
    a0_file_close(&server->_file);
    return err;
  }

  err = a0_writer_push(&server->_response_writer, a0_add_standard_headers());
  if (err) {
    a0_writer_close(&server->_response_writer);
    a0_file_close(&server->_file);
    return err;
  }
*/

  err = a0_reader_init(
      &server->_request_reader,
      &server->_user,
      server->_file.arena,
      alloc,
      init,
      A0_ITER_NEXT,
      (a0_packet_callback_t){
          .user_data = server,
    #ifdef NO_PACKET_HEADER
            .fn = NULL,
    #else
          .fn = a0_rpc_server_onpacket,
    #endif
          .fn_flat = a0_rpc_server_onpacket_flat,
      },
      cpymode/*@lxf add*/,
      cache/*@lxf add*/);
  if (err) {
    //lxf remove, not use
    //a0_writer_close(&server->_response_writer);
    a0_file_close(&server->_file);
    return err;
  }

  return A0_OK;
}

//@lxf Add
a0_err_t a0_flat_rpc_server_init(a0_rpc_server_t* server,
                                 a0_rpc_topic_t topic,
                                 a0_alloc_t alloc,
                                 a0_rpc_request_flat_callback_t onrequest_flat/*@lxf add*/,
                                 a0_rpc_request_callback_t onrequest,
                                 a0_packet_id_callback_t oncancel,
                                 a0_zero_copy_mode_t cpymode/*@lxf add*/,
                                 a0_cache_t cache/*@lxf add*/) {
    return a0_flat_rpc_server_init_full(server,
                                  topic,
                                  alloc,
                                  A0_INIT_AWAIT_NEW,
                                    onrequest_flat,
                                    onrequest,
                                    oncancel,
                                    cpymode,
                                    cache);
}

//@lxf Add
a0_err_t a0_flat_rpc_server_init_oldest(a0_rpc_server_t* server,
                                 a0_rpc_topic_t topic,
                                 a0_alloc_t alloc,
                                 a0_rpc_request_flat_callback_t onrequest_flat/*@lxf add*/,
                                 a0_rpc_request_callback_t onrequest,
                                 a0_packet_id_callback_t oncancel,
                                 a0_zero_copy_mode_t cpymode/*@lxf add*/,
                                 a0_cache_t cache/*@lxf add*/) {
    return a0_flat_rpc_server_init_full(server,
                                        topic,
                                        alloc,
                                        A0_INIT_OLDEST,
                                        onrequest_flat,
                                        onrequest,
                                        oncancel,
                                        cpymode,
                                        cache);
}

a0_err_t a0_rpc_server_close(a0_rpc_server_t* server) {
  a0_reader_close(&server->_request_reader);
  //lxf remove, not use
 //a0_writer_close(&server->_response_writer);
  a0_file_close(&server->_file);
  return A0_OK;
}

a0_err_t a0_rpc_server_reply(a0_rpc_request_t req, a0_packet_t resp) {
#ifdef NO_PACKET_HEADER
  //lxf remove, not use
  //return a0_writer_write(&req.server->_response_writer, resp);
#else/*NO_PACKET_HEADER*/
  a0_packet_header_t extra_headers[] = {
      {RPC_TYPE, RPC_TYPE_RESPONSE},
      //lxf modify
#ifdef NO_PACKET_ID
#else/*NO_PACKET_ID*/
      {REQUEST_ID, (char*)req.pkt.id},
      {A0_PACKET_DEP_KEY, (char*)req.pkt.id},
#endif
  };
  //lxf modify
  const size_t num_extra_headers = sizeof(extra_headers)/sizeof(a0_packet_header_t);

  a0_packet_t full_resp = resp;
  full_resp.headers_block = (a0_packet_headers_block_t){
      .headers = extra_headers,
      .size = num_extra_headers,
      .next_block = (a0_packet_headers_block_t*)&resp.headers_block,
  };

  //lxf remove, not use
  //return a0_writer_write(&req.server->_response_writer, full_resp);
#endif
  return A0_OK;
}

////////////
// Client //
////////////

#ifndef NO_PACKET_HEADER
A0_STATIC_INLINE
void a0_rpc_client_onpacket(void* user_data, a0_packet_t pkt) {
  a0_rpc_client_t* client = (a0_rpc_client_t*)user_data;

  a0_packet_header_t req_id_hdr;
  a0_packet_header_iterator_t hdr_iter;
  a0_packet_header_iterator_init(&hdr_iter, &pkt);
  if (a0_packet_header_iterator_next_match(&hdr_iter, REQUEST_ID, &req_id_hdr)) {
    return;
  }

  //lxf remove, not use
  /*a0_packet_callback_t cb;
  pthread_mutex_lock(&client->_outstanding_requests_mu);
  a0_err_t err = a0_map_pop(&client->_outstanding_requests, req_id_hdr.val, &cb);
  pthread_mutex_unlock(&client->_outstanding_requests_mu);
  if (!err) {
    a0_packet_callback_call(cb, pkt);
  }*/
}
#endif

a0_err_t a0_rpc_client_init(a0_rpc_client_t* client,
                            a0_rpc_topic_t topic,
                            a0_alloc_t alloc) {
  // Outstanding requests must be initialized before the response reader is opened to avoid a race condition.

//lxf remove, not use
  /*A0_RETURN_ERR_ON_ERR(a0_map_init(
      &client->_outstanding_requests,
      sizeof(a0_uuid_t),
      sizeof(a0_packet_callback_t),
      A0_HASH_UUID,
      A0_COMPARE_UUID));
  pthread_mutex_init(&client->_outstanding_requests_mu, NULL);*/

  a0_err_t err = a0_rpc_topic_open(topic, &client->_file);
  if (err) {
    //lxf remove, not use
    /*a0_map_close(&client->_outstanding_requests);
    pthread_mutex_destroy(&client->_outstanding_requests_mu);*/
    return err;
  }

  err = a0_writer_init(&client->_request_writer, &client->_user, client->_file.arena);
  if (err) {
    a0_file_close(&client->_file);
    //lxf remove, not use
    /*a0_map_close(&client->_outstanding_requests);
    pthread_mutex_destroy(&client->_outstanding_requests_mu);*/
    return err;
  }

#ifdef NO_PACKET_HEADER
#else
  err = a0_writer_push(&client->_request_writer, a0_add_standard_headers());
  if (err) {
    a0_writer_close(&client->_request_writer);
    a0_file_close(&client->_file);
    //lxf remove, not use
    /*a0_map_close(&client->_outstanding_requests);
    pthread_mutex_destroy(&client->_outstanding_requests_mu);*/
    return err;
  }
#endif

  /*@lxf 移除
  因为读Response数据，需要按照顺序读取共享内存里的所有未读数据，无论是否是response数据，效率低，
  所以去除读数据，相应地在close时也要去除read close.
  */
#if 0
  err = a0_reader_init(
      &client->_response_reader,
      client->_file.arena,
      alloc,
      A0_INIT_AWAIT_NEW,
      A0_ITER_NEXT,
      (a0_packet_callback_t){
          .user_data = client,
          #ifdef NO_PACKET_HEADER
          .fn = NULL,
          #else
          .fn = a0_rpc_client_onpacket,
          #endif
      },
      A0_ZEROCOPY_MODE_PARSE/*@lxf add*/,
      (a0_cache_t){}/*@lxf add*/);
  if (err) {
    a0_writer_close(&client->_request_writer);
    a0_file_close(&client->_file);
    a0_map_close(&client->_outstanding_requests);
    pthread_mutex_destroy(&client->_outstanding_requests_mu);
    return err;
  }
#endif

  return A0_OK;
}

a0_err_t a0_rpc_client_close(a0_rpc_client_t* client) {
  /*@lxf 移除
  因为移除了读Response数据，所以这里也要移除
  */
  //a0_reader_close(&client->_response_reader);

  a0_writer_close(&client->_request_writer);
  a0_file_close(&client->_file);
  //lxf remove, not use
  /*a0_map_close(&client->_outstanding_requests);
  pthread_mutex_destroy(&client->_outstanding_requests_mu);*/
  return A0_OK;
}

a0_err_t a0_rpc_client_send(a0_rpc_client_t* client, a0_packet_t* pkt/*lxf modify*/, size_t pkt_count/*lxf add*/,a0_packet_callback_t onresponse) {
  //lxf remove, not use
  /*pthread_mutex_lock(&client->_outstanding_requests_mu);
  a0_err_t err = a0_map_put(&client->_outstanding_requests, pkt.id, &onresponse);
  pthread_mutex_unlock(&client->_outstanding_requests_mu);
  A0_RETURN_ERR_ON_ERR(err);*/

 #ifdef NO_PACKET_HEADER
  return a0_writer_write(&client->_request_writer, pkt, pkt_count);
 #else
  const size_t num_extra_headers = 1;
  a0_packet_header_t extra_headers[] = {
      {RPC_TYPE, RPC_TYPE_REQUEST},
  };

  a0_packet_t full_pkt = *pkt;
  full_pkt.headers_block = (a0_packet_headers_block_t){
      .headers = extra_headers,
      .size = num_extra_headers,
      .next_block = (a0_packet_headers_block_t*)&pkt->headers_block,
  };

  return a0_writer_write(&client->_request_writer, &full_pkt, 1);
#endif
}

//lxf add, send directly without extra packet headers
A0_STATIC_INLINE_RECURSIVE
a0_err_t a0_writer_write_direct(a0_writer_t* w, a0_packet_t *pkt, size_t pkt_count) {
    a0_middleware_chain_t chain;
    //assert(w->_action.process == NULL);
    A0_RETURN_ERR_ON_ERR(w->_action.process_locked(w->_action.user_data, chain._node._tlk, pkt, pkt_count, chain));
}

///@lxf add, only remove @onresponse from above function a0_rpc_client_send
a0_err_t a0_rpc_client_send_no_ack(a0_rpc_client_t* client, a0_packet_t* pkt, size_t pkt_count) {
#ifdef NO_PACKET_HEADER
  return a0_writer_write_direct(&client->_request_writer, pkt, pkt_count);
#else
    //assert(pkt_count<2);
    if(pkt_count > 1)
        return A0_ERRCODE_INVALID_ARG;

  a0_packet_t full_pkt = *pkt;

  const size_t num_extra_headers = 1;
  a0_packet_header_t extra_headers[] = {
      {RPC_TYPE, RPC_TYPE_REQUEST},
  };

  full_pkt.headers_block = (a0_packet_headers_block_t){
      .headers = extra_headers,
      .size = num_extra_headers,
      .next_block = (a0_packet_headers_block_t*)&pkt->headers_block,
  };

  return a0_writer_write(&client->_request_writer, &full_pkt, 1);
#endif
}

//lxf, 目前无此需求, 不使用了
a0_err_t a0_rpc_client_cancel(a0_rpc_client_t* client, const a0_uuid_t uuid) {
  //lxf remove, not use
  /*pthread_mutex_lock(&client->_outstanding_requests_mu);
  a0_map_del(&client->_outstanding_requests, uuid);
  pthread_mutex_unlock(&client->_outstanding_requests_mu);*/

  a0_packet_t pkt;
  a0_packet_init(&pkt);

 #ifndef NO_PACKET_HEADER
  const size_t num_headers = 3;
  a0_packet_header_t headers[] = {
      {RPC_TYPE, RPC_TYPE_CANCEL},
      {REQUEST_ID, uuid},
      {A0_PACKET_DEP_KEY, uuid},
  };

  pkt.headers_block = (a0_packet_headers_block_t){
      .headers = headers,
      .size = num_headers,
      .next_block = NULL,
  };
#endif
  pkt.payload = (a0_buf_t){
      .ptr = (uint8_t*)uuid,
      .size = A0_UUID_SIZE,
  };

  //@lxf modify
  return a0_writer_write(&client->_request_writer, &pkt, 1);
}

////////////////
// Log server //
///////////////

//@lxf Add, for log
a0_err_t a0_rpc_log_server_init(a0_rpc_log_t* log_server,
                            a0_rpc_topic_t topic) {
  A0_RETURN_ERR_ON_ERR(a0_rpc_topic_open(topic, &log_server->_file));
  return A0_OK;
}

//@lxf Add, for log
a0_err_t a0_rpc_log_server_close(a0_rpc_log_t* log_server) {
  a0_file_close(&log_server->_file);
  return A0_OK;
}

//@lxf Add, for log
a0_err_t a0_rpc_log_server_read_once(a0_rpc_log_t* log_server,
                            a0_alloc_t alloc,
                            a0_reader_init_t init,
                            int flags,                                  
                           a0_arena_log_t *out)
{
  return a0_reader_read_arena_log(&log_server->_user,
                            log_server->_file.arena,
                            alloc,
                            init,
                            flags,                                  
                            out);
}
